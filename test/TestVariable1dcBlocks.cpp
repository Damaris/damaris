#include <cppunit/ui/text/TestRunner.h>
#include <mpi.h>

#include "TestVariable1dcBlocks.hpp"

using namespace std;

int main(int argc, char** argv) {
    MPI_Init(&argc,&argv);
    
    damaris_initialize("test_noaction.xml",MPI_COMM_WORLD) ;
            
    int is_client ;
    CppUnit::TextUi::TestRunner runner;
    
    runner.addTest(damaris::TestVariable1dcBlocks::GetTestSuite());
    
    damaris_start(&is_client) ;
    if (is_client > 0) {
        int rankInNode ;
        MPI_Comm_rank(MPI_COMM_WORLD ,&rankInNode); // this call requires this test to be run on a single node
        int NROW ;
        // Run for 1 iteration, 1 Damaris server, 3 clients
        if ( rankInNode == 0 ) {
            // This client will have three blocks
            int err = damaris_parameter_get("NROW", &NROW , sizeof (int)) ;
            std::vector<float> fdata1(NROW,1.0) ;
            damaris_write_block("coordinates/x2d", 0, fdata1.data()) ;
            
            std::vector<float> fdata2(NROW,2.0) ;
            damaris_write_block("coordinates/x2d", 1, fdata2.data()) ;
            
            std::vector<float> fdata3(NROW,3.0) ;
            damaris_write_block("coordinates/x2d", 2, fdata3.data()) ;
            std::cout << " rankInNode: " << rankInNode << " wrote 3 blocks "  << std::endl << std::flush;
            
        } else if ( rankInNode == 1 ) {
            // This client will have two blocks
            int err = damaris_parameter_get("NROW", &NROW , sizeof (int)) ;
            std::vector<float> fdata1(NROW,3.0) ;
            damaris_write_block("coordinates/x2d", 0, fdata1.data()) ;
            std::vector<float> fdata2(NROW,4.0) ;
            damaris_write_block("coordinates/x2d", 1, fdata2.data()) ;
            std::cout << " rankInNode: " << rankInNode << " wrote 2 blocks "  << std::endl << std::flush;
        } else if ( rankInNode == 2 ) {
            // This client will have two blocks
            int err = damaris_parameter_get("NROW", &NROW , sizeof (int)) ;
            std::vector<float> fdata1(NROW,3.0) ;
            damaris_write_block("coordinates/x2d", 0, fdata1.data()) ;
            std::cout << " rankInNode: " << rankInNode << " wrote 1 block "  << std::endl << std::flush;
        }
        damaris_end_iteration() ;
        damaris_stop();
    }
    
    bool failed = runner.run();
    damaris_finalize() ;
    MPI_Finalize();
    return !failed;
}