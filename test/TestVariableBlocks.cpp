#include <cppunit/ui/text/TestRunner.h>
#include <mpi.h>

#include "TestVariableBlocks.hpp"

using namespace std;

int main(int argc, char** argv) {
    MPI_Init(&argc,&argv);
    
    damaris_initialize("test_2dc.xml",MPI_COMM_WORLD) ;
            
    int is_client ;
    CppUnit::TextUi::TestRunner runner;
    
    runner.addTest(damaris::TestVariableBlocks::GetTestSuite());
    
    damaris_start(&is_client) ;
    if (is_client > 0) {
        int rankInNode ;
        MPI_Comm_rank(MPI_COMM_WORLD ,&rankInNode); // this call requires this test to be run on a single node
        int NROW ;
        // Run for 2 iterations
        for (int iter = 0 ; iter < 2 ; iter++) {
            if ( rankInNode == 0 ) {
                // This client will have three blocks
                int err = damaris_parameter_get("NROW", &NROW , sizeof (int)) ;
                std::vector<float> fdata1(NROW,1.0) ;
                damaris_write_block("coordinates/x2d", 0, fdata1.data()) ;
                
                std::vector<float> fdata2(NROW,2.0) ;
                damaris_write_block("coordinates/x2d", 1, fdata2.data()) ;
                
                std::vector<float> fdata3(NROW,3.0) ;
                damaris_write_block("coordinates/x2d", 2, fdata3.data()) ;
                
                if (iter == 1) {
                    std::vector<float> fdata4(NROW,4.0) ;
                    damaris_write_block("coordinates/x2d", 3, fdata4.data()) ;
                }
            } else if ( rankInNode == 1 ) {
                // This client will have two blocks
                int err = damaris_parameter_get("NROW", &NROW , sizeof (int)) ;
                std::vector<float> fdata1(NROW,3.0) ;
                damaris_write_block("coordinates/x2d", 0, fdata1.data()) ;
                std::vector<float> fdata2(NROW,4.0) ;
                damaris_write_block("coordinates/x2d", 1, fdata2.data()) ;
            }
            damaris_end_iteration() ;
        }
        damaris_stop();
    }
    
    bool failed = runner.run();
    damaris_finalize() ;
    MPI_Finalize();
    return !failed;
}