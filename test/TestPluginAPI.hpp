#include <iostream>
#include <list>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>

#include "Damaris.h"
// #include "damaris/model/Model.hpp"
// #include "damaris/model/BcastXML.hpp"


namespace damaris {


class TestPluginAPI : public CppUnit::TestFixture {
    
private:
    // int rank, size;
    // static std::shared_ptr<model::Simulation> mdl;
public:
    TestPluginAPI() {
        
    }

    virtual ~TestPluginAPI() {}

    static CppUnit::Test* GetTestSuite() {
        CppUnit::TestSuite *suiteOfTests = 
            new CppUnit::TestSuite("PluginAPI");
        
        suiteOfTests->addTest(new CppUnit::TestCaller<TestPluginAPI>(
                "Call damaris_has_plugin(DAMARIS_HAS_HDF5) API",
                &TestPluginAPI::HasPluginHDF5));
                
        suiteOfTests->addTest(new CppUnit::TestCaller<TestPluginAPI>(
                "Call damaris_has_plugin(DAMARIS_HAS_VISIT) API",
                &TestPluginAPI::HasPluginVISIT));
        
        suiteOfTests->addTest(new CppUnit::TestCaller<TestPluginAPI>(
                "Call damaris_has_plugin(DAMARIS_HAS_PARAVIEW) API",
                &TestPluginAPI::HasPluginPARAVIEW));
        
        suiteOfTests->addTest(new CppUnit::TestCaller<TestPluginAPI>(
                "Call damaris_has_plugin(DAMARIS_HAS_PYTHON) API",
                &TestPluginAPI::HasPluginPYTHON));
        
        return suiteOfTests;
    }

protected:

    void HasPluginHDF5() {
        int has_plugin ;
        int match_plugin ;
        has_plugin = damaris_has_plugin(DAMARIS_HAS_HDF5) ; 
        #ifdef HAVE_HDF5_ENABLED
            match_plugin = 1 ;
            std::cout << "HAVE_HDF5_ENABLED" << std::endl ;
        #else
            match_plugin = 0 ;
        std::cout << "Do Not HAVE_HDF5_ENABLED" << std::endl ;
        #endif
        CPPUNIT_ASSERT(has_plugin == match_plugin);
    };
       
    void HasPluginVISIT() {
        int has_plugin ;
        int match_plugin ;
        has_plugin = damaris_has_plugin(DAMARIS_HAS_VISIT) ;
        #ifdef HAVE_VISIT_ENABLED
            match_plugin = 1 ;
            std::cout << "HAVE_VISIT_ENABLED" << std::endl ;
        #else
            match_plugin = 0 ;
            std::cout << "Do Not HAVE_VISIT_ENABLED" << std::endl ;
        #endif
        CPPUNIT_ASSERT(has_plugin == match_plugin);
    };
       
    void HasPluginPARAVIEW() {
        int has_plugin ;
        int match_plugin ;
        has_plugin = damaris_has_plugin(DAMARIS_HAS_PARAVIEW);
        #ifdef HAVE_PARAVIEW_ENABLED
            match_plugin = 1 ;
            std::cout << "HAVE_PARAVIEW_ENABLED" << std::endl ;
        #else
            match_plugin = 0 ;
            std::cout << "Do Not HAVE_PARAVIEW_ENABLED" << std::endl ;
        #endif
        CPPUNIT_ASSERT(has_plugin == match_plugin);
        
    };
    
    void HasPluginPYTHON() {
        int has_plugin ;
        int match_plugin ;
        has_plugin = damaris_has_plugin(DAMARIS_HAS_PYTHON);
        #ifdef HAVE_PYTHON_ENABLED
            match_plugin = 1 ;
            std::cout << "HAVE_PYTHON_ENABLED" << std::endl ;
        #else
            match_plugin = 0 ;
            std::cout << "Do Not HAVE_PYTHON_ENABLED" << std::endl ;
        #endif
        CPPUNIT_ASSERT(has_plugin == match_plugin);
    };
} ;

}