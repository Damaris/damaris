#include <iostream>
#include <set>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>


#include "Damaris.h"
#include "damaris/model/Model.hpp"
#include "damaris/model/BcastXML.hpp"
#include "damaris/env/Environment.hpp"
#include "damaris/data/VariableManager.hpp"
#include "damaris/data/BlockIndex.hpp"
#include "damaris/util/DamarisVar.hpp"

namespace damaris {


class TestDamVarClass : public CppUnit::TestFixture {
    
private:
    static bool initialized;
    int is_client;
public:
    TestDamVarClass() {
        if(not initialized) {
            //Environment::Init("test_modify.xml",MPI_COMM_WORLD);
            damaris_initialize("test_modify.xml",MPI_COMM_WORLD) ;
            damaris_start(&is_client) ;
            initialized = true;
        }
    }

    virtual ~TestDamVarClass() {
        //Environment::Finalize();
        initialized = false;
        // MPI_Finalize();
    }

    static CppUnit::Test* GetTestSuite() {
        CppUnit::TestSuite *suiteOfTests = 
            new CppUnit::TestSuite("Variable");
       
        suiteOfTests->addTest(new CppUnit::TestCaller<TestDamVarClass>(
                "Allocate Variable in Damaris Shared Memory",
                &TestDamVarClass::AllocateDamarisVar));
                
        suiteOfTests->addTest(new CppUnit::TestCaller<TestDamVarClass>(
                "Test missmatch between XML specified type and DamarisVar class templated type",
                &TestDamVarClass::TestTypeDamarisVar));
                
                
        suiteOfTests->addTest(new CppUnit::TestCaller<TestDamVarClass>(
                "Test variable block size that was allocated",
                &TestDamVarClass::TestSizeOfDamarisVarBlock));
                
       /* suiteOfTests->addTest(new CppUnit::TestCaller<TestDamVarClass>(
                "Test variable block sizes that was allocated, but from the Damaris server",
                &TestDamVarClass::TestSizeOfDamarisVarBlockFromServer));
             */   
        return suiteOfTests;
    }

protected:
    
    void AllocateDamarisVar() {
        int rank;
        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsClient()) {
            MPI_Comm_rank(c,&rank);
            int n_client_ranks ;
            MPI_Comm_size(c,&n_client_ranks);
            // This test should involve 3 ranks total :
            // 2 client and 1 server
            CPPUNIT_ASSERT_EQUAL(2, n_client_ranks);
            damaris::model::DamarisVar<int> * life_cells ;
            int dims = 2 ;
            // Create DamarisVar, but do not allocate shared memory
            life_cells = new damaris::model::DamarisVar<int>(dims, 
                                                            {std::string("NROW"),std::string("NCOL")}, 
                                                            std::string("life/cells"), rank) ;
            // Test if variable type (int) and tempplate type (int) match (should be true)
            bool res = life_cells->TestType( std::string("life/cells") ) ;
            CPPUNIT_ASSERT_EQUAL(true, res);
            
            // specify Damaris Parameter values and allocate the shared memory
            life_cells->SetDamarisParameterAndShmem( {192, 192 } ) ;
            
            std::shared_ptr<Variable> v 
                = VariableManager::Search("life/cells");
            CPPUNIT_ASSERT(v);
            
            BlocksByIteration::iterator ibegin;
            BlocksByIteration::iterator iend;
            v->GetBlocksByIteration(0, ibegin, iend);
            int nitems_read = 0 ;
            int total_blocks = 0 ;
            // loop over the blocks of the variable
            for(auto it = ibegin; it != iend; it++) {
                std::shared_ptr<Block> b = *it;
                nitems_read = b->GetNumberOfItems() ;
                total_blocks++ ;
            }
            
            CPPUNIT_ASSERT_EQUAL(total_blocks,1);
            
            std::cout << "Test AllocateDamarisVar: " << nitems_read  << " expected: " << 192*192/2 << std::endl ;
            CPPUNIT_ASSERT_EQUAL(nitems_read,192*192/2);
            
            // See if we have an allocated the pointer
            int * mymemory = nullptr ;
            mymemory = life_cells->data_ptr() ;
            CPPUNIT_ASSERT(mymemory);
            
            res = life_cells->HasError() ;
            CPPUNIT_ASSERT_EQUAL(false,res);
            
            delete life_cells ;
            
            int res_endit = damaris_end_iteration() ;
            int dok = DAMARIS_OK ;
            CPPUNIT_ASSERT_EQUAL(dok,res_endit);
        }
    }
    
    void TestTypeDamarisVar() {
        int rank;
        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsClient()) {
            MPI_Comm_rank(c,&rank);
            // This test should involve 3 ranks total :
            // 2 client and 1 server
            int n_client_ranks ;
            MPI_Comm_size(c,&n_client_ranks);
            CPPUNIT_ASSERT_EQUAL(2, n_client_ranks);
            damaris::model::DamarisVar<double> * life_cells ;
            int dims = 2 ;
            // Create DamarisVar, but do not allocate shared memory
            life_cells = new damaris::model::DamarisVar<double>(dims, 
                                                                {std::string("NROW"),std::string("NCOL")}, 
                                                                std::string("life/cells"), rank, false) ;
            // Test if variable type (int) and tempplate type (double) match (should be false)
            bool res = life_cells->TestType( std::string("life/cells") ) ;
            CPPUNIT_ASSERT_EQUAL(false, res);
            
            delete life_cells ;
            
            int res_endit = damaris_end_iteration() ;
            int dok = DAMARIS_OK ;
            CPPUNIT_ASSERT_EQUAL(dok,res_endit);
            
        }
    }
    
    
    void TestSizeOfDamarisVarBlock() {
        int rank, n_client_ranks;
        MPI_Comm c = Environment::GetEntityComm();
        
        if(Environment::IsClient()) {
            MPI_Comm_rank(c,&rank);
            MPI_Comm_size(c,&n_client_ranks);
            // This test should involve 3 ranks total :
            // 2 client and 1 server
            CPPUNIT_ASSERT_EQUAL(2, n_client_ranks);
            damaris::model::DamarisVar<int> * life_cells ;
            int dims = 2 ;
            // Create DamarisVar, but do not allocate shared memory
            life_cells = new damaris::model::DamarisVar<int>(dims, 
                                                            {std::string("NROW"),std::string("NCOL")}, 
                                                            std::string("life/cells"), rank) ;
            // specify Damaris Parameter values and allocate the shared memory
            life_cells->SetDamarisParameterAndShmem( {192, 192 } ) ;
            
            int * mymemory = nullptr ;
            mymemory = life_cells->data_ptr() ;
            for (int i=0 ; i < (192*96) ; i++) 
                mymemory[i] = 2 ;
            
            // delete life_cells ;
            life_cells->CommitVariableDamarisShmem() ;
            //life_cells->ClearVariableDamarisShmem();
            
            int res_endit = damaris_end_iteration() ;
            int dok = DAMARIS_OK ;
            CPPUNIT_ASSERT_EQUAL(dok,res_endit);
            
            std::shared_ptr<Variable> v 
                = VariableManager::Search("life/cells");
            CPPUNIT_ASSERT(v);
            
            BlocksByIteration::iterator ibegin;
            BlocksByIteration::iterator iend;
            v->GetBlocksByIteration(2, ibegin, iend);  // assumes the first two tests have run
            int nitems_read = 0 ;
            int total_blocks = 0 ;
            // loop over the blocks of the variable - one block for each client
            for(auto it = ibegin; it != iend; it++) {
                std::shared_ptr<Block> b = *it;
                nitems_read = b->GetNumberOfItems() ;
                total_blocks++ ;
            }
            
            CPPUNIT_ASSERT_EQUAL(192*192/2,nitems_read);
            
            std::cout << "Test TestSizeOfDamarisVarBlock: total_blocks: " << total_blocks  << " expected: 1" << std::endl ;
            CPPUNIT_ASSERT_EQUAL(1,total_blocks);
            
            damaris_stop() ;
        }
    }
    
    // Currently failing to get blocks from the server
     void TestSizeOfDamarisVarBlockFromServer() {

        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsServer()) {
            int n_server_ranks;
            MPI_Comm_size(c,&n_server_ranks);
            // This test should involve 3 ranks total :
            // 2 client and 1 server
            CPPUNIT_ASSERT_EQUAL(1, n_server_ranks);
            
            std::shared_ptr<Variable> v 
                = VariableManager::Search("life/cells");
            CPPUNIT_ASSERT(v);
            
            BlocksByIteration::iterator ibegin;
            BlocksByIteration::iterator iend;
            v->GetBlocksByIteration(0, ibegin, iend);  // assumes the first three tests have run
            int nitems_read = 0 ;
            int total_blocks = 0 ;
            // loop over the blocks of the variable - one block for each client
            for(auto it = ibegin; it != iend; it++) {
                std::shared_ptr<Block> b = *it;
                nitems_read = b->GetNumberOfItems() ;
                CPPUNIT_ASSERT_EQUAL(192*192/2, nitems_read);
                total_blocks++ ;
            }
            
            std::cout << "Test TestSizeOfDamarisVarBlockFromServer: total_blocks: " << total_blocks  << " expected: 2" << std::endl ;
            CPPUNIT_ASSERT_EQUAL(2, total_blocks);
            
        }
    }

    

};

bool TestDamVarClass::initialized = false;

}
