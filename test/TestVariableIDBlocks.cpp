#include <cppunit/ui/text/TestRunner.h>
#include <mpi.h>

#include "TestVariableIDBlocks.hpp"

using namespace std;

int main(int argc, char** argv) {
    MPI_Init(&argc,&argv);
    
    damaris_initialize("test_4dc.xml",MPI_COMM_WORLD) ;
            
    int is_client ;
    CppUnit::TextUi::TestRunner runner;
    
    runner.addTest(damaris::TestVariableIDBlocks::GetTestSuite());
    
    damaris_start(&is_client) ;
    if (is_client > 0) {
        int rankInNode ;
        MPI_Comm_rank(MPI_COMM_WORLD ,&rankInNode); // this call requires this test to be run on a single node
        int NROW ;

        // This client will have one block
        int err = damaris_parameter_get("NROW", &NROW , sizeof (int)) ;
        std::vector<int> fdata1(NROW,rankInNode) ;
        damaris_write_block("life/cells", 0, fdata1.data()) ;

        damaris_end_iteration() ;
        damaris_stop();
        }
        

    
    bool failed = runner.run();
    damaris_finalize() ;
    MPI_Finalize();
    return !failed;
}