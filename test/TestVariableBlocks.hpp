#include <iostream>
#include <set>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>


#include "Damaris.h"
#include "damaris/model/Model.hpp"
#include "damaris/model/BcastXML.hpp"
#include "damaris/data/VariableManager.hpp"
#include "damaris/env/Environment.hpp"
#include "damaris/data/BlockIndex.hpp"

namespace damaris {


class TestVariableBlocks : public CppUnit::TestFixture {
    
private:
    static bool initialized;
public:
    TestVariableBlocks() {
        if(not initialized) {
            
            if (Environment::NumberOfNodes() != 1) {
                    // This test should be run on a single node only
                    initialized = false ;
                } else {
                    initialized = true;
                }
        }
    }

    virtual ~TestVariableBlocks() {
        
        initialized = false;
    }

    static CppUnit::Test* GetTestSuite() {
        CppUnit::TestSuite *suiteOfTests =  new CppUnit::TestSuite("Count Variable Total Blocks");
        
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariableBlocks>(
                "Check to see if the Damaris library could be initialized for the tests (N.B. This test needs to be run on a single node)",
                &TestVariableBlocks::Initialized));
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariableBlocks>(
                "Counting number of server processes - should be 2 and test run as mpirun -np 4 with xml file requesting 2 dedicated cores",
                &TestVariableBlocks::CountServers));
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariableBlocks>(
                "Counting blocks using 2 methods CountTotalBlocks() and CountTotalBlocksExact()",
                &TestVariableBlocks::CountBlocks));
                
        return suiteOfTests;
    }

protected:
    
    void Initialized() {
            CPPUNIT_ASSERT(initialized == true) ;
    }
    
    
    void CountServers() {
        int serverrank;
        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsServer()) {
            MPI_Comm_rank(c,&serverrank);
            std::shared_ptr<Variable> v 
                = VariableManager::Search("coordinates/x2d");
            CPPUNIT_ASSERT(v);
            
             int numServers = Environment::ServersPerNode() ;
            CPPUNIT_ASSERT_EQUAL(2, numServers) ;  // should be run with -np 4, 2 servers 2 clients
            
        }
    }
    
    void CountBlocks() {
        int serverrank;
        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsServer()) {
            MPI_Comm_rank(c,&serverrank);
            std::shared_ptr<Variable> v 
                = VariableManager::Search("coordinates/x2d");
            CPPUNIT_ASSERT(v);
            
            std::cout  << std::endl << std::flush;
            
            
            int numblocks_1 = v->CountTotalBlocks(0); // N.B. This returns a different value dependant on the server rank (returns 4 or 2)
            int numblocks_2 = v->CountTotalBlocksExact(0);
            // int id_low, id_high ;
            // v->GetIDRange(id_low, id_high) ;
            int localBlocks =  v->CountLocalBlocks(0) ;
            if (serverrank == 0 ) {
                CPPUNIT_ASSERT_EQUAL(3, localBlocks ) ; 
            } 
            if (serverrank == 1) {
                CPPUNIT_ASSERT_EQUAL(2, localBlocks ) ;
            }
            
           
            int lowest, biggest ;
            int numblocks = 0 ;
            v->GetIDRange(lowest, biggest) ;
            numblocks = biggest - lowest  + 1 ;
            std::cout << " Server: " << serverrank << " biggest: " << biggest << "  lowest : " << lowest << " numblocks: " << numblocks   << std::endl << std::flush;

            std::cout  << std::endl << std::flush ;
              
            BlocksByIteration::iterator ibegin;
            BlocksByIteration::iterator iend;
            v->GetBlocksByIteration(0, ibegin, iend);
            for(auto it = ibegin; it != iend; it++) {
                std::shared_ptr<Block> b = *it;
                int source   = b->GetSource();
                int block    = b->GetID();
                int64_t size = b->GetNumberOfItems();
                int iter       = b->GetIteration() ;
                std::cout  << " GetBlocksByIteration() Server: " << serverrank  << "  Variable: " << v->GetName() <<" Source: " << source  <<   " Iter: " << iter <<" Block: " << block << " Size: " << size  << std::endl << std::flush;
            }
            
            BlocksBySource::iterator sbegin;
            BlocksBySource::iterator send;
            v->GetBlocksBySource(0, sbegin, send);
            for(auto it = sbegin; it != send; it++) {
                std::shared_ptr<Block> b = *it;
                int source   = b->GetSource();
                int block    = b->GetID();
                int64_t size = b->GetNumberOfItems();
                int iter       = b->GetIteration() ;
                std::cout  << " GetBlocksBySource() Server: " << serverrank  << "  Variable: " << v->GetName() <<" Source: " << 0  << " Iter: " << iter << " Block: " << block << " Size: " << size  << std::endl << std::flush;
            }
            
            
            // Using GetBlocksById we can specify each id in range 0,.. x and see if the server has a block relating to it.
            // The range of begin to end in this query can be a maximum of 1 block.
            BlocksById::iterator begin ;
            BlocksById::iterator end ;
            int s0_total = 0;
            int s1_total = 0;
            v->GetBlocksById(0, begin, end) ;
            int numblock_for_id = 0 ;
            for(auto it = begin; it != end; it++) {
                if ( (*it)->GetIteration() == 0 )
                    numblock_for_id++ ;
            }
            if (serverrank == 0 ) {
                s0_total += numblock_for_id ;
                std::cout << " server 0 " << " id 0 " << numblock_for_id   << std::endl << std::flush ;
                CPPUNIT_ASSERT_EQUAL(1 , numblock_for_id ) ;
            } else {
                s1_total += numblock_for_id ;
                std::cout << " server 1 " << " id 0 " << numblock_for_id   << std::endl << std::flush ;
                CPPUNIT_ASSERT_EQUAL(1 , numblock_for_id ) ;
            }
            
            v->GetBlocksById(1, begin, end) ;
            numblock_for_id = 0 ;
            for(auto it = begin; it != end; it++) {
                if ( (*it)->GetIteration() == 0 )
                    numblock_for_id++ ;
            }
            if (serverrank == 0 ) {
                s0_total += numblock_for_id ;
                std::cout << " server 0 " << " id 1 " << numblock_for_id  << std::endl << std::flush;
                CPPUNIT_ASSERT_EQUAL(1 , numblock_for_id ) ;
            }  else {
                s1_total += numblock_for_id ;
                std::cout << " server 1 " << " id 1 " << numblock_for_id  << std::endl << std::flush;
                CPPUNIT_ASSERT_EQUAL(1 , numblock_for_id ) ;
            }
            
            // There should be 1 blocks for id=2 on server 0 and 0 on server 1
            v->GetBlocksById(2, begin, end) ;
            numblock_for_id = 0 ;
            for(auto it = begin; it != end; it++) {
                if ( (*it)->GetIteration() == 0 )
                    numblock_for_id++ ;
            }
            if (serverrank == 0) {
                s0_total += numblock_for_id ;
                std::cout << " server 0 " << " id 2 "  << numblock_for_id  << std::endl << std::flush;
                CPPUNIT_ASSERT_EQUAL(1 , numblock_for_id ) ;
            }  else {
                s1_total += numblock_for_id ;
                std::cout << " server 1 " << " id 2 "  << numblock_for_id  << std::endl << std::flush;
                CPPUNIT_ASSERT_EQUAL(0 , numblock_for_id ) ;
            }
            
            if (serverrank == 0) {
               CPPUNIT_ASSERT_EQUAL(localBlocks , s0_total ) ;
            }  else {
               CPPUNIT_ASSERT_EQUAL(2, s1_total ) ;
            }
            
            
            // CPPUNIT_ASSERT_EQUAL(numblocks_1, 4 ) ;
            CPPUNIT_ASSERT_EQUAL(5, numblocks_2 ) ;
        }
    }
};

bool TestVariableBlocks::initialized = false;

}
