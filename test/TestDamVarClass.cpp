#include <cppunit/ui/text/TestRunner.h>
#include <mpi.h>

#include "TestDamVarClass.hpp"

using namespace std;

int main(int argc, char** argv) {
	MPI_Init(&argc,&argv);
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(damaris::TestDamVarClass::GetTestSuite());
	bool failed = runner.run();
    damaris_finalize() ;
	MPI_Finalize();
	return !failed;
}