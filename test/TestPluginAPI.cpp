#include <cppunit/ui/text/TestRunner.h>
#include <mpi.h>

#include "TestPluginAPI.hpp"

using namespace std;

int main(int argc, char** argv) {

    CppUnit::TextUi::TestRunner runner;
    runner.addTest(damaris::TestPluginAPI::GetTestSuite());
    bool failed = runner.run();

    return !failed;
}
