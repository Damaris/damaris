#include <iostream>
#include <set>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>


#include "Damaris.h"
#include "damaris/model/Model.hpp"
#include "damaris/model/BcastXML.hpp"
#include "damaris/data/VariableManager.hpp"
#include "damaris/env/Environment.hpp"
#include "damaris/data/BlockIndex.hpp"

namespace damaris {


class TestVariableIDBlocks : public CppUnit::TestFixture {
    
private:
    static bool initialized;
public:
    TestVariableIDBlocks() {
        if(not initialized) {
            if (Environment::NumberOfNodes() != 1) {
                // This test should be run on a single node only
                initialized = false ;
            } else {
                initialized = true;
            }
        }
    }

    virtual ~TestVariableIDBlocks() {
        
        initialized = false;
    }

    static CppUnit::Test* GetTestSuite() {
        CppUnit::TestSuite *suiteOfTests =  new CppUnit::TestSuite("Count Variable Total Blocks");
        
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariableIDBlocks>(
                "Check to see if the Damaris library could be initialized for the tests (N.B. This test needs to be run on a single node)",
                &TestVariableIDBlocks::Initialized));
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariableIDBlocks>(
                "Testing var->GetIDRange()  N.B. should be run with -np 8, 4 servers 4 clients  ",
                &TestVariableIDBlocks::CountBlocks));
                
        return suiteOfTests;
    }

protected:
    
    void Initialized() {
            CPPUNIT_ASSERT(initialized == true) ;
    }
    
    void CountBlocks() {
        int serverrank;
        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsServer()) {
            MPI_Comm_rank(c,&serverrank);
            std::shared_ptr<Variable> v 
                = VariableManager::Search("life/cells");
            CPPUNIT_ASSERT(v);

            std::cout  << std::endl << std::flush;
            
            int numServers = Environment::ServersPerNode() ;
            CPPUNIT_ASSERT_EQUAL(4, numServers) ;  // should be run with -np 8, 4 servers 4 clients

            BlocksByIteration::iterator ibegin;
            BlocksByIteration::iterator iend;
            v->GetBlocksByIteration(0, ibegin, iend);
            for(auto it = ibegin; it != iend; it++) {
                std::shared_ptr<Block> b = *it;
                int source   = b->GetSource();
                int block    = b->GetID();
                int64_t size = b->GetNumberOfItems();
                std::cout << " Server: " << serverrank  << "  Variable: " << v->GetName() <<" Source: " << source  << " Block: " << block << " Size: " << size  << std::endl << std::flush;
            }

            int lowest, biggest ;
            int numblocks = 0 ;

            v->GetIDRange(lowest, biggest) ;
            numblocks = biggest - lowest  + 1 ;
            std::cout << " Server: " << serverrank << ": biggest: " << biggest << "  lowest: " << lowest << " numblocks: " << numblocks   << std::endl << std::flush;
            CPPUNIT_ASSERT_EQUAL(1, numblocks) ; // 1 block per server

            std::cout  << std::endl << std::flush ;
           
            
        }
    }
};

bool TestVariableIDBlocks::initialized = false;

}
