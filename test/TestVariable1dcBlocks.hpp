#include <iostream>
#include <set>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>


#include "Damaris.h"
#include "damaris/model/Model.hpp"
#include "damaris/model/BcastXML.hpp"
#include "damaris/data/VariableManager.hpp"
#include "damaris/env/Environment.hpp"
#include "damaris/data/BlockIndex.hpp"

namespace damaris {


class TestVariable1dcBlocks : public CppUnit::TestFixture {
    
private:
    static bool initialized;
public:
    TestVariable1dcBlocks() {
        if(not initialized) {
            
            if (Environment::NumberOfNodes() != 1) {
                    // This test should be run on a single node only
                    initialized = false ;
                } else {
                    initialized = true;
                }
        }
    }

    virtual ~TestVariable1dcBlocks() {
        
        initialized = false;
    }

    static CppUnit::Test* GetTestSuite() {
        CppUnit::TestSuite *suiteOfTests =  new CppUnit::TestSuite("Count Variable Total Blocks");
        
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariable1dcBlocks>(
                "Check to see if the Damaris library could be initialized for the tests (N.B. This test needs to be run on a single node)",
                &TestVariable1dcBlocks::Initialized));
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariable1dcBlocks>(
                "Counting number of server processes - should be 1 and test run as mpirun -np 4 with xml file requesting 1 dedicated cores",
                &TestVariable1dcBlocks::CountServers));
        suiteOfTests->addTest(new CppUnit::TestCaller<TestVariable1dcBlocks>(
                "Counting blocks using 2 methods CountTotalBlocks() and CountTotalBlocksExact()",
                &TestVariable1dcBlocks::CountBlocks));
                
        return suiteOfTests;
    }

protected:
    
    void Initialized() {
            CPPUNIT_ASSERT(initialized == true) ;
    }
    
    
    void CountServers() {
        int serverrank;
        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsServer()) {
            MPI_Comm_rank(c,&serverrank);
            std::shared_ptr<Variable> v 
                = VariableManager::Search("coordinates/x2d");
            CPPUNIT_ASSERT(v);
            
             int numServers = Environment::ServersPerNode() ;
            CPPUNIT_ASSERT_EQUAL(1, numServers) ;  // should be run with -np 4, 1 servers 3 clients
            
        }
    }
    
    void CountBlocks() {
        int serverrank;
        MPI_Comm c = Environment::GetEntityComm();
        if(Environment::IsServer()) {
            MPI_Comm_rank(c,&serverrank);
            std::shared_ptr<Variable> v 
                = VariableManager::Search("coordinates/x2d");
            CPPUNIT_ASSERT(v);
            
            std::cout  << std::endl << std::flush;
            
            int iteration = 0 ;
            int numblocks_1 = v->CountTotalBlocks(iteration); // N.B. This can return a different value dependant on the server rank 
            int numblocks_2 = v->CountTotalBlocksExact(iteration);
            int localBlocks =  v->CountLocalBlocks(iteration) ;

           
            int lowest, biggest ;
            int numblocks = 0 ;
            v->GetIDRange(lowest, biggest) ;
            numblocks = biggest - lowest  + 1 ;
            std::cout << " Server: " << serverrank << " biggest: " << biggest << "  lowest : " << lowest << " block range: " << numblocks   << std::endl << std::flush;

            std::cout  << std::endl << std::flush ;
            
            int source_range_low, source_range_high ;
            v->GetSourceRange(source_range_low, source_range_high) ;
            int source_total      = source_range_high - source_range_low + 1 ;
            
            std::vector<int>   vect_count_source_blocks(source_total, 0) ;
            
            BlocksByIteration::iterator ibegin;
            BlocksByIteration::iterator iend;
            v->GetBlocksByIteration(0, ibegin, iend);
            for(auto it = ibegin; it != iend; it++) {
                std::shared_ptr<Block> b = *it;
                int source   = b->GetSource();
                int block    = b->GetID();
                int source_offset = source - source_range_low ;
                std::cout  << " GetBlocksByIteration() Server: " << serverrank  << "  Variable: " << v->GetName() <<" Source: " << source  << " Iter: " << 0  << " Block: " << block  << std::endl << std::flush;
                
                vect_count_source_blocks[source_offset] +=  1 ;
            }
            
            CPPUNIT_ASSERT_EQUAL(3, source_total ) ;  // there should be 3 clients (aka sources)
            CPPUNIT_ASSERT_EQUAL(3, vect_count_source_blocks[0] ) ; // client 0 should have 3 blocks
            CPPUNIT_ASSERT_EQUAL(2, vect_count_source_blocks[1] ) ; // client 1 should have 2 blocks
            CPPUNIT_ASSERT_EQUAL(1, vect_count_source_blocks[2] ) ; // client 2 should have 1 blocks
            
            CPPUNIT_ASSERT_EQUAL(6, localBlocks ) ; // local blocks should be the same as total blocks as we only have 1 Damaris server
            CPPUNIT_ASSERT_EQUAL(6, numblocks_1 ) ; // should be total of 6 blocks in this iteration (iteration 0)
            CPPUNIT_ASSERT_EQUAL(6, numblocks_2 ) ; // should be total of 6 blocks in this iteration (iteration 0)
        }
    }
};

bool TestVariable1dcBlocks::initialized = false;

}
