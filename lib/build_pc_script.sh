#! /bin/bash

CMAKE_BINARY_DIR=$1

if [[ -f "$CMAKE_BINARY_DIR/lib/CMakeFiles/damaris.dir/link.txt" ]] ; then

    FULL_LINK=$(cat ${CMAKE_BINARY_DIR}/lib/CMakeFiles/damaris.dir/link.txt)
    # echo ${FULL_LINK}
    SELECT_LINK=$(echo ${FULL_LINK} | sed  -e 's/.*:\(.*\)-Wl,-rpath.*/\1/')
    # echo ""

    
    # Remove duplicates by making a map of key-values
    declare -A words
    IFS=" "
    for w in ${SELECT_LINK}; do
      words+=( [$w]="" )
    done

    DEDUP=$(echo ${!words[@]})
    echo "-- Adding extracted libraries to damaris.pc file"
    sed -i "s|#LIBS_USING_BUILD_PC_SCRIPT#|${DEDUP}|g" lib/damaris.pc
    
else
    echo "Daamris PC file configuration error: Could not find the CMake link.txt file"
fi


