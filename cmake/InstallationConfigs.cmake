# note that it is not the CMAKE_INSTALL_PREFIX what we are checking here,
# but the CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT
if(DEFINED CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    message(
            STATUS
            "CMAKE_INSTALL_PREFIX is not set\n"
            "Default value: ${CMAKE_INSTALL_PREFIX}\n"
            "Will set it to ${CMAKE_SOURCE_DIR}/install"
    )
    set(CMAKE_INSTALL_PREFIX
            "${CMAKE_SOURCE_DIR}/install"
            CACHE PATH "Where the Damaris library will be installed to" FORCE
    )
else()
    message(
            STATUS
            "CMAKE_INSTALL_PREFIX was already set\n"
            "Current value: ${CMAKE_INSTALL_PREFIX}"
    )
endif()


# install the target and create export-set
# Already done in lib/
#install(
        #TARGETS ${DAMARIS_NAME_LOWER}
        #EXPORT "${DAMARIS_NAME_CAMEL}Targets"
        #COMPONENT ${DAMARIS_NAME_LOWER} # must be here, not any line lower
        # these get default values from GNUInstallDirs, no need to set them
        #RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR} # bin
        #LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR} # lib
        #ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR} # lib
        # except for public headers, as we want them to be inside a library folder
        #PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${DAMARIS_NAME_LOWER} # include/damaris
        #INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} # include
#)


#----------------------------------------------------------------------------#
# Packaging
#----------------------------------------------------------------------------#

# these are cache variables, so they could be overwritten with -D,
set(
        CPACK_PACKAGE_NAME ${DAMARIS_NAME_LOWER}
        CACHE STRING "The resulting package name"
)
# which is useful in case of packing only selected components instead of the whole thing
set(
        CPACK_PACKAGE_DESCRIPTION_SUMMARY "Damaris is a library for data managmement and visualisation that provides asynchronous, in situ processing capabilities to MPI based simulation codes."
        CACHE STRING "Package description for the package metadata"
)
set(CPACK_PACKAGE_HOMEPAGE_URL ${DAMARIS_PACKAGE_URL})
set(CPACK_PACKAGE_VENDOR "KerData Team - Inria Rennes, France")


set(CPACK_VERBATIM_VARIABLES YES)

set(CPACK_PACKAGE_INSTALL_DIRECTORY ${DAMARIS_NAME_LOWER})
#set(CPACK_PACKAGE_ICON "https://project.inria.fr/damaris/files/2018/10/cropped-Damaris-Icon-512x512-32x32.png")#${DAMARIS_SOURCE_DIR}/damaris-icon.png)
set(CPACK_OUTPUT_FILE_PREFIX "${CMAKE_SOURCE_DIR}/_packages")
set(CPACK_STRIP_FILES YES)

#The installation path directory should have 0755 permissions
set(
        CPACK_INSTALL_DEFAULT_DIRECTORY_PERMISSIONS
        OWNER_READ OWNER_WRITE OWNER_EXECUTE
        GROUP_READ GROUP_EXECUTE
        WORLD_READ WORLD_EXECUTE
)

#set(CPACK_PACKAGING_INSTALL_PREFIX "/opt/damaris/${CMAKE_PROJECT_VERSION}")#/${CMAKE_PROJECT_VERSION}")
set(CPACK_PACKAGING_INSTALL_PREFIX "/usr/lib/damaris/${CMAKE_PROJECT_VERSION}")#/${CMAKE_PROJECT_VERSION}")

set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})

set(CPACK_RESOURCE_FILE_LICENSE "${DAMARIS_SOURCE_DIR}/LICENSE.txt")
set(CPACK_RESOURCE_FILE_README "${DAMARIS_SOURCE_DIR}/README.md")

if (WIN32 OR MINGW)
elseif (APPLE)
else ()
    set(CPACK_PACKAGE_CONTACT "jean-etienne.ndamlabin-mboula.inria.fr")
    set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Etienne Ndamlabin <${CPACK_PACKAGE_CONTACT}>")
    set (CPACK_DEBIAN_PACKAGE_SECTION "Libraries")

    # package name for deb. If set, then instead of damaris-1.11.1-Linux.deb
    # you'll get something like damaris_1.11.1_amd64.deb
    set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)
    set(CPACK_RPM_FILE_NAME RPM-DEFAULT)
    # that is if you want every group to have its own package,
    # although the same will happen if this is not set (so it defaults to ONE_PER_GROUP)
    # and CPACK_DEB_COMPONENT_INSTALL is set to YES
    set(CPACK_COMPONENTS_GROUPING ALL_COMPONENTS_IN_ONE)#ONE_PER_GROUP)
    # with this we will be able to pack only specified component
    set(CPACK_DEB_COMPONENT_INSTALL YES)
    set(CPACK_RPM_COMPONENT_INSTALL YES)
    # list dependencies
    set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS YES)

    #set(CPACK_GENERATOR "DEB") # Use cpack -G DEB
    #set(CPACK_GENERATOR "RPM") # Use cpack -G RPM
endif ()

include(CPack)

# optionally, you can add various meta information to the components defined in INSTALL
# cpack_add_component(some-application
#     DISPLAY_NAME "Some application"
#     DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION_SUMMARY}"
#     #GROUP group1
# )
# cpack_add_component(SomeLibrary
#     DISPLAY_NAME "Some library"
#     DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION_SUMMARY}"
#     #GROUP group1
# )
# cpack_add_component(AnotherLibrary
#     DISPLAY_NAME "Another library"
#     DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION_SUMMARY}"
#     #GROUP group2
# )
# you can also put them into groups
#cpack_add_component_group(group1)
#cpack_add_component_group(group2)

# can be also set as -DCPACK_COMPONENTS_ALL="AnotherLibrary"
#set(CPACK_COMPONENTS_ALL "AnotherLibrary")
message(STATUS "Components to pack: ${CPACK_COMPONENTS_ALL}")