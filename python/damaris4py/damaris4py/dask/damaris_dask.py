__all__ = ['return_scheduler_filename', 'return_iteration', 'return_magic_number', 'return_scalar_from_position', 'return_numpy_array', 'return_dask_array']


def return_separator(p0, p1, dim, instr, first_elem):         
    resstr = ''
    if p1[dim] > p0[dim]:
        if first_elem == False:
            resstr = instr + ","
        else:
            first_elem = False
            resstr = instr + ","
    elif p1[dim] <= p0[dim]:
        if dim > 0:
            instr += "]\n"
            first_elem = False
            resstr2, first_elem = return_separator(p0, p1, dim-1, instr, first_elem)
            first_elem = True
            resstr += resstr2 + "["
    return resstr, first_elem

    
def return_dask_block_layout_v4(mylist_sorted, dask_client_name_str, varname):
    """
        return_dask_block_layout_v4(mylist_sorted, dask_client_name_str)
        
        returns string with dask.block() nested lists of published datasets 
        with keys of form: varname + '_' +p1_pub_name + '_' + p1_key
        
        e.g. client.datset['PRESSURE_S0_I1_<simulation_magic_number>_P0_B0']
    """
    tup_len = len(mylist_sorted[0][2])
    
    dask_str='data = '
    for dim in reversed(range(0, tup_len)):
        dask_str+= '[' 
    
    # initialize inputs
    first_elem = False         
    p0_pub_name = mylist_sorted[0][0]
    p0_key = mylist_sorted[0][1]
    p0_tpl = mylist_sorted[0][2]
    # add first tuple to list
    p0_full_str = dask_client_name_str + '.datasets[\''+ varname + '_' + p0_pub_name + '_' + p0_key + '\']' 
    dask_str+=str(p0_full_str)
    t1 = 1
    while t1 < len(mylist_sorted):            
        p1_pub_name = mylist_sorted[t1][0]
        p1_key = mylist_sorted[t1][1]
        p1_tpl = mylist_sorted[t1][2]
        # add first tuple to list
        p1_full_str = dask_client_name_str + '.datasets[\''+ varname + '_'  + p1_pub_name + '_' + p1_key + '\']' 
        dim = tup_len-1
        sepStr , first_elem =  return_separator(p0_tpl, p1_tpl, dim, '',first_elem) 

        dask_str += sepStr + str(p1_full_str)
        t1 = t1 + 1
        p0_tpl = p1_tpl
      
    for dim in reversed(range(0, tup_len)):
        dask_str+= ']'
    
    # dask_str+= ')'
    
    return dask_str
    
def return_scheduler_filename(DamarisData):
    """
        return_scheduler_filename(DamarisData)
        
        DamarisData: Is the dictionary that describes the Damaris data that is created 
                     by the Damaris server processes. It contains a dictionary 'dask_env'
                     that contains the Dask scheduler name.
                     
        If this scheduler_file exists (is not an empty string) then a Dask 
        scheduler was found (This access has been tested on the Damaris server C++ Python).
    """
    dask_dict       = DamarisData['dask_env']
    
    scheduler_file  = dask_dict['dask_scheduler_file']  
    return scheduler_file
    
        
def return_iteration(DamarisData):
    """
        return_iteration(DamarisData)
        
        DamarisData: Is the dictionary that describes the Damaris data that is created 
                     by the Damaris server processes. It contains a dictionary 'iteration_data'
                     that contains meta-data about the current iteration of the simulation. 
                     It is produced by Damaris C++ method PyAction::PassDataToPython()
                     
        Returns the iteration value the from Damaris perspective
        i.e. It is the number of times damaris_end_iteration() has been called.
    """

    iter_dict    = DamarisData['iteration_data']   
    iteration    = iter_dict['iteration']
    return iteration
        
        
        
def return_magic_number(DamarisData):
    """
        return_magic_number(DamarisData)
        
        DamarisData: Is the dictionary that describes the Damaris data that is created 
                     by the Damaris server processes. It contains a dictionary 'damaris_env'
                     that contains meta-data about the current simulation.
                     
        Returns a unique string that identifies the instance of the simulation.
    """
    damaris_dict = DamarisData['damaris_env']
    magic_num    = damaris_dict['simulation_magic_number']
    return magic_num


def return_scalar_from_position(DamarisData, varname, pos=(0), client_rank=0, block_number=0 ):
    """
        return_scalar_from_position(DamarisData, varname, pos=(0), client_rank=0, block_number=0)
        
        DamarisData:   Is the dictionary that describes the Damaris data that is created 
                       by the Damaris server processes. It contains a dictionary 'iteration_data'
                       that contains meta-data about the current simulation.
                     
        varname:      The Damaris variable name - matches names used in the Damaris XML file and used 
                      for damaris_write()
                    
        pos     :     Is the specific index into the NumPy array from which to return the value.
        
        client_rank:  Specifies the Damaris client rank where the NumPy data was produced. 
                      N.B. As data is distributed, only certain Damaris server processes will have 
                      access to particular Damaris client data - they both need to reside on the
                      same host. 
        
        block_number: Specifies the block (or the domain) written by damaris_write_block() or 0 if
                      only damaris_write() has been used.
                     
        Returns single value from the specified NumPy array
    """
    iter_dict    = DamarisData['iteration_data']   
    try:
        if varname in iter_dict.keys():
            array_of_interest =  iter_dict[varname]         
            # Only Process/rank 0 of the Damaris clients (P0) will hold the value
            key = 'P' + str(client_rank) + '_B' + str(block_number)
            if key in array_of_interest['numpy_data'].keys():
                res_val = array_of_interest['numpy_data'][key][pos]
                return res_val
    except IndexError as err:
        print('damaris4py Error: IndexError: ', err)
    except KeyError as err: 
        print('damaris4py: KeyError: No damaris client or block data ', err)

        
def return_numpy_array(DamarisData, varname, client_rank=0, block_number=0 ):
    """
        return_numpy_array(DamarisData, varname, client_rank=0, block_number=0)
        
        DamarisData:   Is the dictionary that describes the Damaris data that is created 
                       by the Damaris server processes. It contains a dictionary 'iteration_data'
                       that contains meta-data about the current simulation.
                     
        varname:      The Damaris variable name - matches names used in the Damaris XML file and used 
                      for damaris_write()
                    
        client_rank:  Specifies the Damaris client rank where the NumPy data was produced. 
                      N.B. As data is distributed, only certain Damaris server processes will have 
                      access to particular Damaris client data - they both need to reside on the
                      same host. 
        
        block_number: Specifies the block (or the domain) written by damaris_write_block() or 0 if
                      only damaris_write() has been used.
                     
        Returns the full NumPy array specified by the client rank and block number
    """
    iter_dict    = DamarisData['iteration_data']   
    try:
        if varname in iter_dict.keys():
            array_of_interest =  iter_dict[varname]         
            key = 'P' + str(client_rank) + '_B' + str(block_number)
            if key in array_of_interest['numpy_data'].keys():
                res_val = array_of_interest['numpy_data'][key]
                return res_val
            else:
                return None
    except KeyError as err: 
        print('damaris4py.ReturnAsNumpy() : KeyError: No damaris client or block data ', err)

    
def return_dask_array_v5(DamarisData, client, varname, server_comm, print_array_component=False ):
    """
        return_dask_array(DamarisData, varname, server_comm block_number=0)
        
        DamarisData:   Is the dictionary that describes the Damaris data that is created 
                       by the Damaris server processes. It contains a dictionary 'iteration_data'
                       that contains meta-data about the current simulation.
                     
        varname:      The Damaris variable name - matches names used in the Damaris XML file and used 
                      for damaris_write()
                    
        server_comm:  The simulation communicator collecting Damaris servers.
        
        print_array_component: Specifies whether to print to stdout the array structure or not.
                     
        Returns a created Dask dask.array of the distributed simulation data
    """
    import dask.array as da
    import time
    try:
        rank = server_comm.Get_rank()
        # The iter_dict dictionary is set up in PyAction::PassDataToPython() and contains
        # the positional data of where the data comes from and also the NumPy data array of
        # the data section istself.
        iter_dict    = DamarisData['iteration_data']
        mylist_local = iter_dict[varname]['sort_list']
        mydatadict = iter_dict[varname]['numpy_data']
        
        convert_to_scattered_futures_worked_v5(mylist_local, mydatadict, varname, client)
        str_plus_list  =  return_dask_string_v5(DamarisData, 'client', varname, server_comm, print_time=print_array_component ) 

        # Only rank 0 returns the dask.array string, the others return None
        try:
            dask_str = str_plus_list[0]
        except TypeError as err: 
            dask_str = None

        if (dask_str is not None):
            # global data
            if print_array_component:
                print('Damaris dask_str: ', varname, ':\n', dask_str)
                
            
            exec( "global data; " + dask_str )
           
            # This creates the Dask dask.array
            x = da.block( data )
            
            return x  # return the dask.array
        else:
            return None
            
    except KeyError as err: 
        print('Python ERROR: damaris_dask.return_dask_array_v4() KeyError: No damaris data of name: ', err)
        

# Version 5 - similar to v1 .result() is in the datasets of the list used for da.block() instead of the client.persist() call in this function  
def convert_to_scattered_futures_worked_v5(mysort_data_local, mydata_dict_local, varname, client):
    """
    convert_to_scattered_futures_worked_v5(mysort_data_local, mydata_dict_local, vaname, dask_client)
    
    mysort_data_local: The data structure used to pass the position information for the NumPy data in the global array, and it
                       specifies a unique strings to distiguish this data from any other array produced by a simulation.
                       The varname concatenated with the two string list items are used to access the data and name the 
                       client.dataset[] dictionary result from scattering the data to the Dask cluster. 
                       e.g. of a single list item:
                       DamarisData['iteration_data']['varname']['sort_data'] ==  [['S0_I1_<simulation_magic_number>', 'P0_B0', [ 0, 9, 12 ]],...]
                       
    varname           : Is the name of Damaris variable as defined in the XML file and usied in damaris_write()
    
    mydata_dict_local : This is the NumPy data, located in the list as seen here:                     
                           DamarisData['iteration_data'][varname]['numpy_data']['PX_BY']
    
    dask_client       : A Dask client used to scatter data futures, create a dask.array from and then persist, for later access in building a dask.array.
    
    The result of this function is to create shared datasets (Dask arrays, one array for each simulation Numpy block) 
    that will be available to other Dask client processes.
    The shared data is available via the dictionary: client.datasets[key]
    The key is made from: <varname>_S0_I1_<simulation_magic_number>_P0_B0', 
        where:  
            <varname> is the Damaris variable name
            S is the Damaris server process
            I is the iteration number
            P is the Damaris client process
            B is the block of data from the client process
    N.B. The key must match what is used in: return_dask_block_layout_v4()
    """
    
    import dask.array as da
    from dask.distributed import wait
    # mysort_data_local == ['sort_data'] ==  
    # [['S0_I1_<simulation_magic_number>',  'P0_B0',        [ 0, 9, 12 ]],...]
    #  [ list_item[0]                    ,  list_item[1] ,  list_item[2]]
    for list_item in mysort_data_local:
        p0_key      = varname + '_' + list_item[0] + '_' +list_item[1]  # must match what is used in return_dask_block_layout_v4
        np_ar  = mydata_dict_local[list_item[1]]
        np_fut = client.scatter(np_ar)
        wait(np_fut)
        da_fd  = da.from_delayed(np_fut, np_ar.shape, dtype=np_ar.dtype)
        wait(da_fd)
        da_fd  = client.persist(da_fd)
        # wait(da_fd)
        client.datasets[p0_key] = da_fd
        
def convert_to_scattered_future(DamarisData, client, varname, print_time=False ):
    """
        convert_to_scattered_future(DamarisData, varname, server_comm block_number=0)
        
        DamarisData:   Is the dictionary that describes the Damaris data that is created 
                       by the Damaris server processes. It contains a dictionary 'iteration_data'
                       that contains meta-data about the current simulation.
                     
        varname:       The Damaris variable name - matches names used in the Damaris XML file and used 
                       for damaris_write()
                    
        server_comm:   The simulation communicator collecting Damaris servers.
        
        print_time:    Specifies whether to print to stdout the array structure or not.
                     
        Returns a created Dask dask.array of the distributed simulation data
    """
    import dask.array as da
    import time
    import datetime
    try:
        iteration = return_iteration(DamarisData)
        # The iter_dict dictionary is set up in PyAction::PassDataToPython() and contains
        # the positional data of where the data comes from and also the NumPy data array of
        # the data section istself.
        iter_dict    = DamarisData['iteration_data']
        mylist_local = iter_dict[varname]['sort_list']
        mydatadict = iter_dict[varname]['numpy_data']
        
        stime = time.perf_counter() 
        convert_to_scattered_futures_worked_v5(mylist_local, mydatadict, varname, client)
        
        ftime = time.perf_counter()
        if print_time:
            ct = datetime.datetime.now()
            print( '[', ct, f']: DASKTIME: damaris_dask.convert_to_scattered_futures_worked_v5.gather took {ftime - stime:0.8f} seconds')
            
    except KeyError as err: 
        print('Python ERROR: damaris_dask.convert_to_scattered_future() KeyError: No damaris data of name: ', err)
        

def return_dask_string_v5(DamarisData, client_as_str, varname, server_comm, print_time=False ):
    """
        return_dask_string(DamarisData, client, varname, server_comm , print_time)
        This version does not scatter the numpy array data first - you must call convert_to_scattered_future() first
        
        DamarisData:   Is the dictionary that describes the Damaris data that is created 
                       by the Damaris server processes. It contains a dictionary 'iteration_data'
                       that contains meta-data about the current simulation.
                       
        client_as_str  The string version of the name of the client variable being used -
                       e.g. if you obtain a client using:
                                myclinet = Client(scheduler_file='dask_scehd.json')
                       then, client_as_str will be 'myclient' as a string
                     
        varname:      The Damaris variable name - matches names used in the Damaris XML file and used 
                      for damaris_write()
                    
        server_comm:  The simulation communicator collecting Damaris servers.
        
        print_time:   Specifies whether to print the time for the routine.
                     
        Returns a created string that can be used in the da.block() method to create a dask.array from the distributed and shared simulation data. Also returns a list of the keys used to name the client.datasets[ ] dictionary items, 
        which is useful to delete them.
    """
    import dask.array as da
    import time
    try:
        rank = server_comm.Get_rank()
        # The iter_dict dictionary is set up in PyAction::PassDataToPython() and contains
        # the positional data of where the data comes from and also the NumPy data array of
        # the data section istself.
        iter_dict    = DamarisData['iteration_data']
        mylist_local = iter_dict[varname]['sort_list']
        mydatadict = iter_dict[varname]['numpy_data']
        
        stime = time.perf_counter()
        # This MPI gather collects together a list of lists of the sort data information 
        # used to reconstruct the Dask array from the Numpy pieces      
        mylist = server_comm.gather(mylist_local, root=0) 

        list_merged = []  
        scatter_key_list = []
        if rank == 0:
            # merge into a single list
            for list_itm in mylist:
                for p_k_bo in list_itm:
                    # p_k_bo is a list of: p_k_bo[0]='string' p_k_bo[1]='string' p_k_bo[3]=list[integers]
                    # and the list[integers] are the offsets into a Damaris array, set using damaris_set_position() API call
                    tlist = [ p_k_bo[0], p_k_bo[1], tuple( p_k_bo[2]) ]
                    ttup = (tuple(tlist))
                    list_merged.append( ttup )
                    scatter_key_list.append(p_k_bo[0]+'_'+ p_k_bo[1])

            mylist_sorted = sorted(list_merged, key=lambda tup: tup[2])

            dask_str = return_dask_block_layout_v4(mylist_sorted, client_as_str, varname)
            ftime = time.perf_counter()
            
            if print_time:
                print(str(rank), f'damaris_dask.return_dask_string_v5, took {ftime - stime:0.8f} seconds')
                
            return [dask_str, scatter_key_list]
   
        else:
            return None
    except KeyError as err: 
        print('Python ERROR: damaris_dask.return_dask_array_v3() KeyError: No damaris data of name: ', err)
        
        
def return_dask_array(DamarisData, client, varname, server_comm, print_array_component=False ):
    return return_dask_array_v5(DamarisData, client, varname, server_comm, print_array_component )
