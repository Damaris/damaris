/**************************************************************************
This file is part of Damaris.

Damaris is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Damaris is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Damaris.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include <sstream>
#include <cassert>
#include "util/Debug.hpp"
#include "storage/HDF5Store.hpp"
#include "env/Environment.hpp"





namespace damaris {

    HDF5Store::HDF5Store(const model::Store &mdl)
            : Store(mdl) {

        fileMode_= FilePerCore;
        xdmfMode_= NoIteration;

        if (!ReadKeyValues(mdl))
            ERROR("Bad KeyValue Parameters for HDF5 Store");
    }

    bool HDF5Store::ReadKeyValues(const model::Store &mdl) {
        model::Store::option_const_iterator op = mdl.option().begin();

        for (; op != mdl.option().end(); op++) {
            //std::cout << "Option \"" << op->key() << "\" has value \"" << (string)(*op) << "\"" << std::endl;


            if (op->key()->compare("FileMode") == 0) {
                if (op->compare("FilePerCore") == 0) {
                    fileMode_ = FilePerCore;
                } else if (op->compare("FilePerNode") == 0) {
                    fileMode_ = FilePerNode;
                    ERROR("FilePerNode mode is not supported in this version!");
                    return false;
                } else if (op->compare("Collective") == 0) {
                    fileMode_ = Collective;
                } else if (op->compare("CollectiveTest") == 0) {
                    fileMode_ = CollectiveTest;
                }  else {
                    ERROR("HDFStore FileMode is undefined. Available modes are \"FilePerCore\" and \"Collective\"");
                    return false;
                }
            } else if (op->key()->compare("XDMFMode") == 0) {
                if (op->compare("NoIteration") == 0) {
                    xdmfMode_ = NoIteration;
                } else if (op->compare("FirstIteration") == 0) {
                    xdmfMode_ = FirstIteration;
                    ERROR("Generating the XMF file is not supported in this version.");
                } else if (op->compare("EveryIteration") == 0) {
                    xdmfMode_ = EveryIteration;
                    ERROR("Generating the XMF file is not supported in this version.");
                } else {
                    ERROR("Undefined value for XDMFMode key !");
                    return false;
                }
            } else if (op->key()->compare("FilesPath") == 0) {
                path_ = (std::string)(*op);
            }
        }
        return true;
    }

    bool HDF5Store::GetHDF5Type(model::Type mdlType, hid_t &hdfType) {
        if (mdlType.compare("int") == 0) {
            hdfType = H5T_NATIVE_INT;
        } else if (mdlType.compare("float") == 0) {
            hdfType = H5T_NATIVE_FLOAT;
        } else if (mdlType.compare("real") == 0) {
            hdfType = H5T_NATIVE_FLOAT;
        } else if (mdlType.compare("integer") == 0) {
            hdfType = H5T_NATIVE_INT;
        } else if (mdlType.compare("double") == 0) {
            hdfType = H5T_NATIVE_DOUBLE;
        } else if (mdlType.compare("long") == 0) {
            hdfType = H5T_NATIVE_LONG;
        } else if (mdlType.compare("short") == 0) {
            hdfType = H5T_NATIVE_SHORT;
        } else if (mdlType.compare("char") == 0) {
            hdfType = H5T_NATIVE_CHAR;
        } else if (mdlType.compare("character") == 0) {
            hdfType = H5T_NATIVE_CHAR;
        } else {
            return false;
        }

        return true;
    }

    void HDF5Store::Output(const int32_t iteration){

        if (IterationIsEmpty(iteration))
        {
            Environment::Log("HDF5Store::Output() calling IterationIsEmpty() returned TRUE. Iteration: " + std::to_string(iteration), EventLogger::Info);
            return;
        }

        switch(fileMode_) {
            case FilePerCore:
                Environment::Log("HDF5Store::Output() calling OutputPerCore(). Iteration: " + std::to_string(iteration), EventLogger::Info);
                OutputPerCore(iteration);
                break;
            case Collective:
                Environment::Log("HDF5Store::Output() calling OutputCollective(). Iteration: " + std::to_string(iteration), EventLogger::Info);
                OutputCollective(iteration);
                break;
            case CollectiveTest:
                OutputCollective(iteration);
                break;
            default: // e.g. file per dedicated node
                ERROR("FileMode is not supported!!!");
        }
    }

    bool HDF5Store::UpdateGhostZones(std::shared_ptr<Variable> v , hid_t &memSpace , hsize_t* localDim){

        int varDimension = v->GetLayout()->GetDimensions();
        hsize_t* offset = new hsize_t[varDimension];
        bool retVal = true;

        for(int i=0; i<varDimension ; i++) {
            int g1 = v->GetLayout()->GetGhostAlong(i).first;
            int g2 = v->GetLayout()->GetGhostAlong(i).second;

            offset[i] = g1 ;
            localDim[i] = localDim[i] - g1 - g2;
        }

        if ((H5Sselect_hyperslab(memSpace, H5S_SELECT_SET, offset, NULL, localDim , NULL)) < 0){
            ERROR("Hyperslab on memory buffer failed.");
            retVal = false;
        }

        delete [] offset;
        return retVal;
    }
    
    void HDF5Store::SetAttributeString(std::string & inString) {
        attribute_string_ = inString ;
    }
    

    // Create string attribute. Uses the string \ref attribute_string_ which is set using \ref SetAttributeString()
    void HDF5Store::CreateDatasetStringAttributes(hid_t& dataset_id, int32_t iteration) {
        herr_t      ret ;             // Return error value 
        hid_t       atype_str ;       // Attribute type 
        hid_t       aid_str ;         // Attribute dataspace identifiers 
        hid_t       attr_str ;        // Attribute identifiers 
        //hid_t       aid_int ;       // Attribute dataspace identifiers 
        //hid_t       attr_int ;      // Attribute identifiers 

        /*// Create and write the iteration integer attribute:
        aid_int  = H5Screate(H5S_SCALAR);  // Create scalar attribute
        if (aid_int < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() iteration H5Screate failed " );
        attr_int = H5Acreate2(dataset_id, "Iteration", H5T_NATIVE_INT, aid_int, H5P_DEFAULT, H5P_DEFAULT);
        if (aid_int < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() iteration H5Acreate2 failed " );
        // Write scalar attribute.
        if (H5Awrite(attr_int, H5T_NATIVE_INT, &iteration) < 0 )
            ERROR("HDF5Store:  H5Awrite failed to write iteration number " );
        
        // tidy up
        ret = H5Sclose(aid_int);
        if (ret < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() iteration H5Sclose failed " );
        ret = H5Aclose(attr_int);
        if (ret < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() iteration H5Aclose failed " );
        */
        if (attribute_string_ != "#") {
            // Create and write the string (comment) attribute:
            size_t str_len = attribute_string_.length() ;
            aid_str  = H5Screate(H5S_SCALAR);
            if (aid_str < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() comment H5Screate failed " );
            atype_str = H5Tcopy(H5T_C_S1);
            if (atype_str < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() comment H5Tcopy failed " );
            H5Tset_size(atype_str, str_len);
            H5Tset_strpad(atype_str, H5T_STR_NULLTERM);
            attr_str = H5Acreate2(dataset_id, "comments", atype_str, aid_str, H5P_DEFAULT, H5P_DEFAULT);
            // Write string attribute.
            if (H5Awrite(attr_str, atype_str, attribute_string_.c_str()))
                ERROR("HDF5Store:  H5Awrite failed to write comment string" );
            
            // tidy up
            ret = H5Sclose(aid_str);
            if (ret < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() comment H5Sclose failed " );
            ret = H5Aclose(attr_str);
            if (ret < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() comment H5Aclose failed " );
            ret = H5Tclose(atype_str);
            if (ret < 0) ERROR("HDF5Store::CreateDatasetStringAttributes() comment H5Tclose failed " );
        }
    }


    std::string HDF5Store::GetOutputFileName(int32_t iteration) {
        std::stringstream fileName;
        int processId;
        std::string baseName;

        processId = Environment::GetEntityProcessID();
        baseName = Environment::GetSimulationName();

        if (fileMode_ == FilePerCore){
            fileName << path_ << baseName << "_It" << iteration << "_Pr" << processId << ".h5";
        } else if (fileMode_ == Collective) {
            fileName << path_ << baseName << "_It" << iteration << ".h5";
        } else if (fileMode_ == CollectiveTest) {
            fileName << path_ << baseName << "_It" << iteration << ".h5";
        }

        return  fileName.str();
    }
    

    std::string HDF5Store::GetVariableFullName(std::shared_ptr<Variable> v , std::shared_ptr<Block> *b){
        std::stringstream varName;
        std::string baseName;
        int numDomains;

        baseName = Environment::GetSimulationName();
        numDomains = Environment::NumDomainsPerClient();

        // (b == NULL) means that there is no access to block data, i.e. in file-per-core mode or future modes.
        if ((fileMode_ == Collective) || (fileMode_ == CollectiveTest) ||  (b == NULL))
            return v->GetName();

        if (numDomains == 1){
            varName << v->GetName() << "/P" << (*b)->GetSource(); // e.g. varName/P2
        } else {// more than one domain
            varName << v->GetName() << "/P" << (*b)->GetSource() << "/B" << (*b)->GetID(); // e.g. varName/P2/B3
        }

        return  varName.str();
    }


    void HDF5Store::OutputPerCore(int32_t iteration) {
        hid_t       fileId, dsetId;           // file and dataset identifiers
        hid_t       fileSpace , memSpace;     // file and memory dataspace identifiers
        hid_t       dtypeId = -1;
        hid_t       lcplId;
        std::string      fileName;
        std::vector<std::weak_ptr<Variable> >::const_iterator w;

        // Initialise variables
        fileName = GetOutputFileName(iteration);
        w = GetVariables().begin();
        
        BlocksByIteration::iterator begin;
        BlocksByIteration::iterator end;

        // Check if any variables are set for writing
        const int num_var_dist = std::distance(GetVariables().begin(), GetVariables().end()) ;
        if (num_var_dist <= 0 ) 
            return ;

        // Create the HDF5 file
        if ((fileId = H5Fcreate(fileName.c_str() , H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)) < 0)
            ERROR("HDF5: H5Fcreate Failed ");

        // Enable group creation with fully qualified dataset names
        lcplId = H5Pcreate (H5P_LINK_CREATE);
        H5Pset_create_intermediate_group(lcplId, 1);

        // for each variable ...
        for (; w != GetVariables().end(); w++) {
            std::shared_ptr<Variable> v = w->lock();
            
            // non TimeVarying variables only are written in the first iteration.
            if ((not v->IsTimeVarying()) && (iteration > 0))
                continue;
            // Test to see that variable contains data 
            // (XML variables may exist in the variable manager, 
            // but have not been written to)
            v->GetBlocksByIteration(iteration, begin, end); 
            const int num_blocks_dist = std::distance(begin, end) ;
            if (num_blocks_dist == 0) 
                continue ;

            // Set the attribute string for the dataset - it will be reused for each block
            // Used in conjunction with CreateDatasetStringAttributes()
            std::string var_sttr_str = v->GetModelCommentVariable() ;
            this->SetAttributeString(var_sttr_str) ;

            // Getting the dimention of the variable
            int varDimension;
            varDimension = v->GetLayout()->GetDimensions();

           /*
            * fileSpace creation is being moved to the inner block loop due
            * to issue with updating layout sizes when parameters change on the
            * server side. See:
            * https://gitlab.inria.fr/Damaris/damaris-development/-/issues/20
            */

            // Getting the equivalent HDF5 Variable Type
            if (not GetHDF5Type(v->GetLayout()->GetType(), dtypeId))
                ERROR("HDF5:Unknown variable type " << v->GetLayout()->GetType());

            BlocksByIteration::iterator begin;
            BlocksByIteration::iterator end;
            v->GetBlocksByIteration(iteration, begin, end);
            int numBlocks = 0;
            std::string varName;

            for (BlocksByIteration::iterator bid = begin; bid != end; bid++) {
                std::shared_ptr<Block> b = *bid;
                numBlocks++;

                // Create block dimentions
                int blockDimension = b->GetDimensions();
                hsize_t *blockDim = new (std::nothrow) hsize_t[blockDimension];
                if (blockDim == NULL)
                    ERROR("HDF5:Failed to allocate memory ");

                for (int i = 0 ; i < blockDimension ; i++)
                {
                     blockDim[i] = b->GetEndIndex(i) - b->GetStartIndex(i) + 1;
                }
                
                // Create memory data space
                memSpace = H5Screate_simple(blockDimension, blockDim , NULL);

                // Update ghost zones - returns the correct size of the dataset on file in blockDim
                UpdateGhostZones(v , memSpace , blockDim);

                // create the file space
                if ((fileSpace = H5Screate_simple(varDimension, blockDim , NULL)) < 0)
                    ERROR("HDF5: file space creation failed !");

                // Create Dataset for each block
                varName = GetVariableFullName(v , &b);
                if ((dsetId = H5Dcreate(fileId, varName.c_str() , dtypeId , fileSpace,
                                         lcplId, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                    ERROR("HDF5: Failed to create dataset ... ");
               
                
                // Select hyperslab in the file.
                fileSpace = H5Dget_space(dsetId);
                H5Sselect_all(fileSpace);
                //H5Sselect_hyperslab(fileSpace, H5S_SELECT_SET, memOffset, NULL, blockDim , NULL);

                // Getting the data
                void *ptr = b->GetDataSpace().GetData();

                // Writing data
                if (H5Dwrite(dsetId, dtypeId, memSpace, fileSpace, H5P_DEFAULT, ptr) < 0) {
                    ERROR("HDF5: Writing Data Failed !");
                } else {
                    this->CreateDatasetStringAttributes(dsetId, iteration) ;
                }
                
                // Free evertything
                delete [] blockDim;
                H5Sclose(memSpace);
                H5Sclose(fileSpace);
                H5Dclose(dsetId);
            } // for of block iteration
        } // for of variable iteration

        H5Fclose(fileId);
        H5Pclose(lcplId);
    }
    
    

    void HDF5Store::OutputCollective(int32_t iteration) {
        hid_t fileId;
        hid_t lcplId;

        hid_t plistId = H5P_DEFAULT;
        std::string fileName;

        // Initializing variables
        std::vector<std::weak_ptr<Variable> >::const_iterator w = GetVariables().begin();

        // Check if any variables are set for writing
        const int num_var_dist = std::distance(GetVariables().begin(), GetVariables().end()) ;
        if (num_var_dist <= 0 ) 
            return ;
        
        MPI_Comm comm = Environment::GetEntityComm();
        MPI_Info info  = MPI_INFO_NULL;
        fileName = GetOutputFileName(iteration);

        // Create file access property list
        plistId = H5Pcreate(H5P_FILE_ACCESS);
        H5Pset_fapl_mpio(plistId, comm, info);

        // Creating the HDF5 file
        if((fileId = H5Fcreate(fileName.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plistId)) < 0)
            ERROR("HDF5: H5Fcreate Failed. Filename: " << fileName.c_str() );

        H5Pclose(plistId);

        // enabling group creation with full qualified names
        lcplId = H5Pcreate (H5P_LINK_CREATE);
        H5Pset_create_intermediate_group(lcplId, 1);
        
        BlocksByIteration::iterator begin;
        BlocksByIteration::iterator end;

        // for each variable do
        for(; w != GetVariables().end(); w++) {
            std::shared_ptr<Variable> v = w->lock();
            
            // Test to see that variable contains data 
            // (XML variables may exist in the variable manager, 
            // but have not been written to)
            v->GetBlocksByIteration(iteration, begin, end); 
            const int num_blocks_dist = std::distance(begin, end) ;
            if (num_blocks_dist == 0) 
                continue ;
               
            // Check if the "select-file=" attribute is present in this Variable
            // if so then we have another variable that specifies the order that the current variables 
            // data in memory should be written to file using, and we use OutputCollectiveSelect() 
            // It populates the fileSelect_ map
            std::string select_file_var_str        = v->GetModelSelectFileVariable() ;
            std::shared_ptr<Variable> v_file_select = nullptr;
            if (  select_file_var_str != "#" ) {
                v_file_select = VariableManager::Search(select_file_var_str);
            }
            // If the output to file shape is different from the original input data variable
            // in memory we will require this select_var_subset_str variables to describe the 
            // shape per mpi-rank.
            // A select-mem variable may be also required,  however it is searched for within
            // the OutputCollectiveSelectSubset() method and if not found then a hyperslab selction
            // method used.
            std::string select_var_subset_str = v->GetModelSelectSubsetVariable() ; 
            std::shared_ptr<Variable> v_select_subset = nullptr;
            if (  select_var_subset_str != "#" ) {
                v_select_subset = VariableManager::Search(select_var_subset_str);
            }
            
            // Deal with a memory selection variable if available. It populates memSelect_ map
            std::string select_mem_var_str = v->GetModelSelectMemVariable() ;
            std::shared_ptr<Variable> v_mem_select = nullptr;
            if (  select_mem_var_str != "#" ) {
                v_mem_select = VariableManager::Search(select_mem_var_str);
            }
            
            if ( (bool) v_select_subset ) {
                // v_mem_select or v_file_select can be nullptrs. 
                Environment::Log("HDF5Store::OutputCollective() calling () OutputCollectiveSelectSubset(). Iteration: " + std::to_string(iteration), EventLogger::Info);
                HDF5Store::OutputCollectiveSelectSubset(iteration, v, v_file_select, v_select_subset, v_mem_select, fileId, lcplId) ;
            } else if ( (bool) v_file_select ){
                // This one requires the number of values selected to match the number of values 
                // in the original variable being selected from.
                Environment::Log("HDF5Store::OutputCollective() calling () OutputCollectiveSelect(). Iteration: " + std::to_string(iteration), EventLogger::Info);
                HDF5Store::OutputCollectiveSelect(iteration, v, v_file_select, fileId, lcplId) ;
            } else {
                // There was no select data specified so use the standard hyperslab
                // method for writing the variable data
                Environment::Log("HDF5Store::OutputCollective() calling () OutputCollectiveOrdered(). Iteration: " + std::to_string(iteration), EventLogger::Info);
                HDF5Store::OutputCollectiveOrdered(iteration, v, fileId, lcplId) ;
            }

        } // for loop over variables
        
        H5Pclose(lcplId);
        H5Fclose(fileId);
        
    }
    
    
    void HDF5Store::OutputCollectiveOrdered(int32_t iteration, std::shared_ptr<Variable> v, hid_t& fileId, hid_t& lcplId) {

        hid_t dsetId = -1;
        hid_t dtypeId = -1;
        hid_t fileSpace;
        hid_t fileSpace2;
        hid_t memSpace;
        hsize_t *memOffset;
        hsize_t *memDim;

        hid_t plistId = H5P_DEFAULT;
  
        BlocksByIteration::iterator begin;
        BlocksByIteration::iterator end;

        // write non-time-varying variables only in first iteration
        if ((not v->IsTimeVarying()) && (iteration > 0))
            return;

        int varDimension;
        varDimension = v->GetLayout()->GetDimensions();

        hsize_t *globalDim;
        globalDim = new (std::nothrow) hsize_t[varDimension];

        // Get equivalent HDF5 type
        if (not GetHDF5Type(v->GetLayout()->GetType() , dtypeId))
            ERROR("HDF5Store::OutputCollectiveOrdered: Unknown variable type " << v->GetLayout()->GetType());

        v->GetBlocksByIteration(iteration, begin, end);

        memOffset = new (std::nothrow) hsize_t[varDimension];
        memDim = new (std::nothrow) hsize_t[varDimension];

        if ((memOffset == NULL) || (memDim == NULL)) {
            ERROR("HDF5Store::OutputCollectiveOrdered: Failed to allocate memDim and memOffset memory ");
            ERROR("Writing blocks to the file failed. ");
        }
        
         // Set the attribute string for the dataset - it will be reused for each block
         // Used in conjunction with CreateDatasetStringAttributes()
         std::string var_sttr_str = v->GetModelCommentVariable() ;
         this->SetAttributeString(var_sttr_str) ;
            
            
        int blockNum = 0;

        for(BlocksByIteration::iterator bid = begin; bid != end; bid ++) {
            std::shared_ptr<Block> b = *bid;
             
            std::string logString_global("HDF5Store::OutputCollective() globalDim ") ;
            if (blockNum == 0) {
                // Obtain the FilesSpace size (has to match the memory space dimensions)
                std::string varName = GetVariableFullName(v);
                logString_global += varName ;
                
                for (int i = 0; i < varDimension; i++) {
                    globalDim[i] = b->GetGlobalExtent(i);
                    logString_global += "[" + std::to_string(globalDim[i]) + "]" ;
                }
               
                
                // Create dataspace.
                if ((fileSpace = H5Screate_simple(varDimension, globalDim , NULL)) < 0)
                    ERROR("HDF5Store::OutputCollectiveOrdered: file space creation failed !");

                
                // Create the dataset
                if ((dsetId = H5Dcreate( fileId, varName.c_str(), dtypeId, fileSpace,
                         lcplId, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                    ERROR("HDF5Store::OutputCollectiveOrdered: Failed to create dataset ... " << varName.c_str());

                H5Sclose(fileSpace);
            } else {
                std::string varName = GetVariableFullName(v);
                logString_global += varName ;
            }

            // Obtain the starting indices and the size of the hyperslab
            std::string logString_offset("HDF5Store::OutputCollective() memOffset") ;
            std::string logString_dim("HDF5Store::OutputCollective()    memDim") ;
            for(int i = 0; i < varDimension; i++) {
                 memOffset[i]  = b->GetStartIndex(i);
                 memDim[i]     = b->GetEndIndex(i) - b->GetStartIndex(i) + 1;
                 logString_offset += "[" + std::to_string(memOffset[i]) + "]" ;
                 logString_dim    += "[" + std::to_string(memDim[i]) + "]" ;                     
             }

             
             // Create memory data space
             memSpace = H5Screate_simple(varDimension, memDim , NULL);

             // Update ghost zones
             UpdateGhostZones(v , memSpace , memDim);

             // Select hyperslab in the file.
             fileSpace2 = H5Dget_space(dsetId);
             H5Sselect_hyperslab(fileSpace2, H5S_SELECT_SET, memOffset, NULL, memDim , NULL);

             // Create property list for collective dataset write.
             plistId = H5Pcreate(H5P_DATASET_XFER);
             H5Pset_dxpl_mpio(plistId, H5FD_MPIO_COLLECTIVE);

             void* ptr = b->GetDataSpace().GetData();

             if (H5Dwrite(dsetId, dtypeId, memSpace, fileSpace2, plistId, ptr) < 0) {
                 ERROR("HDF5Store::OutputCollectiveOrdered: Writing blocks to the file failed. ");
             } 
                
             blockNum++ ;
             H5Sclose(fileSpace2);
             H5Sclose(memSpace);
             H5Pclose(plistId);
             
             Environment::Log(logString_global , EventLogger::Debug); // only updated when the counter blockNum==0
             Environment::Log(logString_dim ,    EventLogger::Debug);
             Environment::Log(logString_offset , EventLogger::Debug);
         
        } // for the blocks loop
        
        this->CreateDatasetStringAttributes(dsetId, iteration) ;
         
         
        delete [] memOffset;
        delete [] memDim;
        delete [] globalDim;
        if (dsetId >= 0) H5Dclose(dsetId);
   

    }  // OutputCollectiveOrdered
    
    
    
    
    
    
    
    
   // This version will use the select_subset variable information to determine global and local memory space sizes             
    int HDF5Store::ComputeSelectSubsetData(int32_t iteration,  std::shared_ptr<Variable> v_subset) 
    {
        BlocksByIteration::iterator select_begin;
        BlocksByIteration::iterator select_end;
        
        // We will try to make data access a little more generic by checking for the type of the data
        // Currently AI sm assuming that the select_subset variable is of integer type
        DAMARIS_TYPE_STR damaris_vartype ;
        damaris_get_type(v_subset->GetName().c_str(), &damaris_vartype) ;
        if (damaris_vartype != DAMARIS_TYPE_INT ) {
            ERROR("HDF5Store::ComputeSelectSubsetData() : the select_subset has to be of type=int. Name :" << v_subset->GetName()  << std::endl );
            assert(damaris_vartype != DAMARIS_TYPE_INT ) ;
            return -1 ;
        }
        
        int varDimension;
        varDimension = v_subset->GetLayout()->GetDimensions();
        
        int select_iteration = iteration ;
        if (not v_subset->IsTimeVarying())
            select_iteration = 0;
        
        // Get the iterators to the select values
        v_subset->GetBlocksByIteration(select_iteration, select_begin, select_end);
        
        // Check that the number of blocks found is consistent
        int number_local_blocks = v_subset->CountLocalBlocks(iteration) ;
        const int num_blocks_dist = std::distance(select_begin, select_end) ; 

        if (num_blocks_dist != number_local_blocks)
        {
            ERROR("HDF5Store::ComputeSelectSubsetData: number_local_blocks = " << number_local_blocks ) ;
            ERROR("HDF5Store::ComputeSelectSubsetData: num_blocks          = " << num_blocks_dist ) ;
            assert(num_blocks_dist == number_local_blocks) ;
            return -1 ;
        }
        

        // Stack Overflow: On my platform, size_t and HDF5's hsize_t are both unsigned 64bit integers, 
        // yet they are different C++ types (one is unsigned long, the other unsigned long long) 
        // and cannot be used interchangibly.
        
        int *domainSz;
        std::vector<int> localMemSz;
        std::vector<int> localOffset;
        int *localMemSzTotal;
        size_t select_block_size_first = 0 ;
        int blocknum = 0 ;
        int domain_dimension = 0 ;
        for (auto block_iter = select_begin; block_iter != select_end; ++block_iter) {
            std::shared_ptr<Block>  select_block = *block_iter;
            
            // These will be formed into our random access variable as std::pair<block_Source, block_ID>
            int block_Source = select_block->GetSource() ;
            int block_ID = select_block->GetID() ;  
            
            // select_block_size is used to find the dimension of the select data as we specify 
            size_t select_block_size = 1 ; // select_block->GetEndIndex(0) - select_block->GetStartIndex(0) + 1;
            for (int i = 0; i < varDimension; i++) {
                select_block_size *= (select_block->GetEndIndex(0) - select_block->GetStartIndex(0) + 1);
            }
            
            // Currently assume that the select_subset variable is of integer type
            int* int_ptr = static_cast<int*>( select_block->GetDataSpace().GetData() );
            // All blocks of this variable should be the same size and 2 x the size of the dimensions of the data being selected
            if (blocknum == 0) {
                select_block_size_first = select_block_size ;
                domain_dimension = select_block_size_first / 3  ;                // The select_block_size_first should be of a factor of 3 e.g. 3 => 1D; 6 => 2D; 9 => 3D, etc.
                domainSz        = new (std::nothrow) int[domain_dimension];  // This should be the same between all blocks and all damaris servers
                localMemSz.reserve(domain_dimension)  ;                          // 
                localOffset.reserve(domain_dimension)  ; 
                localMemSzTotal = new (std::nothrow) int[domain_dimension];  // This is the running total over the blocks
                // Get the domain size and the block dimension of first block
                for (int i = 0 ; i < domain_dimension ; i++) {
                    domainSz[i] = static_cast<int>(int_ptr[i]);
                    // localMemSz.data()[i] =  static_cast<int>(int_ptr[domain_dimension+i])  ;
                    localMemSz.push_back(static_cast<int>(int_ptr[domain_dimension+i])) ;
                    localOffset.push_back(static_cast<int>(int_ptr[(domain_dimension*2)+i])) ;
                    localMemSzTotal[i]   =  static_cast<int>(int_ptr[domain_dimension+i])  ;
                }
            } else {
                // Check that the size of the memory sections is the same
                if (select_block_size_first != select_block_size) {
                    ERROR("HDF5Store::ComputeSelectSubsetData() : the block sizes are not equal in the select_subset variable. Name :" << v_subset->GetName() << " block size: " << select_block_size_first << std::endl );
                    delete [] domainSz;
                    delete [] localMemSzTotal;
                    assert(select_block_size_first == select_block_size) ;
                    return -1;
                }
                // Check the domain sizes are equal amongst the blocks on the Damaris server
                for (int i = 0 ; i < domain_dimension ; i++) {
                    assert(domainSz[i] == static_cast<int>(int_ptr[i]) );
                    localMemSzTotal[i]  += static_cast<int>(int_ptr[domain_dimension+i]) ;
                    localMemSz.data()[i] =  static_cast<int>(int_ptr[domain_dimension+i])  ;
                    localOffset.data()[i]   =  static_cast<int>(int_ptr[(domain_dimension*2)+i])  ;
                }
                // localMemSz.clear() ;
            }
            // Store the array for later retrieval
            this->selectSubsetBlockSize_.insert( std::make_pair(std::make_pair(block_Source, block_ID), localMemSz) ) ;
            this->selectSubsetOffset_.insert( std::make_pair(std::make_pair(block_Source, block_ID), localOffset) ) ;
            // For debug reasons:
            for (int i = 0 ; i < domain_dimension ; i++) {
                  localMemSz.data()[i] =  0  ;
                  localOffset.data()[i] =  0  ;
            }
                
            blocknum++ ;
        }
        
        int *localMemSzTotalRes = new (std::nothrow) int[domain_dimension];  
        //  H5T_NATIVE_INT == int 
        // Sum values into localMemSzTotalRes
        MPI_Allreduce(localMemSzTotal, localMemSzTotalRes, domain_dimension, MPI_INT, MPI_SUM, Environment::GetEntityComm()) ;
        
        this->globalSizeSelectSubset_.reserve(domain_dimension) ;
        this->globalSizeSelectSubset_.resize(domain_dimension) ;
        
        for (int dim = 0 ; dim < domain_dimension ; dim++)
        {
            int domainSzDivisor = 1; // 
            for (int d = 0 ; d < domain_dimension ; d++){
                if (d != dim)
                    domainSzDivisor *= domainSz[d] ;
            }
            this->globalSizeSelectSubset_[dim] = localMemSzTotalRes[dim] / domainSzDivisor ;
        }
        
        delete [] domainSz;
        delete [] localMemSzTotal;
        delete [] localMemSzTotalRes ;

        
        return number_local_blocks ;
               
    } 
    
    
    
    // Using select_subset data to try to ge the correct memory space. The file memory space selection is found within this function and used if available
    void HDF5Store::OutputCollectiveSelectSubset(int32_t iteration, std::shared_ptr<Variable> v, 
                                                                     std::shared_ptr<Variable> v_file_select, 
                                                                     std::shared_ptr<Variable> v_subset, 
                                                                     std::shared_ptr<Variable>  v_mem_select, 
                                                                     hid_t& fileId, hid_t& lcplId) 
    {

        hid_t dsetId = -1;
        hid_t dtypeId = -1;
        hid_t fileSpace;
        hid_t fileSpace2;
        hid_t memSpace;
        hsize_t *memOffset;
        hsize_t *memDim;

        hid_t plistId = H5P_DEFAULT;

        BlocksByIteration::iterator begin;
        BlocksByIteration::iterator end;
        
         // I am currently assuming this is the number of local blocks of this server
        // We test if this is true in the assert() below
        v->GetBlocksByIteration(iteration, begin, end);  
        int blockNum = 0;
        
        size_t select_block_size_mem = 0;
        size_t select_block_size_file = 0;
        
        // write non-time-varying variables only in first iteration
        if ((not v->IsTimeVarying()) && (iteration > 0))
                return ;
        
        // Deal with a memory selection variable if available. It populates memSelect_ map
        // if it is a nullptr then we use the whole memory 
        if ((bool) v_mem_select) {
            if ((v_mem_select->IsTimeVarying() == true) ||  (iteration == 0)) {
                // set up the hd5f select data as a hsize_t array
                number_local_blocks_ =  this->SetSelectData( iteration, v_mem_select, this->memSelect_) ;
                if ( number_local_blocks_ <= 0) {
                    ERROR("HDF5Store::OutputCollectiveSelectSubset() : Failed to get select data for variable: " << v->GetName() << "\n from Damaris XML variable. Select variable name: " << v_mem_select->GetName() );
                }                
            }
        }
        

        if ( (bool) v_file_select) 
        {
            // This is correct, as the HDF5 select data (*coord) is serialised 
            // (see https://portal.hdfgroup.org/display/HDF5/H5S_SELECT_ELEMENTS)
            if (v_file_select->GetLayout()->GetDimensions() != 1) {
                assert(v_file_select->GetLayout()->GetDimensions() != 1);
                ERROR("HDF5::OutputCollectiveSelectSubset The select variable needs to be a 1 dimensional array of indecies. Variable :" << v->GetName()  << ",  Select variable: " << v_file_select->GetName());
            }
            // On the first iteration, or if the select data variable is time varying then 
            // fill in the fileSelect_<> array with the transformed/cast data select data
            if ((v_file_select->IsTimeVarying() == true) ||  (iteration == 0)) {
                // set up the hd5f select data as a hsize_t array
                number_local_blocks_ =  this->SetSelectData( iteration, v_file_select, this->fileSelect_) ;
                if ( number_local_blocks_ <= 0) {
                    ERROR("HDF5Store::OutputCollectiveSelectSubset() : Failed to get select data for variable: " << v->GetName() << "\n from Damaris XML variable. Select variable name: " << v_file_select->GetName());
                }                
            }

            const int num_blocks_dist = std::distance(begin, end) ;                
            if (num_blocks_dist != number_local_blocks_) {
              ERROR("HDF5Store::OutputCollectiveSelectSubset() : the number of blocks in the variable is not equal to the number of blocks in the select variable.  Variable :" << v->GetName()  << ",  Select variable: " << v_file_select->GetName() );
              assert(num_blocks_dist == number_local_blocks_) ;
              return ;
            }
        }
        
       
        
        // On the first iteration, or if the select data variable is time varying then 
        // fill in the select-subset array with the transformed/cast data select data
        if ((v_subset->IsTimeVarying() == true) ||  (iteration == 0)) {
            // ComputeSelectSubsetData: Set up the hdf5 select subset data .
            // It computes global output dataset sizes and places in: globalSizeSelectSubset_
            // It also fills the std::map selectSubsetBlockSize_ that we use for the memory space sizes per block
            number_local_blocks_ =  this->ComputeSelectSubsetData( iteration, v_subset) ;
            if ( number_local_blocks_ <= 0) {
                ERROR("HDF5Store::OutputCollectiveSelectSubset() : Failed to get select data for variable: " << v->GetName() << "\n from Damaris XML variable. Select subset variable name: " << v_subset->GetName());
            }                
        }
        
       
        // This is the dimension of the data being read from (v)
        int varDimension;
        varDimension = v->GetLayout()->GetDimensions();
        
         // This is the dimensions specified in the select_subset data information for dataset output
        int varDimensionFile ;
        varDimensionFile = this->globalSizeSelectSubset_.size(); // our output dimensions are goverend by the v_subset variable data.
        

        hsize_t *globalDim;
        globalDim = new (std::nothrow) hsize_t[varDimensionFile];

        // Get equivalent HDF5 type to the variable (this is specified in the layout)
        if (not GetHDF5Type(v->GetLayout()->GetType() , dtypeId))
            ERROR("HDF5:Unknown variable type " << v->GetLayout()->GetType());



        // memOffset = new (std::nothrow) hsize_t[varDimension];
        memDim = new (std::nothrow) hsize_t[varDimensionFile];
        hsize_t * fileDim = new (std::nothrow) hsize_t[varDimensionFile];
        hsize_t * fileOffset = new (std::nothrow) hsize_t[varDimensionFile];

        if (memDim == NULL) {
          ERROR("HDF5Store::OutputCollectiveSelectSubset() : Failed to allocate memDim and memOffset memory " << v->GetName() );
        }
        
        // Set the attribute string for the dataset - it will be reused for each block
        // Used in conjunction with CreateDatasetStringAttributes()
        std::string var_sttr_str = v->GetModelCommentVariable() ;
        this->SetAttributeString(var_sttr_str) ;
        
        
        // auto select_block_iter = select_begin;
        for(BlocksByIteration::iterator bid = begin; bid != end; bid ++) {

            std::shared_ptr<Block>  b = *bid;

            // Get the select data in correct form ready for H5Sselect_elements
            // An assumption is that the user specifies the same block number for 
            // the data variable and the select data.
            // These will be used as std::pair<block_Source, block_ID>
            int block_Source = b->GetSource() ;
            int block_ID = b->GetID() ;  
            
            hsize_t * select_block_data_mem = nullptr; // = memSelect_[block_ID]->data() ;
            if ((bool) v_mem_select) {
                std::vector<hsize_t>* select_data_mem ;
                std::map< std::pair<int, int>,   std::vector<hsize_t> * >::const_iterator search_mem ;
                search_mem = memSelect_.find(std::pair<int,int>(block_Source, block_ID)) ; 
                if ( search_mem != memSelect_.end()) {
                    select_data_mem = search_mem->second ;
                    select_block_size_mem = select_data_mem->size() / varDimension ;
                    select_block_data_mem = select_data_mem->data() ;
                }
                else
                    ERROR("HDF5Store::OutputCollectiveSelectSubset(): Failed to find block in memSelect_, Iteration: " << iteration << "   Variable:" <<   v->GetName()  << " with client rank (block_Source)= " << block_Source << " and block id=" << block_ID);
            }
            
            hsize_t * select_block_data_file = nullptr; // = fileSelect_[block_ID]->data() ;
            if ((bool) v_file_select) {
                // This finds the select data for the file dataspace if 'select-file=""' 
                // is present in the Damaris XML file. 
                std::vector<hsize_t>* select_data_file ;
                std::map< std::pair<int, int>,   std::vector<hsize_t> * >::const_iterator search_file ;
                search_file = fileSelect_.find(std::pair<int,int>(block_Source, block_ID)) ; 
                if ( search_file != fileSelect_.end()) {
                    select_data_file = search_file->second ;
                    select_block_size_file = select_data_file->size() / varDimensionFile ;
                    select_block_data_file = select_data_file->data() ;
                }
                else
                    ERROR("HDF5Store::OutputCollectiveSelectSubset(): Failed to find block in fileSelect_, Iteration: " << iteration << "   Variable:" <<   v->GetName()  << " with client rank (block_Source)= " << block_Source << " and block id=" << block_ID);
            }
            
            
            // These two code blocks use the data from the v_subset description of our desired output shape
            // obtained via ComputeSelectSubsetData() method
            std::vector<int> select_subset_block_dims  ;// 
            std::map< std::pair<int, int>,   std::vector<int> >::const_iterator search_subset ;
            search_subset = selectSubsetBlockSize_.find(std::pair<int,int>(block_Source, block_ID)) ; 
            if ( search_subset != selectSubsetBlockSize_.end()) {
                select_subset_block_dims = search_subset->second ;
            }
            else
                ERROR("HDF5Store::OutputCollectiveSelectSubset(): Failed to find block in selectSubsetBlockSize_, Iteration: " << iteration << "   Variable:" <<  v->GetName()  << " with client rank (block_Source)= " << block_Source << " and block id=" << block_ID);
            
            std::vector<int> select_subset_block_offset  ;// 
            std::map< std::pair<int, int>,   std::vector<int> >::const_iterator search_subset_offset ;
            search_subset_offset = selectSubsetOffset_.find(std::pair<int,int>(block_Source, block_ID)) ; 
            if ( search_subset_offset != selectSubsetOffset_.end()) {
                select_subset_block_offset = search_subset_offset->second ;
            }
            else
                ERROR("HDF5Store::OutputCollectiveSelectSubset(): Failed to find block in selectSubsetOffset_, Iteration: " << iteration << "   Variable:" <<  v->GetName()  << " with client rank (block_Source)= " << block_Source << " and block id=" << block_ID);
            
            
            
            std::string varName ;
            std::string logString_global("HDF5Store::OutputCollectiveSelectSubset() globalDim ") ;
            if (blockNum == 0) {
                 // Obtain the FilesSpace size (has to match the memory space dimensions)
                varName = GetVariableFullName(v);
                logString_global += varName ;

                for (int i = 0; i < varDimensionFile; i++) {
                    globalDim[i] = globalSizeSelectSubset_[i];
                    logString_global += "[" + std::to_string(globalSizeSelectSubset_[i]) + "]" ;
                }

                // Create dataspace.
                if ((fileSpace = H5Screate_simple(varDimensionFile, globalDim , NULL)) < 0)
                    ERROR("HDF5Store::OutputCollectiveSelectSubset(): file space creation failed !" <<  v->GetName());

                // Create the dataset
                if ((dsetId = H5Dcreate( fileId, varName.c_str(), dtypeId, fileSpace,
                         lcplId, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                    ERROR("HDF5Store::OutputCollectiveSelectSubset(): Failed to create dataset ... " << varName.c_str());

                H5Sclose(fileSpace);
                
                // Create property list for collective dataset write.
                 plistId = H5Pcreate(H5P_DATASET_XFER);
                 H5Pset_dxpl_mpio(plistId, H5FD_MPIO_COLLECTIVE);
            } else {
                varName = GetVariableFullName(v);
                logString_global += varName ;
            }      
            
             // Obtain the starting indices and the size of the full memory hyperslab
            std::string logString_dim("HDF5Store::OutputCollectiveSelectSubset()    memDim") ;
            for(int i = 0; i < varDimension; i++) {  // varDimensionFile or varDimension
                 // memOffset[i]  = b->GetStartIndex(i);
                  memDim[i]     = b->GetEndIndex(i) - b->GetStartIndex(i) + 1;
                  logString_dim    += "[" + std::to_string(memDim[i]) + "]" ;                     
             }
             
             std::string logString_FileDim("HDF5Store::OutputCollectiveSelectSubset() fileDim") ;
             std::string logString_Fileoffset("HDF5Store::OutputCollectiveSelectSubset() fileOffset") ;
             for(int i = 0; i < varDimensionFile; i++) {  // varDimensionFile or varDimension
                  fileDim[i]     = select_subset_block_dims[i] ;
                  fileOffset[i]  = select_subset_block_offset[i] ;
                 // logString_offset += "[" + std::to_string(memOffset[i]) + "]" ;
                 logString_FileDim    += "[" + std::to_string(fileDim[i]) + "]" ;
                 logString_Fileoffset    += "[" + std::to_string(fileOffset[i]) + "]" ; 
             }

             // Create memory data space
             memSpace = H5Screate_simple(varDimension, memDim , NULL); // varDimensionFile or varDimension
             
            if ( (bool) v_mem_select) {
                if (H5Sselect_elements(memSpace, H5S_SELECT_SET, select_block_size_mem, select_block_data_mem ) < 0) {
                     ERROR("HDF5Store::OutputCollectiveSelectSubset(): H5Sselect_elements failed  to select memory elements for block.  Iteration: " << iteration << "   Variable:" <<    v->GetName() );
                }
                
                 htri_t ret_bool = H5Sselect_valid(memSpace); 
                 if (! ret_bool ) {
                     std::cerr << "Memory dataspace is NOT valid : " << ret_bool << "  block_Source= " << block_Source << " ; block id=" << block_ID << std::endl ;
                     hssize_t numpointsMS = H5Sget_select_elem_npoints (memSpace) ;
                     std::cerr << "  numpointsMS = " << numpointsMS << std::endl ;
                     hsize_t*  pointBuff = new hsize_t[numpointsMS*varDimension] ;
                     H5Sget_select_elem_pointlist(memSpace, 0, numpointsMS, pointBuff) ;
                     std::cerr << "Values: "  ;
                     for (int i = 0 ; i < numpointsMS; i++) {
                         std::cerr << "  [" ;
                         for (int j = 0 ; j < varDimension ; j++)
                            std::cerr <<  pointBuff[i*varDimension + j] << "," ;
                         std::cerr << "]" ;
                     }
                     std::cerr << std::endl ;
                     delete [] pointBuff ;
                 }
            }
             
             fileSpace2 = H5Dget_space(dsetId);
             
             
             // Select hyperslab in the file.
             if ( (bool) v_file_select) {
                  // Select the points in the file.
                 if (H5Sselect_elements(fileSpace2, H5S_SELECT_SET, select_block_size_file, select_block_data_file ) < 0) {
                     ERROR("HDF5Store::OutputCollectiveSelectSubset(): H5Sselect_elements failed to select file space elements for block.  Iteration: " << iteration << "   Variable:" <<    v->GetName() );
                 }
                 htri_t ret_bool = H5Sselect_valid(fileSpace2); 
                 if (! ret_bool ) {
                     std::cerr << "File dataspace is NOT valid : " << ret_bool << "  block_Source= " << block_Source << " ; block id=" << block_ID << std::endl ;
                     hssize_t numpointsFS = H5Sget_select_elem_npoints (fileSpace2) ;
                     std::cerr << "  numpointsFS = " << numpointsFS << std::endl ;
                     hsize_t*  pointBuff = new hsize_t[numpointsFS*varDimension] ;
                     H5Sget_select_elem_pointlist(fileSpace2, 0, numpointsFS, pointBuff) ;
                     std::cerr << "  Values: " ;
                     for (int i = 0 ; i < numpointsFS; i++) {
                         std::cerr << "  [" ;
                         for (int j = 0 ; j < varDimension ; j++)
                            std::cerr <<  pointBuff[i*varDimension + j] << "," ;
                         std::cerr << "]" ;
                     }
                     std::cerr << std::endl ;
                     delete [] pointBuff ;
                 }
                 
             } else {
                if (H5Sselect_hyperslab(fileSpace2, H5S_SELECT_SET, fileOffset, NULL, fileDim , NULL) < 0) {
                      ERROR("HDF5Store::OutputCollectiveSelectSubset(): H5Sselect_hyperslab failed to select file space elements for block.  Iteration: " << iteration << "   Variable:" <<    v->GetName() );
                 }
             }

             void* ptr = b->GetDataSpace().GetData();

             if (H5Dwrite(dsetId, dtypeId,  memSpace, fileSpace2, plistId, ptr) < 0) {
                 ERROR("HDF5Store::OutputCollectiveSelectSubset(): Writing blocks to the file failed.  Iteration: " << iteration << "   Variable:" <<   v->GetName());
             }
             blockNum++ ;
             H5Sclose(fileSpace2);
             H5Sclose(memSpace);
             
             
             Environment::Log(logString_global , EventLogger::Debug); // only updated when the counter blockNum==0
             Environment::Log(logString_dim ,    EventLogger::Debug);
             Environment::Log(logString_FileDim ,    EventLogger::Debug);
             Environment::Log(logString_Fileoffset ,    EventLogger::Debug);
             
             // ++select_block_iter ;
             
        } // for the blocks loop
        
        this->CreateDatasetStringAttributes(dsetId, iteration) ;
         
        // delete [] memOffset;
        H5Pclose(plistId);
        delete [] memDim;
        delete [] fileDim; 
        delete [] fileOffset; 
        delete [] globalDim;
        if (dsetId >= 0) H5Dclose(dsetId);

    } // end of OutputCollectiveSelectSubset
  
    
    
 
 
   // The original - This one works if the same number of points are selected in the H5Sselect_elements as are present in the data being selected from             
       
    int HDF5Store::SetSelectData(int32_t iteration, std::shared_ptr<Variable> v_select, std::map< std::pair<int, int>,   std::vector<hsize_t> * > & mem_or_file_select) 
    {
        BlocksByIteration::iterator select_begin;
        BlocksByIteration::iterator select_end;
        
                // We will try to make data access a little more generic by checking for the type of the data
        DAMARIS_TYPE_STR damaris_vartype ;
        damaris_get_type(v_select->GetName().c_str(), &damaris_vartype) ;
        
         
        int select_iteration = iteration ;
        if (not v_select->IsTimeVarying())
            select_iteration = 0;
        
        // Get the iterators to the select values
        v_select->GetBlocksByIteration(select_iteration, select_begin, select_end);
        
       
        // We check above that the selct data is one dimensional, so just use 
        // the single first dimension to get the number of elements
       
        int number_local_blocks = v_select->CountLocalBlocks(iteration) ;
        const int num_blocks_dist = std::distance(select_begin, select_end) ; 

        if (num_blocks_dist != number_local_blocks)
        {
            ERROR("HDF5Store::SetSelectData: number_local_blocks = " << number_local_blocks ) ;
            ERROR("HDF5Store::SetSelectData: num_blocks          = " << num_blocks_dist ) ;
            assert(num_blocks_dist == number_local_blocks) ;
            return -1 ;
        }
        
        
        // make sure our std::vector<> pointers are deleted
        for ( auto& it : mem_or_file_select) {
            if (it.second != nullptr) {
                delete it.second  ;
            }
        }
        mem_or_file_select.clear() ;
        
        
        for (auto block_iter = select_begin; block_iter != select_end; ++block_iter) {
            std::shared_ptr<Block>  select_block = *block_iter;
            
            // These will be formed into our random access variable as std::pair<block_Source, block_ID>
            int block_Source = select_block->GetSource() ;
            int block_ID = select_block->GetID() ;  

            // We have checked that the variable is 1 dimensional, so we can use index = 0
            const size_t select_block_size = select_block->GetEndIndex(0) - select_block->GetStartIndex(0) + 1;
            
            // This is where we are copying the damaris variable data (and casting to type hsize_t)
            std::vector<hsize_t>* select_data = new std::vector<hsize_t>(select_block_size) ;
            mem_or_file_select.insert(std::make_pair(std::make_pair(block_Source, block_ID), select_data) ) ;
            

            
            // fills in the mem_or_file_select<> array with the transformed/cast data
            if (damaris_vartype == DAMARIS_TYPE_INT ) {
                const int*  select_block_data = static_cast<int*>(select_block->GetDataSpace().GetData());
                std::copy(select_block_data, select_block_data + select_block_size, select_data->data()) ;
            } else if (damaris_vartype == DAMARIS_TYPE_UINT ) {
                const unsigned int*  select_block_data = static_cast<unsigned int*>(select_block->GetDataSpace().GetData());
                std::copy(select_block_data, select_block_data + select_block_size, select_data->data()) ;
            } else if (damaris_vartype == DAMARIS_TYPE_LONG ) {
                const long int *  select_block_data = static_cast<long int*>(select_block->GetDataSpace().GetData());
                std::copy(select_block_data, select_block_data + select_block_size, select_data->data()) ;
            } else if (damaris_vartype == DAMARIS_TYPE_ULONG ) {
                const unsigned long int*  select_block_data = static_cast<unsigned long int*>(select_block->GetDataSpace().GetData());
                std::copy(select_block_data, select_block_data + select_block_size, select_data->data()) ;
            } else {
                ERROR("HDF5Store::SetSelectData() : The Damaris type is not avialable for conversion ");
                return -1 ;
            }
            
        }
       // std::vector<hsize_t>* select_data_ptr ;
       // for ( auto& it : this->memSelect_) {
       //     if (it.second != nullptr) {
       //         select_data_ptr = it.second  ;
       //         hsize_t select_block_size = select_data_ptr->size() ;
       //         WARN("Iteration = " << iteration << " select_block_size = " << select_block_size ) ;
       //     }
       // }
        
        return number_local_blocks ;
               
    } 
    
    // Based on AR's original pull request code
    void HDF5Store::OutputCollectiveSelect(int32_t iteration, std::shared_ptr<Variable> v, std::shared_ptr<Variable> v_file_select, hid_t& fileId, hid_t& lcplId) {

        hid_t dsetId = -1;
        hid_t dtypeId = -1;
        hid_t fileSpace;
        hid_t fileSpace2;
        hid_t memSpace;
        hsize_t *memOffset;
        hsize_t *memDim;

        hid_t plistId = H5P_DEFAULT;

        BlocksByIteration::iterator begin;
        BlocksByIteration::iterator end;

        
        // This is correct, as the HDF5 select data (*coord) is serialised 
        // (see https://portal.hdfgroup.org/display/HDF5/H5S_SELECT_ELEMENTS)
        // However, due to serialisation, to get the number of elements selected, we need to divide the 
        // number of elements found in the select variable by the rank  of the data variable (v) ( i.e. by varDimensions)
        if (v_file_select->GetLayout()->GetDimensions() != 1) {
            assert(v_file_select->GetLayout()->GetDimensions() != 1);
            ERROR("HDF5::OutputCollectiveSelect The select variable needs to be a 1 dimensional array of indecies. Variable :" << v->GetName()  << ",  Select variable: " << v_file_select->GetName());
        }


        // write non-time-varying variables only in first iteration
        if ((not v->IsTimeVarying()) && (iteration > 0))
                return ;

        int varDimension;
        varDimension = v->GetLayout()->GetDimensions();

        hsize_t *globalDim;
        globalDim = new (std::nothrow) hsize_t[varDimension];

        // Get equivalent HDF5 type to the variable (this is specified in the layout)
        if (not GetHDF5Type(v->GetLayout()->GetType() , dtypeId))
            ERROR("HDF5:Unknown variable type " << v->GetLayout()->GetType());

        // I am currently assuming this is the number of local blocks of this server
        // We test if this is true in the assert() below
        v->GetBlocksByIteration(iteration, begin, end);  

        memOffset = new (std::nothrow) hsize_t[varDimension];
        memDim = new (std::nothrow) hsize_t[varDimension];

        if ((memOffset == NULL) || (memDim == NULL)) {
          ERROR("HDF5Store::OutputCollectiveSelect() : Failed to allocate memDim and memOffset memory " << v->GetName() );
        }
        

        int blockNum = 0;

        // On the first iteration, or if the select data variable is time varying then 
        // fill in the fileSelect_<> array with the transformed/cast data select data
        if ((v_file_select->IsTimeVarying() == true) ||  (iteration == 0)  || (this->fileSelect_.empty())  ){
            // set up the hd5f select data as a hsize_t array
            number_local_blocks_ =  this->SetSelectData( iteration, v_file_select, this->fileSelect_) ;
            if ( number_local_blocks_ <= 0) {
                ERROR("HDF5Store::OutputCollectiveSelect() : Failed to get select data for variable: " << v->GetName() << "\n from Damaris XML variable. Select variable name: " << v_file_select->GetName());
            }                
        }
        size_t select_block_size = 0;
        
        
        const int num_blocks_dist = std::distance(begin, end) ;                
        if (num_blocks_dist != number_local_blocks_) {
          ERROR("HDF5Store::OutputCollectiveSelect() : the number of blocks in the variable is not equal to the number of blocks in the select variable.  Variable :" << v->GetName()  << " (blocks: " << num_blocks_dist << "),  Select variable: " << v_file_select->GetName() << " (blocks: " << number_local_blocks_ << ")");
          assert(num_blocks_dist == number_local_blocks_) ;
          delete [] memOffset;
          delete [] memDim;
          delete [] globalDim;
          return ;
        }
        
        // Set the attribute string for the dataset - it will be reused for each block
        // Used in conjunction with CreateDatasetStringAttributes()
        std::string var_sttr_str = v->GetModelCommentVariable() ;
        this->SetAttributeString(var_sttr_str) ;
        
        // auto select_block_iter = select_begin;
        for(BlocksByIteration::iterator bid = begin; bid != end; bid ++) {

            std::shared_ptr<Block>  b = *bid;

            // Get the select data in correct form ready for H5Sselect_elements
            // An assumption is that the user specifies the same block number for 
            // the data variable and the select data.
            // These will be used as std::pair<block_Source, block_ID>
            int block_Source = b->GetSource() ;
            int block_ID = b->GetID() ;  
            
            // select_block_size = fileSelect_[block_ID]->size() / varDimension ;
            
            hsize_t * select_block_data = nullptr; // = fileSelect_[block_ID]->data() ;
            std::vector<hsize_t>* select_data ;
            std::map< std::pair<int, int>,   std::vector<hsize_t> * >::const_iterator search ;
            search = fileSelect_.find(std::pair<int,int>(block_Source, block_ID)) ; 
            if ( search != fileSelect_.end()) {
                select_data = search->second ;
                select_block_size = select_data->size() / varDimension ;
                select_block_data = select_data->data() ;
            }
            else
                ERROR("HDF5Store::OutputCollectiveSelect(): Failed to find block in Iteration: " << iteration << "   Variable:" <<  v->GetName()  << "\n with client rank (block_Source)= " << block_Source << " and block id=" << block_ID);
            
            
            std::string varName ;
            std::string logString_global("HDF5Store::OutputCollectiveSelect() globalDim ") ;
            if (blockNum == 0) {
                 // Obtain the FilesSpace size (has to match the memory space dimensions)
                varName = GetVariableFullName(v);
                logString_global += varName ;

                for (int i = 0; i < varDimension; i++) {
                    globalDim[i] = b->GetGlobalExtent(i);
                    logString_global += "[" + std::to_string(globalDim[i]) + "]" ;
                }

                // Create dataspace.
                if ((fileSpace = H5Screate_simple(varDimension, globalDim , NULL)) < 0)
                    ERROR("HDF5Store::OutputCollectiveSelect(): file space creation failed !" <<  v->GetName());

                // Create the dataset
                if ((dsetId = H5Dcreate( fileId, varName.c_str(), dtypeId, fileSpace,
                         lcplId, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                    ERROR("HDF5Store::OutputCollectiveSelect(): Failed to create dataset ... " << varName.c_str());

                H5Sclose(fileSpace);
            } else {
                varName = GetVariableFullName(v);
                logString_global += varName ;
            }      
            
             // Obtain the starting indices and the size of the hyperslab
            std::string logString_offset("HDF5Store::OutputCollectiveSelect() memOffset") ;
            std::string logString_dim("HDF5Store::OutputCollectiveSelect()    memDim") ;
            for(int i = 0; i < varDimension; i++) {
                 memOffset[i]  = b->GetStartIndex(i);
                 memDim[i]     = b->GetEndIndex(i) - b->GetStartIndex(i) + 1;
                 logString_offset += "[" + std::to_string(memOffset[i]) + "]" ;
                 logString_dim    += "[" + std::to_string(memDim[i]) + "]" ;                     
             }

             // Create memory data space 
             // It is the full size of local domain of the varaible in memory
             memSpace = H5Screate_simple(varDimension, memDim , NULL);

             // Update ghost zones (N.B. untested)
             // UpdateGhostZones(v , memSpace , memDim);

             // Select the points in the file.
             fileSpace2 = H5Dget_space(dsetId);
             if (select_block_data != nullptr) {
                 if (H5Sselect_elements(fileSpace2, H5S_SELECT_SET, select_block_size, select_block_data ) < 0) {
                     ERROR("HDF5Store::OutputCollectiveSelect(): H5Sselect_elements failed for block.  Iteration: " << iteration << "   Variable:" <<    v->GetName() );
                 }
             }

             // Create property list for collective dataset write.
             plistId = H5Pcreate(H5P_DATASET_XFER);
             H5Pset_dxpl_mpio(plistId, H5FD_MPIO_COLLECTIVE);

             void* ptr = b->GetDataSpace().GetData();

             if (H5Dwrite(dsetId, dtypeId, memSpace, fileSpace2, plistId, ptr) < 0) {
                 ERROR("HDF5Store::OutputCollectiveSelect(): Writing blocks to the file failed. Iteration: " << iteration << "   Variable:" <<   v->GetName());
             } 
             blockNum++ ;
             H5Sclose(fileSpace2);
             H5Sclose(memSpace);
             H5Pclose(plistId);
             
             Environment::Log(logString_global , EventLogger::Debug); // only updated when the counter blockNum==0
             Environment::Log(logString_dim ,    EventLogger::Debug);
             Environment::Log(logString_offset , EventLogger::Debug);
             
             // ++select_block_iter ;
             
        } // for the blocks loop
        
        this->CreateDatasetStringAttributes(dsetId, iteration) ;
         
        delete [] memOffset;
        delete [] memDim;
        delete [] globalDim;
        if (dsetId >= 0) H5Dclose(dsetId);

    } 
    
    
    
} // namespace damaris



