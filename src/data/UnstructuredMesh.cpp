/**************************************************************************
This file is part of Damaris.

Damaris is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Damaris is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Damaris.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "env/Environment.hpp"
#include "data/Variable.hpp"
#include "data/VariableManager.hpp"
#include "damaris/data/UnstructuredMesh.hpp"


namespace damaris {

#ifdef HAVE_VISIT_ENABLED
bool UnstructuredMesh::ExposeVisItMetaData(visit_handle md)
{
    DBG("In UnstructuredMesh::ExposeVisItMetaData");
	return false;
}

bool UnstructuredMesh::ExposeVisItData(visit_handle* h, 
	int source, int iteration, int block) 
{
	//const model::Mesh& model = GetModel();
	DBG("In UnstructuredMesh::ExposeVisItData");
	
	return (false);
}

#endif

#ifdef HAVE_PARAVIEW_ENABLED


void UnstructuredMesh::SetNVerticies(size_t n_verticies)
{
    // The total number of (x,y,z) tuples to describe the vertex positions
    // in 3d space
    n_verticies_ = n_verticies ;
}

 
bool UnstructuredMesh::GetGridVariables(std::shared_ptr<Variable>& coords_xyz,
                                        std::shared_ptr<Variable>& coords_y,
                                        std::shared_ptr<Variable>& coords_z,
                                        std::shared_ptr<Variable>& vertex_gid,
                                        std::shared_ptr<Variable>& section_vtk_type,
                                        std::shared_ptr<Variable>& section_vtk_sizes,
                                        std::shared_ptr<Variable>& element_vtk_offsets,
                                        std::shared_ptr<Variable>& vertex_connectivity,
                                        std::shared_ptr<Variable>& polyhedral_cell_faces_connectivity,
                                        std::shared_ptr<Variable>& polyhedral_cell_faces_offsets,
                                        std::shared_ptr<Variable>& polyhedral_n_faces_per_cell
                                        )
{
    unsigned int numCoord = GetNumCoord();
    /*if (numCoord != 1) {
        CFGERROR("Type UnstructuredMesh should have a single coord, with [][3] "
                "(for topology==3) or [][2] (for topology==2) layout dimension");
        return false;
    }*/

    coords_xyz                    = GetCoord(0);  // this is either the x cordinate (if AoS) or a SoA x,y,z,x,y,z,... structure
    coords_y                      = GetCoord(1);  // This could be an empty std::shared_ptr<Variable> if AoS x,y,z data given
    coords_z                      = GetCoord(2);  // This could be an empty std::shared_ptr<Variable>  if AoS x,y,z data given
    vertex_gid                    = GetVertexGID();
    section_vtk_type              = GetSectionVTKType();
    section_vtk_sizes             = GetSectionSizes();
    element_vtk_offsets           = GetElementOffsets();
    vertex_connectivity           = GetSectionVertexConnectivity();
    polyhedral_cell_faces_connectivity = GetPolyhedralCellFacesConnectivity(); // This could be an empty if no Polyhedral elements present
    polyhedral_cell_faces_offsets = GetPolyhedralCellFacesOffsets(); // This could be an empty if no Polyhedral elements present
    polyhedral_n_faces_per_cell   = GetPolyhedralNFacesPerCell() ; // This could be an empty if no Polyhedral elements present

    return true;
}



vtkDataSet* UnstructuredMesh::GetVtkGrid(int source , int iteration , int block ,
                                       const std::shared_ptr<Variable>& var)
{
    std::shared_ptr<Variable> coords_xyz = nullptr;
    std::shared_ptr<Variable> coords_y   = nullptr;  // used only if coordinates are presented as separate arrays
    std::shared_ptr<Variable> coords_z   = nullptr;  // used only if coordinates are presented as separate arrays
    std::shared_ptr<Variable> vertex_gid = nullptr;
    std::shared_ptr<Variable> section_vtk_type = nullptr;
    std::shared_ptr<Variable> section_vtk_sizes = nullptr;  // used in codes_saturne based compact representation - it is how many cells of a particular type to create
    std::shared_ptr<Variable> element_vtk_offsets = nullptr;
    std::shared_ptr<Variable> vertex_connectivity = nullptr;
    
    std::shared_ptr<Variable> polyhedral_cell_faces_connectivity = nullptr;
    std::shared_ptr<Variable> polyhedral_cell_faces_offsets = nullptr;
    std::shared_ptr<Variable> polyhedral_n_faces_per_cell = nullptr;
    // Create the proper vtkGrid Object, dependent on grid type (in this case, unstructured)

    vtkUnstructuredGrid* grid ;
    if ( IsNull(source, block) ) {
        // std::cout << "UnstructuredMesh::GetVtkGrid(): Setting up vtk grid pointer. Source: " << source << "   Block: " << block <<std::endl ;
        std::stringstream s;
        s << "UnstructuredMesh::GetVtkGrid(): Setting up vtk grid pointer. Iteration: " << iteration << "  Source: " << source << "   Block: " << block <<std::endl ;
        Environment::Log(s.str(), EventLogger::Debug);
        grid = vtkUnstructuredGrid::SafeDownCast
                    (vtkDataSet::SafeDownCast(CreateVtkGrid(source, block)));

        // Getting the grid info. Not all Variables will be returned - 
        //  - Coordinates may be returend as xyz, or separate x, y and z Variables
        //  - Section_vtk_sizes is kind of mutually exclusive to element_vtk_offsets)
        if (not GetGridVariables(coords_xyz ,
                                 coords_y ,
                                 coords_z ,
                                 vertex_gid ,
                                 section_vtk_type,
                                 section_vtk_sizes,
                                 element_vtk_offsets,
                                 vertex_connectivity,
                                 polyhedral_cell_faces_connectivity,
                                 polyhedral_cell_faces_offsets,
                                 polyhedral_n_faces_per_cell)) {
            ERROR("Failed to get variables related to the vtkGrid of: " << GetName());
            return nullptr;
        }

        vtkPoints * vtkPoints ;
        // Setting the vertex point coordinates 
        // First is for array of structures (x,y,z,x,y,z,x,y,z,...)
        if ((coords_xyz != nullptr) && (coords_y == nullptr) && (coords_z == nullptr)) {
            vtkPoints = SetVertexCoords( source ,  iteration ,  block , coords_xyz) ;
            if (vtkPoints == nullptr ) {
                ERROR("Setting the unstructured mesh vertex AoS coordinates failed: Mesh name: " << GetName()) ;
                return nullptr;
            }
        // Second is for structure of arrays (x,x,x,...) (y,y,y,...),(z,z,z,...)
        } else if ((coords_xyz != nullptr) && (coords_y != nullptr) && (coords_z != nullptr)) {
            vtkPoints = SetVertexCoords( source ,  iteration ,  block , coords_xyz, coords_y, coords_z) ;
            if (vtkPoints == nullptr ) {
                ERROR("Setting the unstructured mesh vertex SoA coordinates failed: Mesh name: " << GetName()) ;
                return nullptr;
            }
        } else {
            ERROR("Setting the unstructured mesh vertex coordinates failed: Mesh name: " << GetName()) ;
            return nullptr;
        }

        // It seems that the GID mapping is not a hard requirement This is assumed as Ascent 
        // does not need it in building distributed meshes
        if (vertex_gid != nullptr) {
            //Setting the global ID's
            vtkIdTypeArray * vtkGid =  SetGlobalIDs(  source , iteration , block ,vertex_gid) ;
            if (vtkGid == nullptr ) {
                ERROR("Setting the unstructured mesh global ids failed! Mesh name: " << GetName()) ;
                return nullptr;
            }

            grid->GetPointData()->SetGlobalIds(vtkGid) ;
            vtkGid->Delete() ;
        }

        grid->SetPoints(vtkPoints) ;
        vtkPoints->Delete() ;

        // This does a loop over all sections, adding the connectivities which are specific for
        // each vtk mesh element type 
        // This combination is based on Code_Saturne data structures - elements are specified in blocks
        // of size specified by the section_vtk_sizes data. No Polygon or Polyhedral elements are available
        // in the current implementation
        if (section_vtk_sizes != nullptr) {
            if ( (section_vtk_type->GetLayout()->GetType() == model::Type::int_) &&  
                 (section_vtk_sizes->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::long_) ) {
                    
                if (not SetVtkConnections<int, int, long>(grid, source, iteration, block,
                                          section_vtk_type,
                                          section_vtk_sizes,
                                          vertex_connectivity)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
           } else if  ( (section_vtk_type->GetLayout()->GetType() == model::Type::char_) &&  
                 (section_vtk_sizes->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::long_) ) {
                     if (not SetVtkConnections<char, int, long>(grid, source, iteration, block,
                                          section_vtk_type,
                                          section_vtk_sizes,
                                          vertex_connectivity)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
                     
            } else if  ( (section_vtk_type->GetLayout()->GetType() == model::Type::int_) &&  
                 (section_vtk_sizes->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::int_) ) {
                     if (not SetVtkConnections<int, int, int>(grid, source, iteration, block,
                                          section_vtk_type,
                                          section_vtk_sizes,
                                          vertex_connectivity)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
                     
           } else if  ( (section_vtk_type->GetLayout()->GetType() == model::Type::char_) &&  
                 (section_vtk_sizes->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::int_) ) {
                     if (not SetVtkConnections<char, int, int>(grid, source, iteration, block,
                                          section_vtk_type,
                                          section_vtk_sizes,
                                          vertex_connectivity)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
                     
           }
        }
        
        // This data is more like Conduit Blueprint data. Polyhedral elements are defined.
        if (element_vtk_offsets != nullptr) {
            if ( (section_vtk_type->GetLayout()->GetType() == model::Type::int_) &&  
                 (element_vtk_offsets->GetLayout()->GetType() == model::Type::long_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::long_) ) {
                if (not SetVtkConnectionsWithElements<int, long, long>(grid, source, iteration, block,
                                          section_vtk_type,
                                          element_vtk_offsets,
                                          vertex_connectivity,
                                          polyhedral_cell_faces_connectivity,
                                          polyhedral_cell_faces_offsets,
                                          polyhedral_n_faces_per_cell)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
           } else if ( (section_vtk_type->GetLayout()->GetType() == model::Type::int_) &&  
                 (element_vtk_offsets->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::long_) ) {
                if (not SetVtkConnectionsWithElements<int, int, long>(grid, source, iteration, block,
                                          section_vtk_type,
                                          element_vtk_offsets,
                                          vertex_connectivity,
                                          polyhedral_cell_faces_connectivity,
                                          polyhedral_cell_faces_offsets,
                                          polyhedral_n_faces_per_cell)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
           } else if  ( (section_vtk_type->GetLayout()->GetType() == model::Type::int_) &&  
                 (element_vtk_offsets->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::int_) ) {
                     if (not SetVtkConnectionsWithElements<int, int, int>(grid, source, iteration, block,
                                          section_vtk_type,
                                          element_vtk_offsets,
                                          vertex_connectivity,
                                          polyhedral_cell_faces_connectivity,
                                          polyhedral_cell_faces_offsets,
                                          polyhedral_n_faces_per_cell)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
            }  else if  ( (section_vtk_type->GetLayout()->GetType() == model::Type::char_) &&  
                 (element_vtk_offsets->GetLayout()->GetType() == model::Type::long_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::long_) ) {
                     if (not SetVtkConnectionsWithElements<char, long, long>(grid, source, iteration, block,
                                          section_vtk_type,
                                          element_vtk_offsets,
                                          vertex_connectivity,
                                          polyhedral_cell_faces_connectivity,
                                          polyhedral_cell_faces_offsets,
                                          polyhedral_n_faces_per_cell)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
            } else if  ( (section_vtk_type->GetLayout()->GetType() == model::Type::char_) &&  
                 (element_vtk_offsets->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::long_) ) {
                     if (not SetVtkConnectionsWithElements<char, int, long>(grid, source, iteration, block,
                                          section_vtk_type,
                                          element_vtk_offsets,
                                          vertex_connectivity,
                                          polyhedral_cell_faces_connectivity,
                                          polyhedral_cell_faces_offsets,
                                          polyhedral_n_faces_per_cell)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
           } else if  ( (section_vtk_type->GetLayout()->GetType() == model::Type::char_) &&  
                 (element_vtk_offsets->GetLayout()->GetType() == model::Type::int_) &&  
                 (vertex_connectivity->GetLayout()->GetType() == model::Type::int_) ) {
                     if (not SetVtkConnectionsWithElements<char, int, int>(grid, source, iteration, block,
                                          section_vtk_type,
                                          element_vtk_offsets,
                                          vertex_connectivity,
                                          polyhedral_cell_faces_connectivity,
                                          polyhedral_cell_faces_offsets,
                                          polyhedral_n_faces_per_cell)) {
                    ERROR("Failed to set the vertex connections in the vtkGrid of: " << GetName());
                    return nullptr;
                }
           }
        }

    } else {
        // std::cout << "INFO: Returning the cached grid pointer " << std::endl ;
        std::stringstream s;
        s << "UnstructuredMesh::GetVtkGrid(): Returning the cached grid pointer. Iteration: " << iteration << "  Source: " << source << "   Block: " << block <<std::endl ;
        Environment::Log(s.str(), EventLogger::Debug);
        grid = ReturnVTKMeshPtr(source, block) ;
    }

    return grid;
}

template <typename ITYPE, typename ISIZE, typename ICON>
bool UnstructuredMesh::SetVtkConnectionsWithElements(vtkDataSet* grid,  int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& section_vtk_type,
                                     const std::shared_ptr<Variable>& element_vtk_offsets,
                                     const std::shared_ptr<Variable>& vertex_connectivity,
                                     const std::shared_ptr<Variable>& polyhedral_cell_faces_connectivity,
                                     const std::shared_ptr<Variable>& polyhedral_cell_faces_offsets,
                                     const std::shared_ptr<Variable>& polyhedral_n_faces_per_cell
                                     )
{
    size_t vtk_num_items ; // This is the size of the section_vtk_type and element_vtk_offsets Variables
    const ITYPE * vtk_type_ptr ;
    const ISIZE * vtk_elements_offsets_ptr ;  // offsets into the vertex_connectivity  (vertex ids) array
    const ICON * vrtx_connect_ptr ;

    vtkUnstructuredGrid* uGrid = vtkUnstructuredGrid::SafeDownCast(grid);

    if (uGrid == nullptr) {
            ERROR("Cannot downcast the parameter grid to vtkUnstructuredGrid");
            return false;
    }

    // Get the array of VTK mesh element types (VTK_LINE, VTK_QUAD etc.), one element type for each mesh section
    std::shared_ptr<Block> b_types = ReturnBlock(source ,  iteration ,  block , section_vtk_type) ;
    vtk_type_ptr                   = static_cast<const ITYPE *>(ReturnBlockDataPtr<ITYPE>( b_types )) ;
    vtk_num_items                  = b_types->GetNumberOfItems();
    
    // Get the array for the mesh offsets. 
    std::shared_ptr<Block>  b_offsets = ReturnBlock(source ,  iteration ,  block , element_vtk_offsets) ;
    vtk_elements_offsets_ptr            = static_cast<const ISIZE *>(ReturnBlockDataPtr<ISIZE>( b_offsets )) ;
    // The offsets size should *may need to be* be 1 more (?) than the number of elements of the VTK type array block, as one extra offset
    // should be added incase a polygon/polyhedral element is the last element and we need to find the size of its vtxids section.
    if (vtk_num_items != (size_t) (b_offsets->GetNumberOfItems() -1)) { // check the sizes
        ERROR("The section size for mesh " << GetName()  << " variable named: " << element_vtk_offsets->GetName()
                << " in iteration " << iteration << " does not match the section size or the vtk_type variable: "
                << section_vtk_type->GetName() << " : " <<  (b_offsets->GetNumberOfItems() -1 ) << " vs " <<  vtk_num_items << std::endl );
               // return false ;
    }

    long total_elements = vtk_num_items ;
    if (total_elements > 0)
        uGrid->Allocate(total_elements);

    // Get the array of mesh connectivities.
    std::shared_ptr<Block>  b_vtxids = ReturnBlock(source ,  iteration ,  block , vertex_connectivity) ;
    vrtx_connect_ptr         = static_cast<const ICON *>(ReturnBlockDataPtr<const ICON>( b_vtxids )) ;

    int vtk_type_stride ;
    int vtk_type ;  // typically a char 
    int vert_gid_offset = GetModel().vertex_global_id().get().offset() ; // can also request offset()
    
    bool havePolyherdalData = false ;
    // Get the array for the mesh polyhedral cell faces. 
    const ICON * polyhedral_cell_face_connectivity_ptr = nullptr ;
    const ICON * polyhedral_n_faces_per_cell_ptr = nullptr ;
    const ICON * polyhedral_cell_faces_offsets_ptr = nullptr ;
    if ((polyhedral_cell_faces_connectivity != nullptr) && (polyhedral_cell_faces_offsets != nullptr) && (polyhedral_n_faces_per_cell != nullptr)) 
    {
        havePolyherdalData = true ;

        std::shared_ptr<Block>  b_poly_cell_connects    = ReturnBlock(source ,  iteration ,  block , polyhedral_cell_faces_connectivity) ;
        polyhedral_cell_face_connectivity_ptr          = static_cast<const ICON *>(ReturnBlockDataPtr<ICON>( b_poly_cell_connects )) ;
        
        std::shared_ptr<Block>  b_poly_cell_face_offset = ReturnBlock(source ,  iteration ,  block , polyhedral_cell_faces_offsets) ;
        polyhedral_cell_faces_offsets_ptr  = static_cast<const ICON *>(ReturnBlockDataPtr<ICON>( b_poly_cell_face_offset )) ;
        
        std::shared_ptr<Block>  b_poly_n_faces         = ReturnBlock(source ,  iteration ,  block , polyhedral_n_faces_per_cell) ;
        polyhedral_n_faces_per_cell_ptr    = static_cast<const ICON *>(ReturnBlockDataPtr<ICON>( b_poly_n_faces )) ;
    }
    size_t sectn_offset = 0 ;
    long n_element_tally = 0 ;  
    ICON n_faces_tally = 0 ;   // the ongoing offset into the faces offset array
    long n_polyhedral_faces_tally = 0 ; 
    for (long element_id = 0; element_id < total_elements; element_id++) 
    {
        vtk_type                   = vtk_type_ptr[element_id] ;
        ICON vtk_offset_strt       = vtk_elements_offsets_ptr[element_id] ;
        ICON vtk_offset_end        = vtk_elements_offsets_ptr[element_id+1] ;
        vtkIdType vtk_offset_step  =  vtk_offset_end - vtk_offset_strt ;
        vtk_type_stride            = strideofVtkType(vtk_type);  // vtk_type_stride should be equal to strideofVtkType for non-polyhedral / non-polygonal cell types
        
        if (vtk_type == VTK_POLYGON) 
        {
            
           /* elements: 
              shape: "polygonal"
              connectivity: [0, 3, 4, 1, 1, 4, 5, 2, 3, 6, 7, 4, 4, 7, 8, 5]
              sizes: [4, 4, 4, 4]
              offsets: [0, 4, 8, 12]
              */
            std::vector<vtkIdType> vtx_ids_vect(vtk_offset_step) ;
            std::copy(&vrtx_connect_ptr[vtk_offset_strt], &vrtx_connect_ptr[vtk_offset_end], vtx_ids_vect.data()) ; // we should add vert_gid_offset to be complete
            uGrid->InsertNextCell(VTK_POLYGON, vtk_offset_step,  vtx_ids_vect.data());

            n_element_tally++ ;
        }
        else if (vtk_type == VTK_POLYHEDRON) 
        {
             if (havePolyherdalData == true) 
             {
                // add the faces 
                ICON nfaces = polyhedral_n_faces_per_cell_ptr[ n_element_tally ] ;
                vtkNew<vtkIdList> faces; 
                for (int fid = 0 ; fid < nfaces; fid++){
                    ICON vtk_face_offset_strt = polyhedral_cell_faces_offsets_ptr[ n_faces_tally+fid ] ;
                    ICON vtk_face_offset_end  = polyhedral_cell_faces_offsets_ptr[ n_faces_tally+fid+1 ] ;
                    vtkIdType vtk_face_offset_step  =  vtk_face_offset_end - vtk_face_offset_strt ; // This is how many connectivity id's in this face
                    faces->InsertNextId(vtk_face_offset_step) ; 
                    std::vector<vtkIdType> vtx_face_ids_vect(vtk_face_offset_step) ;
                    // Copy to the correct data type
                    std::copy(&polyhedral_cell_face_connectivity_ptr[vtk_face_offset_strt], &polyhedral_cell_face_connectivity_ptr[vtk_face_offset_end], vtx_face_ids_vect.data()) ;
                    // Add each individual connection id for this face
                    for (int vid = 0 ; vid < vtk_face_offset_step ; vid++ ) {
                      faces->InsertNextId(vtx_face_ids_vect[vid]); 
                    }
                }
                // original: 
                // uGrid->InsertNextCell(VTK_POLYHEDRON, 8, pointIds_vect.data(), 6, faces->GetPointer(0));
                std::vector<vtkIdType> vtx_ids_vect(vtk_offset_step) ;
                std::copy(&vrtx_connect_ptr[vtk_offset_strt], &vrtx_connect_ptr[vtk_offset_end], vtx_ids_vect.data()) ;
                uGrid->InsertNextCell(VTK_POLYHEDRON, vtk_offset_step, vtx_ids_vect.data(), nfaces, faces->GetPointer(0));
                n_faces_tally += nfaces ;  // offset in to polyhedral_cell_faces_offsets
                n_element_tally++ ;
            } else {
                ERROR("The mesh cell type is VTK_POLYHEDRON, however no polyhedral arrays were specified in the mesh. Cannot add the cell. Mesh name: " << GetName()  << " in iteration " << iteration << std::endl) ;
            }
        } else {
            if (vtk_type_stride > 0) {
                 std::vector<vtkIdType> vtx_ids_vect(vtk_type_stride) ;
                 std::copy(&vrtx_connect_ptr[vtk_offset_strt], &vrtx_connect_ptr[vtk_offset_end], vtx_ids_vect.data()) ; // we should add vert_gid_offset to be complete
                 if (vert_gid_offset != 0 ) {
                     for(vtkIdType& d : vtx_ids_vect) {
                        d += vert_gid_offset; 
                     }
                 }
                 uGrid->InsertNextCell(vtk_type, vtk_type_stride, vtx_ids_vect.data());                 
            } else {
                ERROR("The section type for mesh " << GetName()  << " is " << vtk_type << " which returned an inconsistent stride (a non-positive stride) " << std::endl );
            }
            n_element_tally++ ;
        }
        
      } // End of loop on sections 

    return true ;

}
                                     
template <typename ITYPE, typename ISIZE, typename ICON>
bool UnstructuredMesh::SetVtkConnections(vtkDataSet* grid,  int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& section_vtk_type,
                                     const std::shared_ptr<Variable>& section_vtk_sizes,
                                     const std::shared_ptr<Variable>& vertex_connectivity
                                     )
{
    size_t vtk_num_sections ; // This is the size of the section_vtk_type and section_vtk_sizes Variables
    const ITYPE * vtk_type_ptr ;
    const ISIZE * vtk_sizes_ptr ;
    const ICON * vrtx_connect_ptr ;

    vtkUnstructuredGrid* uGrid = vtkUnstructuredGrid::SafeDownCast(grid);

    if (uGrid == nullptr) {
            ERROR("Cannot downcast the parameter grid to vtkUnstructuredGrid");
            return false;
    }

    // Get the array of VTK mesh element types (VTK_LINE, VTK_QUAD etc.), one element type for each mesh section
    std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , section_vtk_type) ;
    vtk_type_ptr             = static_cast<const ITYPE *>(ReturnBlockDataPtr<ITYPE>( b )) ;
    vtk_num_sections         = b->GetNumberOfItems();
    
    // Get the array of sizes for the mesh sections. It is the number of elements of the VTK type in this section
    // The mesh connectivity will then have vtk_sizes_ptr[x] * strideof(vtk_type_ptr[x]) indices into the vertex data.
    b = ReturnBlock(source ,  iteration ,  block , section_vtk_sizes) ;
    vtk_sizes_ptr            = static_cast<const ISIZE *>(ReturnBlockDataPtr<ISIZE>( b )) ;
    if (vtk_num_sections != (size_t) b->GetNumberOfItems()) { // check they are the same size (they should use the same layout)
        ERROR("The section size for mesh " << GetName()  << " variable named: " << section_vtk_sizes->GetName()
                << " in iteration " << iteration << " does not match the section size or the vtk_type variable: "
                << section_vtk_type->GetName() << std::endl );
                return false ;
    }

    long total_elements = 0 ;
    for (size_t t1 = 0 ; t1 < vtk_num_sections; t1++)
    {
        total_elements += vtk_sizes_ptr[t1] ;
    }
    if (total_elements > 0)
        uGrid->Allocate(total_elements);


    // Get the array of mesh connectivities.
    b = ReturnBlock(source ,  iteration ,  block , vertex_connectivity) ;
    vrtx_connect_ptr         = static_cast<const ICON *>(ReturnBlockDataPtr<const ICON>( b )) ;


    int vtk_type_stride ;
    int vtk_type ;
    int vert_gid_offset = GetModel().vertex_global_id().get().offset() ; // can also request offset()
    vtkIdType *vtx_ids = new vtkIdType[8]; // 8 is the maximum size required

    size_t sectn_offset = 0 ;
    for (size_t sectn_id = 0; sectn_id < vtk_num_sections; sectn_id++) {
        vtk_type         = vtk_type_ptr[sectn_id] ;
        vtk_type_stride  = strideofVtkType(vtk_type);
        if (vtk_type_stride > 0) {
              for (int t1 = 0; t1 < vtk_sizes_ptr[sectn_id]; t1++) {
                  for (int t2 = 0; t2 < vtk_type_stride; t2++)
                      vtx_ids[t2] = vrtx_connect_ptr[sectn_offset + (t1*vtk_type_stride) + t2] + vert_gid_offset;
                  uGrid->InsertNextCell(vtk_type, vtk_type_stride, vtx_ids);
              }
              sectn_offset += vtk_sizes_ptr[sectn_id] * vtk_type_stride;
        }
        else if (vtk_type == VTK_POLYGON) {
            // _export_nodal_polygons(section, ugrid);
            ERROR("The section type for mesh " << GetName()  << " is VTK_POLYGON which is not implemented in current (development) version " << std::endl );
            return false ;
        }
        else if (vtk_type == VTK_POLYHEDRON) {
            // _export_nodal_polyhedra(mesh->n_vertices, section, ugrid);
            ERROR("The section type for mesh " << GetName()  << " is VTK_POLYHEDRON which is not implemented in current (development) version " << std::endl );
            return false ;
        }
      } // End of loop on sections 

    delete [] vtx_ids;

    return true ;

}

/*
template bool UnstructuredMesh::SetVtkConnections<int, int, long int>(vtkDataSet* grid,  int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& section_vtk_type,
                                     const std::shared_ptr<Variable>& section_vtk_sizes,
                                     const std::shared_ptr<Variable>& vertex_connectivity
                                     ) ;
                                     
template bool UnstructuredMesh::SetVtkConnections<int, int, int>(vtkDataSet* grid,  int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& section_vtk_type,
                                     const std::shared_ptr<Variable>& section_vtk_sizes,
                                     const std::shared_ptr<Variable>& vertex_connectivity
                                     ) ;    
                                     
template bool UnstructuredMesh::SetVtkConnections<char, int, long int>(vtkDataSet* grid,  int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& section_vtk_type,
                                     const std::shared_ptr<Variable>& section_vtk_sizes,
                                     const std::shared_ptr<Variable>& vertex_connectivity
                                     ) ;       
                                     
template bool UnstructuredMesh::SetVtkConnections<char, int, int>(vtkDataSet* grid,  int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& section_vtk_type,
                                     const std::shared_ptr<Variable>& section_vtk_sizes,
                                     const std::shared_ptr<Variable>& vertex_connectivity
                                     ) ;             
                                     
*/

/*bool UnstructuredMesh::SetVtkConnections(vtkDataSet* grid,  int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& section_vtk_type,
                                     const std::shared_ptr<Variable>& section_vtk_sizes,
                                     const std::shared_ptr<Variable>& vertex_connectivity
                                     )
{
    size_t vtk_num_sections ; // This is the size of the section_vtk_type and section_vtk_sizes Variables
    const int * vtk_type_ptr ;
    const int * vtk_sizes_ptr ;
    const unsigned long * vrtx_connect_ptr ;

    vtkUnstructuredGrid* uGrid = vtkUnstructuredGrid::SafeDownCast(grid);

    if (uGrid == nullptr) {
            ERROR("Cannot downcast the parameter grid to vtkUnstructuredGrid");
            return false;
    }

    // Get the array of VTK mesh element types (VTK_LINE, VTK_QUAD etc.), one element type for each mesh section
    if (section_vtk_type->GetLayout()->GetType() == model::Type::int_) {
        std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , section_vtk_type) ;
        vtk_type_ptr             = static_cast<const int *>(ReturnBlockDataPtr<int>( b )) ;
        vtk_num_sections         = b->GetNumberOfItems();
    } else {
        ERROR("The section type for mesh " << GetName()  << " variable named: " << section_vtk_type->GetName()
                << " in iteration " << iteration << " does not have the correct data type (requires int) " << std::endl );
        return false ;
    }

    // Get the array of sizes for the mesh sections. It is the number of elements of the VTK type in this section
    // The mesh connectivity will then have vtk_sizes_ptr[x] * strideof(vtk_type_ptr[x]) indices into the vertex data.
    if (section_vtk_sizes->GetLayout()->GetType() == model::Type::int_) {
        std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , section_vtk_sizes) ;
        vtk_sizes_ptr            = static_cast<const int *>(ReturnBlockDataPtr<int>( b )) ;
        if (vtk_num_sections != (size_t) b->GetNumberOfItems()) { // check they are the same size (they should use the same layout)
            ERROR("The section size for mesh " << GetName()  << " variable named: " << section_vtk_sizes->GetName()
                    << " in iteration " << iteration << " does not match the section size or the vtk_type variable: "
                    << section_vtk_type->GetName() << std::endl );
                    return false ;
        }
    } else {
        ERROR("The section type for mesh " << GetName() << " variable named:  " << section_vtk_sizes->GetName()
                << " in iteration " << iteration << " does not have the correct data type (requires int) " << std::endl );
        return false ;
    }

    long int n_elts = 0 ;
    for (size_t t1 = 0 ; t1 < vtk_num_sections; t1++)
    {
        n_elts += vtk_sizes_ptr[t1] ;
    }
    if (n_elts > 0)
        uGrid->Allocate(n_elts);


    // Get the array of mesh connectivities.
    if (vertex_connectivity->GetLayout()->GetType() == model::Type::long_) {
        std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , vertex_connectivity) ;
        vrtx_connect_ptr         = static_cast<const unsigned long *>(ReturnBlockDataPtr<const unsigned long>( b )) ;
    } else {
        ERROR("The section type for mesh " << GetName()  << " variable named: " << section_vtk_sizes->GetName()
                << " in iteration " << iteration << " does not have the correct data type (requires long) " << std::endl );
        return false ;
    }

    int vtk_type_stride ;
    int vtk_type ;
    int vert_gid_offset = GetModel().vertex_global_id().get().offset() ; // can also request offset()
    vtkIdType *vtx_ids = new vtkIdType[8]; // 8 is the maximum size required

    size_t sectn_offset = 0 ;
    for (size_t sectn_id = 0; sectn_id < vtk_num_sections; sectn_id++) {
        vtk_type         = vtk_type_ptr[sectn_id] ;
        vtk_type_stride  = strideofVtkType(vtk_type);
        if (vtk_type_stride > 0) {
              for (int t1 = 0; t1 < vtk_sizes_ptr[sectn_id]; t1++) {
                  for (int t2 = 0; t2 < vtk_type_stride; t2++)
                      vtx_ids[t2] = vrtx_connect_ptr[sectn_offset + (t1*vtk_type_stride) + t2] + vert_gid_offset;
                  uGrid->InsertNextCell(vtk_type, vtk_type_stride, vtx_ids);
              }
              sectn_offset += vtk_sizes_ptr[sectn_id] * vtk_type_stride;
        }
        else if (vtk_type == VTK_POLYGON) {
            // _export_nodal_polygons(section, ugrid);
            ERROR("The section type for mesh " << GetName()  << " is VTK_POLYGON which is not implemented in current (development) version " << std::endl );
            return false ;
        }
        else if (vtk_type == VTK_POLYHEDRON) {
            // _export_nodal_polyhedra(mesh->n_vertices, section, ugrid);
            ERROR("The section type for mesh " << GetName()  << " is VTK_POLYHEDRON which is not implemented in current (development) version " << std::endl );
            return false ;
        }


      } // End of loop on sections 

    delete [] vtx_ids;

    return true ;

}*/


// For array of structs vertex data: (x,y,z,x,y,z,x,y,z,...)
vtkPoints * UnstructuredMesh::SetVertexCoords( int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& varVerticies)
{

    const double * vertexdata ;
    size_t vertexSize ;
    // Assuming model::Type::double_:
    //double * vertexdata = (double *) b->GetDataSpace().GetData();
    if (varVerticies->GetLayout()->GetType() == model::Type::double_) {
        std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , varVerticies) ;
        vertexdata               = static_cast<const double *>(ReturnBlockDataPtr<double>( b ) );
        vertexSize                = b->GetNumberOfItems();
    } else {
        ERROR("The vertex data for variable " << varVerticies->GetName() << " in iteration " << iteration
                        << " does not have the correct data type (requires double) " << std::endl );
        return nullptr ;
    }

    // The size of the data block should be 3 x n_verticies_for topology 3 mesh
    // or 2 x n_verticies_for topology 2 mesh.
    // This is enforced through the [][2 | 3] layout

    int topology = GetTopology() ;

    if (topology == 3) {
        if ((vertexSize % 3) != 0) { // in case the layout is not correct
            ERROR("The unstructured coordinate blocks for variable " << varVerticies->GetName() << " in iteration " << iteration
                << " does not have the correct number of coordinates (not a factor of 3 for x,y,z coordinates) "
                  << std::endl );
            return nullptr ;
        }

        n_verticies_ = vertexSize / 3 ;

    } else if (topology == 2) {
        if ((vertexSize % 2) != 0) { // in case the layout is not correct
            ERROR("The unstructured coordinate blocks for variable " << varVerticies->GetName() << " in iteration " << iteration
                << " does not have the correct number of coordinates (not a factor of 2 for x,y coordinates) "
                  << std::endl );
            return nullptr ;
        }

        n_verticies_ = vertexSize / 2 ;
    } else {
        ERROR("The topology value for the mesh is not a valide value (2 or 3)" << std::endl );
        return nullptr ;
    }

    double point[3] ;
    vtkPoints * points = vtkPoints::New() ;
    points->Allocate(n_verticies_) ;

    if (topology == 2)
        point[2] = 0.0 ;

    //std::cout << "Source: "<< source << "  block : " << block << std::endl ;
    for (size_t t1 = 0 ; t1 < n_verticies_ ; t1++){

        for (int t2 = 0 ; t2 < topology ; t2++)
            point[t2] = vertexdata[t1*topology + t2] ;

        points->InsertNextPoint(point[0],point[1],point[2]) ;
        //std::cout << " " << point[0] << ","  << point[1] << "," << point[2] << std::endl  ;
    }

    return points;
}



// For structure of arrays (x,x,x,...) (y,y,y,...),(z,z,z,...) vertex data
vtkPoints * UnstructuredMesh::SetVertexCoords( int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& x_verticies,
                                     const std::shared_ptr<Variable>& y_verticies,
                                     const std::shared_ptr<Variable>& z_verticies)
{

    const double * x_vertexdata, * y_vertexdata, * z_vertexdata ;
    size_t x_vertexSize, y_vertexSize, z_vertexSize ;
    // Assuming model::Type::double_:
    //double * vertexdata = (double *) b->GetDataSpace().GetData();
    if (x_verticies->GetLayout()->GetType() == model::Type::double_) {
        std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , x_verticies) ;
        x_vertexdata               = static_cast<const double *>(ReturnBlockDataPtr<double>( b ) );
        x_vertexSize             = b->GetNumberOfItems();
    } else {
        ERROR("The vertex data for variable " << x_verticies->GetName() << " in iteration " << iteration
                        << " does not have the correct data type (requires double) " << std::endl );
        return nullptr ;
    }
    
    if (y_verticies->GetLayout()->GetType() == model::Type::double_) {
        std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , y_verticies) ;
        y_vertexdata               = static_cast<const double *>(ReturnBlockDataPtr<double>( b ) );
        y_vertexSize             = b->GetNumberOfItems();
    } else {
        ERROR("The vertex data for variable " << y_verticies->GetName() << " in iteration " << iteration
                        << " does not have the correct data type (requires double) " << std::endl );
        return nullptr ;
    }
    
    if (z_verticies->GetLayout()->GetType() == model::Type::double_) {
        std::shared_ptr<Block> b = ReturnBlock(source ,  iteration ,  block , z_verticies) ;
        z_vertexdata               = static_cast<const double *>(ReturnBlockDataPtr<double>( b ) );
        z_vertexSize             = b->GetNumberOfItems();
    } else {
        ERROR("The vertex data for variable " << z_verticies->GetName() << " in iteration " << iteration
                        << " does not have the correct data type (requires double) " << std::endl );
        return nullptr ;
    }
    
    
    if ((x_vertexSize == y_vertexSize )  && (x_vertexSize == z_vertexSize )) {
        n_verticies_  = x_vertexSize ;
    } else {
        ERROR("SetVertexCoords() : The number of vertices in the x y or z variable do not match" << std::endl );
        return nullptr ;
    }

    vtkPoints * points = vtkPoints::New() ;
    points->Allocate(n_verticies_) ;

    for (size_t t1 = 0 ; t1 < n_verticies_ ; t1++){
        points->InsertNextPoint(x_vertexdata[t1],y_vertexdata[t1],z_vertexdata[t1]) ;
    }

    return points;
}


vtkIdTypeArray * UnstructuredMesh::SetGlobalIDs(int source , int iteration , int block ,
                                     const std::shared_ptr<Variable>& varGID)
{

    vtkIdTypeArray * global_vtx_ids = vtkIdTypeArray::New() ;

    int vert_gid_offset = GetModel().vertex_global_id().get().offset() ; // the offset if needed

    global_vtx_ids->SetNumberOfComponents(1);
    global_vtx_ids->SetName("GlobalNodeIds");
    if (n_verticies_ == 0) {
        ERROR(" The n_verticies_ has not been set! Mesh is named: " << GetName() << ". (Has SetVertexCoords() been called first?) " << std::endl) ;
        return nullptr ;
    }
    global_vtx_ids->SetNumberOfTuples(n_verticies_);


    std::shared_ptr<Block> b = varGID->GetBlock(source , iteration , block);

    if (b == nullptr)  // no coord for this iteration
        b = varGID->GetBlock(source , 0 , block);

    if (b == nullptr) {
        ERROR("No coordinate blocks for variable " << varGID->GetName() << " in iteration " << iteration << std::endl );
        return nullptr;
    }

    // Assuming model::Type::long_:
    // std::shared_ptr<Layout> var_GID_layout = ;

    if (varGID->GetLayout()->GetType()  == model::Type::long_)
    {
        long int * giddata = (long int *) b->GetDataSpace().GetData();

        for (size_t t1 = 0 ; t1 < n_verticies_ ; t1++){
            vtkIdType idt = static_cast<unsigned long int>(giddata[t1]) + vert_gid_offset ;

            global_vtx_ids->SetTypedTuple(t1, &idt) ;
        }
    } else if (varGID->GetLayout()->GetType()  == model::Type::int_) {
         int * giddata = ( int *) b->GetDataSpace().GetData();

        for (size_t t1 = 0 ; t1 < n_verticies_ ; t1++){
            vtkIdType idt = static_cast<unsigned int>(giddata[t1]) + vert_gid_offset ;

            global_vtx_ids->SetTypedTuple(t1, &idt) ;
        }
    }

    return global_vtx_ids ;
}


int UnstructuredMesh::strideofVtkType(int VTK_TYPE)
{
  // case values are defined in VTK library header vtkCellType.h
  switch(VTK_TYPE) {

  case VTK_LINE:
    return 2 ;
    break;

  case VTK_TRIANGLE:
      return 3 ;
    break;

  case VTK_QUAD:
      return 4 ;
    break;

  case VTK_TETRA:
      return 4 ;
    break;

  case VTK_PYRAMID:
      return 5 ;
    break;

  case VTK_WEDGE:
      return 6 ;
    break;

  case VTK_HEXAHEDRON:
      return 8 ;
    break;

  case VTK_POLYGON:
      return -1 ;;
    break;

  case VTK_POLYHEDRON:
    return -1;
    break;

  default:
      ERROR("Stride for VTK_TYPE: " << VTK_TYPE << " was not valid" << std::endl );
  }

  return -2;
}


#endif


} // of namespace damaris
