/**************************************************************************
This file is part of Damaris.

Damaris is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Damaris is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Damaris.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/


#include "util/Debug.hpp"
#include "paraview/ParaViewAdaptor.hpp"
#include "damaris/data/MeshManager.hpp"
#include "damaris/data/VariableManager.hpp"

#include <sstream>


namespace damaris {

std::shared_ptr<ParaViewAdaptor> ParaViewAdaptor::instance_;


void ParaViewAdaptor::Initialize(MPI_Comm comm,
                                const model::Simulation::paraview_optional& mdl,
                                const std::string& simName)
{
    timestep_               = mdl.get().realtime_timestep();    // default is 0.1
    updatefreq_             = mdl.get().update_frequency();     // default is 1, but is this neededas the Catalyst script can set this also?
    end_iteration_          = mdl.get().end_iteration();        // default is 0 - should be tested for
    output_vtk_file_        = mdl.get().write_vtk();            // default is 0 => no vtk output, if > 0, then represents iteration frequency
    output_vtk_file_binary_ = mdl.get().write_vtk_binary();     // default is true, ouput VTK file has data stored as binary.
    
    this->all_scripts_empty_ = false ;
    
    mdl_ = mdl ;  // Do this first as other methods use the class copy of the model
    
    this->TestForScript() ;
    if (all_scripts_empty_ == false) 
    {
        if (processor_ == nullptr) {
            vtkMPICommunicatorOpaqueComm opaqueComm(&comm);
            if (output_vtk_file_ != 0) {
                // This is needed for the VTK parallel writer
                vtkMPIController *vtkController = vtkMPIController::New() ;
                vtkMPICommunicator *vtkComm = vtkMPICommunicator::New() ;
                vtkComm->InitializeExternal(&opaqueComm) ;
                vtkController->SetCommunicator(vtkComm) ;
                vtkMultiProcessController::SetGlobalController(vtkController) ;
                
            } 
            processor_ = vtkCPProcessor::New() ;
            processor_->Initialize(opaqueComm) ;
        } else {
            processor_->RemoveAllPipelines() ;
        }

        AddPythonPipeline();
    }
    
   
}


bool ParaViewAdaptor::ParaviewFileExists(std::string script_path, bool firstTest  ) {

    // From c++17  : std::filesystem::exists(file_);
    int retint = 0 ;
    std::string logString_Script ;
    std::ifstream paraviewfile ;
    bool parafile_exists = false ;
    int rank ;
    MPI_Comm comm_ ;
    comm_ = Environment::GetEntityComm() ;
    MPI_Comm_rank(comm_,&rank);
    
    if ((script_path.length() == 0) || (script_path == "#") ) {
        parafile_exists = false ;
        return parafile_exists ;
    }
    
    if (rank == 0) {
        paraviewfile.open(script_path);
        if(paraviewfile.fail()) {
            retint = 0 ;
        } else {
            retint = 1 ;
        }
        MPI_Bcast(&retint,1,MPI_INT,0,comm_);
    } else {
        
        MPI_Bcast(&retint,1,MPI_INT,0,comm_);  // recieve the value from rank 0
    }
    
    if (retint == 1) {
        parafile_exists = true ;
        if (rank == 0) {
            paraviewfile.close() ;
        }
        if (firstTest) {
            logString_Script = " ParaViewAdaptor::ParaFileExists() Script file being added  for Coprocessing: " +  script_path  ;
            Environment::Log(logString_Script , EventLogger::Info);
        }
        
    } else {
        parafile_exists = false ;
        if (firstTest) {
            logString_Script = " ParaViewAdaptor::ParaFileExists() Failed to open Script File : " +  script_path  ;
            Environment::Log(logString_Script , EventLogger::Info);
        }
    }
    
    return (parafile_exists) ;
}
    

void ParaViewAdaptor::AddPythonPipeline()
{
// Add the Python script
    model::ParaViewParam::script_sequence scripts = mdl_.get().script();
    model::ParaViewParam::script_const_iterator it ;

    for(it = scripts.begin(); it != scripts.end(); ++it) {
        std::string script_path = *it;

        vtkCPPythonScriptPipeline* pipeline = vtkCPPythonScriptPipeline::New();
        
        // Check if file is an empty string and remove blanks
        bool whiteSpacesOnly = std::all_of(script_path.begin(),script_path.end(),[](char i) { return std::isspace(i); }) ;
        if ( whiteSpacesOnly ) {
            script_path.clear();
        }
        
        if ( ParaviewFileExists(script_path, false) ) {
            pipeline->Initialize(script_path.c_str());
            processor_->AddPipeline(pipeline);

            pipeline->Delete();
        } 
    }
}


void ParaViewAdaptor::TestForScript()
{
// Add the Python script
    model::ParaViewParam::script_sequence scripts = mdl_.get().script();
    model::ParaViewParam::script_const_iterator it ;
    const int num_scripts = std::distance(scripts.begin(), scripts.end()) ;
    int empty_scripts = 0 ;
    
    for(it = scripts.begin(); it != scripts.end(); ++it) {
        std::string script_path = *it;
        // Check if file is an empty string and remove blanks
        bool whiteSpacesOnly = std::all_of(script_path.begin(),script_path.end(),[](char i) { return std::isspace(i); }) ;
        if ( whiteSpacesOnly ) {
            script_path.clear();
        }
        
        if ( ! ParaviewFileExists(script_path, true) ) {
            empty_scripts++ ;  // either string is empty or the path does not exist
        }
    }
    
    if (num_scripts == empty_scripts) {
        this->all_scripts_empty_ = true ;
    }        
}



void ParaViewAdaptor::Finalize()
{
    if (processor_ != nullptr) {
        processor_->Delete();
        processor_ = nullptr;
    }
}

void ParaViewAdaptor::CoProcess(const  int iteration)
{
    
    if (this->all_scripts_empty_ == false) {
        vtkNew<vtkCPDataDescription> dataDescription;

        bool lastTimeStep = false ;
        // specify the simulation time and time step for Catalyst
        dataDescription->AddInput("input");

        dataDescription->SetTimeData(iteration*timestep_ , iteration);  // What is the difference? Simulation time and simulation iteration

        if (end_iteration_ != 0)
        {
            if (iteration == end_iteration_)
                lastTimeStep = true ;
        }
        // check: end_iteration_ from the xsd model. if not == 0 then use the value
        if (lastTimeStep == true) { // How to know about it?
            dataDescription->ForceOutputOn();
        }

        // This is influenced by the freq value in the catalyst python script coprocessor.RegisterWriter(.. freq=X
        // i.e. It only returns non-zero on the iterations that match the frequency
        if (processor_->RequestDataDescription(dataDescription) != 0) {
            int gridCount = MeshManager::GetNumObjects();

            if (gridCount < 1) {
                ERROR("No mesh has been defined in the Xml file!");
                Environment::Log("ParaViewAdaptor::CoProcess(): o mesh has been defined in the Xml file!", EventLogger::Error);
                return;
            }

            vtkNew<vtkMultiBlockDataSet> rootGrid;

            if (not FillMultiBlockGrid(iteration , rootGrid)) {
                // ERROR("Error in Filling the root multi-block grid in iteration " << iteration);
                Environment::Log("ParaViewAdaptor::CoProcess(): Error in Filling the root multi-block grid in iteration: "+std::to_string(iteration), EventLogger::Error);
                return;
            }

            // Catalyst gets the proper input dataset for the pipeline.
            dataDescription->GetInputDescriptionByName("input")->SetGrid(rootGrid);

            //if ( processor_->GetNumberOfPipelines() == 0)
            //    AddPythonPipeline();

            // Call Catalyst to execute the desired pipelines.
            //  if (iteration > 0)
            if ( processor_->GetNumberOfPipelines() > 0)
                processor_->CoProcess(dataDescription);
        }
    } else {
        // no scripts specified, so no processing avaialble (besides direct write to VTK file - which should be moved to the <storage> descriptions
        Environment::Log("ParaViewAdaptor::CoProcess(): No scripts specified, so no processing avaialble: "+std::to_string(iteration), EventLogger::Debug);
    }
}

// SetPiece
void ParaViewAdaptor::WriteVTKMBGridToFile(vtkMultiBlockDataSet* vtkMBDSGrid, std::string filename, bool writeVTKbinary )
{
    vtkNew<vtkXMLPMultiBlockDataWriter> writer;
    int serverRank = Environment::GetEntityProcessID();
    
    
    filename += "." ;
    filename += writer->GetDefaultFileExtension();
    writer->SetFileName(filename.c_str());
    
    std::string logstring = "WriteVTKMBGridToFile() called: writing file: " ;
    logstring += filename ;
    Environment::Log(logstring, EventLogger::Info);
    
    if (writeVTKbinary == true)
        writer->SetDataModeToBinary();
    else
        writer->SetDataModeToAscii();
    
    writer->SetInputData(vtkMBDSGrid);

    if (serverRank == 0)
    {
        writer->SetWriteMetaFile(1);
    }
    writer->Update();
    
    Environment::Log("ParaViewAdaptor::WriteVTKMBGridToFile() File written:" + filename, EventLogger::Info);
}

bool ParaViewAdaptor::FillMultiBlockGrid(const int iteration , vtkMultiBlockDataSet* rootGrid)
{
    // int index = 0;
    int serverCount = Environment::CountTotalServers();
    int meshCount = MeshManager::GetNumObjects();
    int serverRank = Environment::GetEntityProcessID();
    
    std::map<std::string, int> variable_block_map ;
    
    // This loop is to check what variables have multi-block data as I have not found
    // a way to add Damaris variables with multiple blocks to the vtkMultiBlockDataSet. 
    // See polyhedral_2blocks.cpp for example
    auto meshItr = MeshManager::Begin();
    for(; meshItr != MeshManager::End(); meshItr++) {
        std::shared_ptr<Mesh> mesh = *meshItr;
        
        auto varItr = VariableManager::Begin();
        for(; varItr != VariableManager::End(); varItr++) {

            std::shared_ptr<Variable> var = *varItr;
            const model::Variable& mdl = var->GetModel();

            if (mdl.mesh() == "#") continue;    // a variable with empty mesh
            if (mdl.mesh() != mesh->GetName()) continue;  // a variable with a different mesh
            if (mdl.visualizable() == false) continue;  // non-visualizable variable
            
            // I'm not sure if this is a valid calculation, as it reurns 1, even if
            // the variable has not been written to.
            // I think GetBlocksByIteration (used below) may perform what I want, somewhat better.
            int lowest, biggest ;
            var->GetIDRange(lowest, biggest) ;
            int numblocks = biggest - lowest  + 1 ;
            
            variable_block_map.insert(std::pair<std::string, int>(var->GetName(), numblocks));
        }
        
    }
    
    BlocksByIteration::iterator select_begin;
    BlocksByIteration::iterator select_end;
    

    rootGrid->SetNumberOfBlocks(meshCount);
    int index = 0;
    meshItr = MeshManager::Begin();
    for(; meshItr != MeshManager::End(); meshItr++) {
        std::shared_ptr<Mesh> mesh = *meshItr;
        
        // vtkMultiPieceDataSet* vtkMPGrid = vtkMultiPieceDataSet::New();
        vtkNew<vtkMultiPieceDataSet> vtkMPGrid ;
        rootGrid->SetBlock(index , vtkMPGrid);
        index++ ;
        
        auto varItr = VariableManager::Begin();
        for(; varItr != VariableManager::End(); varItr++) {

            std::shared_ptr<Variable> var = *varItr;
            const model::Variable& mdl = var->GetModel();

            if (mdl.mesh() == "#") continue;    // a variable with empty mesh
            if (mdl.mesh() != mesh->GetName()) continue;  // a variable with a different mesh
            if (mdl.visualizable() == false) continue;  // non-visualizable variable
            if (variable_block_map.find(var->GetName())->second > 1 ) {
                std::string logstring = "ParaViewAdaptor::FillMultiBlockGrid() not able to add Variable with multiple blocks:  " ;
                logstring += var->GetName() ;
                Environment::Log(logstring, EventLogger::Info);
                continue ;  // do not try to add multi-block variables
            }
            
            int this_iteration = iteration ;
            if (not var->IsTimeVarying())
                this_iteration = 0;
            
            var->GetBlocksByIteration(this_iteration, select_begin, select_end);
            const int num_blocks_dist = std::distance(select_begin, select_end) ; 
            if (num_blocks_dist == 0) {
                std::string logstring = "ParaViewAdaptor::FillMultiBlockGrid() not adding a variable with no block data - damaris_write() was not called for the variable? :  " ;
                logstring += var->GetName() ;
                // std::cout <<  logstring << std::endl ;
                Environment::Log(logstring, EventLogger::Info);
                continue ;  // do not try to add 0 block variables
            } else if (not (*varItr)->AddBlocksToVtkGrid(vtkMPGrid , iteration, 0)) {
                ERROR("Error in adding blocks to variable " << var->GetName() 
                          << ". Please remove mesh reference from the variable in the Damaris XML file. Mesh: "
                          << mesh->GetName() );
                return false;
            }
        }
    }
    
   // Print VTK vtm file of vtkMultiBlockDataSet data structure if requested to do so in Damaris XML file 
   if ( this->OutputVTKFIle() != 0) {
        if (iteration % this->OutputVTKFIle() == 0) {
            std::string filename = Environment::GetSimulationName();
            filename += "_iter_" ;
            filename += std::to_string(iteration) ;
            WriteVTKMBGridToFile(rootGrid, filename, OutputVTKFileBinary()) ;
        }
    }
    
    return true;
} 

/*
// This was an attept to write multi-block Damaris variables to the VTK vtkMultiBlockDataSet. 
// See polyhedral_2locks.cpp for example
bool ParaViewAdaptor::FillMultiBlockGrid(int iteration , vtkMultiBlockDataSet* rootGrid)
{
    // int index = 0;
    int serverCount = Environment::CountTotalServers();
    int meshCount = MeshManager::GetNumObjects();
    int serverRank = Environment::GetEntityProcessID();
    
    std::map<std::string, int> variable_block_map ;
    std::map<std::string, int> mesh_numvars_map ;
    
    
    int max_numblocks ;
    int vars_in_mesh = 0 ;
    int total_blocks = 0 ;
    auto meshItr = MeshManager::Begin();
    for(; meshItr != MeshManager::End(); meshItr++) {
        std::shared_ptr<Mesh> mesh = *meshItr;
        
        auto varItr = VariableManager::Begin();
        for(; varItr != VariableManager::End(); varItr++) {

            std::shared_ptr<Variable> var = *varItr;
            const model::Variable& mdl = var->GetModel();

            if (mdl.mesh() == "#") continue;    // a variable with empty mesh
            if (mdl.mesh() != mesh->GetName()) continue;  // a variable with a different mesh
            if (mdl.visualizable() == false) continue;  // non-visualizable variable
            
            int lowest, biggest ;
            var->GetIDRange(lowest, biggest) ;
            int numblocks = biggest - lowest  + 1 ;
            
            vars_in_mesh++ ;
            
            variable_block_map.insert(std::pair<std::string, int>(var->GetName(), numblocks));
            max_numblocks = std::max (total_blocks, numblocks) ;
            total_blocks += numblocks ;
        }
        
        mesh_numvars_map.insert(std::pair<std::string, int>(mesh->GetName(), vars_in_mesh));
    }

    rootGrid->SetNumberOfBlocks(serverCount * max_numblocks);
    for (int i = 0 ; i < (serverCount * max_numblocks) ;i++) {
        rootGrid->SetBlock(i , nullptr);
    }

   
    meshItr = MeshManager::Begin();
    for(; meshItr != MeshManager::End(); meshItr++) {
        std::shared_ptr<Mesh> mesh = *meshItr;
        int index = 0;
        
        // vtkMultiPieceDataSet* vtkMPGrid = vtkMultiPieceDataSet::New();
        // vtkNew<vtkMultiPieceDataSet> vtkMPGrid ;
        std::vector<vtkNew<vtkMultiPieceDataSet> >  vect_vtkMPGrid(max_numblocks) ;
        
        //for (int i = (serverRank*max_numblocks); i < (serverRank*max_numblocks)+( max_numblocks) ;i++) {
        //    rootGrid->SetBlock(i , nullptr);
        //    rootGrid->SetBlock(index , vect_vtkMPGrid[block]);
        //}
        int j = 0 ;
        for (int i = (serverRank*max_numblocks); i < (serverRank*max_numblocks)+( max_numblocks) ;i++) {
            rootGrid->SetBlock(i , vect_vtkMPGrid[j++]);
        }
        
        auto varItr = VariableManager::Begin();
        for(; varItr != VariableManager::End(); varItr++) {

            std::shared_ptr<Variable> var = *varItr;
            const model::Variable& mdl = var->GetModel();

            if (mdl.mesh() == "#") continue;    // a variable with empty mesh
            if (mdl.mesh() != mesh->GetName()) continue;  // a variable with a different mesh
            if (mdl.visualizable() == false) continue;  // non-visualizable variable
            

            int numblocks = variable_block_map.find(var->GetName())->second ;
            for (int block=0 ; block < numblocks ; block++ )
            {
                if (not (*varItr)->AddBlocksToVtkGrid(vect_vtkMPGrid[block] , iteration, block)) {
                    ERROR("Error in adding blocks to variable " << var->GetName());
                    return false;
                }
            }
            
            //rootGrid->SetBlock(index , vtkMPGrid);
            //index++;
        }
    }
    
   if ( this->OutputVTKFIle() != 0) {
        if (iteration % this->OutputVTKFIle() == 0) {
            std::string filename = Environment::GetSimulationName();
            filename += "_iter_" ;
            filename += std::to_string(iteration) ;
            WriteVTKMBGridToFile(rootGrid, filename, OutputVTKFileBinary()) ;
        }
    }
    

    return true;
} */

} // end of namespace damaris
