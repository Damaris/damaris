/**************************************************************************
This file is part of Damaris.

Damaris is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Damaris is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Damaris.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/
#include "Damaris.h"
#include "env/Environment.hpp"
#include "data/ParameterManager.hpp"
#include "data/Type.hpp"
#include "action/BoundAction.hpp"
#include "action/ActionManager.hpp"

using namespace damaris;


extern "C" {



const char * damaris_error_string(int ERROR_NUMBER) 
{
    const char * retstr ;
    switch (ERROR_NUMBER)
    {
        case DAMARIS_OK:
          retstr = "DAMARIS_OK" ;
          break ;
        case DAMARIS_INIT_ERROR:
          retstr = "DAMARIS_INIT_ERROR" ;
          break ;
        case DAMARIS_FINALIZE_ERROR:
          retstr = "DAMARIS_FINALIZE_ERROR" ;
          break ;
        case DAMARIS_CORE_IS_SERVER:
          retstr = "DAMARIS_CORE_IS_SERVER" ;
          break ;
        case DAMARIS_NO_SERVER:
          retstr = "DAMARIS_NO_SERVER" ;
          break ;
        case DAMARIS_ALLOCATION_ERROR:
          retstr = "DAMARIS_ALLOCATION_ERROR" ;
          break ;
        case DAMARIS_CONFIG_ERROR:
          retstr = "DAMARIS_CONFIG_ERROR" ;
          break ;
        case DAMARIS_ARGUMENT_ERROR:
          retstr = "DAMARIS_ARGUMENT_ERROR" ;
          break ;
        case DAMARIS_MPI_ERROR:
          retstr = "DAMARIS_MPI_ERROR" ;
          break ;
        case DAMARIS_DATASPACE_ERROR:
          retstr = "DAMARIS_DATASPACE_ERROR" ;
          break ;
        case DAMARIS_CHANNEL_ERROR:
          retstr = "DAMARIS_CHANNEL_ERROR" ;
          break ;
        case DAMARIS_INVALID_TAG:
          retstr = "DAMARIS_INVALID_TAG" ;
          break ;
        case DAMARIS_NOT_INITIALIZED:
          retstr = "DAMARIS_NOT_INITIALIZED" ;
          break ;
        case DAMARIS_ALREADY_INITIALIZED:
          retstr = "DAMARIS_ALREADY_INITIALIZED" ;
          break ;  
        case DAMARIS_UNDEFINED_ACTION:
          retstr = "DAMARIS_UNDEFINED_ACTION" ;
          break ;
        case DAMARIS_UNDEFINED_PARAMETER:
          retstr = "DAMARIS_UNDEFINED_PARAMETER" ;
          break ;
        case DAMARIS_UNDEFINED_VARIABLE:
          retstr = "DAMARIS_UNDEFINED_VARIABLE" ;
          break ;
        case DAMARIS_INVALID_BLOCK:
          retstr = "DAMARIS_INVALID_BLOCK" ;
          break ;
        case DAMARIS_INVALID_DIMENSIONS:
          retstr = "DAMARIS_INVALID_DIMENSIONS" ;
          break ;
        case DAMARIS_BLOCK_NOT_FOUND:
          retstr = "DAMARIS_BLOCK_NOT_FOUND" ;
          break ;
        case DAMARIS_REACTOR_NOT_FOUND:
          retstr = "DAMARIS_REACTOR_NOT_FOUND" ;
          break ;
        case DAMARIS_BIND_ERROR:
          retstr = "DAMARIS_BIND_ERROR" ;
          break ;
        case DAMARIS_ERROR_UNKNOWN:
          retstr = "DAMARIS_ERROR_UNKNOWN" ;
          break ;
        default:
          retstr = "Unknown Damaris Error Code" ;
          break ;
    }
    return retstr ;    
    
}


int damaris_initialize(const char* configfile, MPI_Comm comm)
{
    if(std::string(configfile).find("</simulation>") != std::string::npos)
    {
        Environment::Log("damaris_initialize() started: xmlConfigObject.length=" + std::string(configfile).length() , EventLogger::Info);
    }
    else
    {
        Environment::Log("damaris_initialize() started: " + std::string(configfile) , EventLogger::Info);
    }
    Environment::FlushLog() ;

    if(Environment::Initialized()) {
        return DAMARIS_ALREADY_INITIALIZED;
    }

    if(Environment::Init(std::string(configfile),comm)) {

        Environment::Log(Environment::GetEnvString() , EventLogger::Info);
        if (Environment::GetModel()->log().present()) {
                if (Environment::GetModel()->log().get().Flush()) {
                Environment::FlushLog();
            }
        }

        return DAMARIS_OK;
    } else {
        return DAMARIS_INIT_ERROR;
    }
}

int damaris_finalize()
{
    Environment::Log("damaris_finalize() started." , EventLogger::Info);
    Environment::FlushLog() ;
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    if(Environment::Finalize()) {
        return DAMARIS_OK;
    } else {
        return DAMARIS_FINALIZE_ERROR;
    }
}

int damaris_start(int* is_client)
{
    Environment::Log("damaris_start method started." , EventLogger::Info);
    Environment::FlushLog() ;

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    MPI_Barrier(Environment::GetGlobalComm());
    
    if(Environment::HasServer()) {
        if(Environment::IsServer()) {
            *is_client = 0;
            Environment::GetServer()->Run();
        } else {
            *is_client = 1;
        }
    } else {
        *is_client = 1;
        return DAMARIS_NO_SERVER;
    }
    return DAMARIS_OK;
}

int damaris_stop()
{
    Environment::Log("damaris_stop method started." , EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    if(Environment::HasServer() && Environment::IsClient()) {
        int err = Environment::GetClient()->StopServer();
        return err;
    } else {
        return DAMARIS_NO_SERVER;
    }
}

int damaris_write(const char* varname, const void* data)
{
    Environment::Log("damaris_write method started. " + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    return damaris_write_block(varname,0,data);
}

int damaris_write_block(const char* varname, int32_t block, const void* data)
{
    Environment::Log("damaris_write_block method started. "  + std::string(varname), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    if(not Environment::IsClient()) {
        return DAMARIS_CORE_IS_SERVER;
    }
    
    int resval = Environment::GetClient()->Write(varname,block,data);
    Environment::Log("damaris_write_block returning:  "   + std::string(varname) + "  "  + std::string(damaris_error_string(resval)), EventLogger::Error);
    
    return resval ;
}


int damaris_get_type(const char* varname, DAMARIS_TYPE_STR *vartype) 
{
    Environment::Log("damaris_get_type method called. " + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    return Environment::GetClient()->ReturnVarTypeString(varname,vartype) ;
}

// This function should be added to as other plugins are added to Damaris
int damaris_has_plugin(DAMARIS_PLUGIN_TYPE plugin)
{
    int ret ;
    
    switch (plugin) {
        case DAMARIS_HAS_HDF5: 
#ifdef HAVE_HDF5_ENABLED
            ret = 1 ;
#else
            ret = 0 ;
#endif
        break;
        case DAMARIS_HAS_VISIT:
#ifdef HAVE_VISIT_ENABLED
            ret = 1 ;
#else
            ret = 0 ;
#endif
        break;
        case DAMARIS_HAS_PARAVIEW:
#ifdef HAVE_PARAVIEW_ENABLED
            ret = 1 ;
#else
            ret = 0 ;
#endif
        break;
        case DAMARIS_HAS_PYTHON:
#ifdef HAVE_PYTHON_ENABLED
            ret = 1 ;
#else
            ret = 0 ;
#endif
        break;
        default:
           ret = 0 ;
        break;
    }
    return ret ;
}

int damaris_alloc(const char* varname, void** ptr)
{
    Environment::Log("damaris_alloc method started. " + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    return damaris_alloc_block(varname,0,ptr);
}

int damaris_alloc_block(const char* varname, int32_t block, void** ptr)
{
    Environment::Log("damaris_alloc_block method started. "  + std::string(varname), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    return Environment::GetClient()->Alloc(varname,block,ptr);
}

int damaris_commit(const char* varname)
{
    Environment::Log("damaris_commit method started. " + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    int iteration = Environment::GetLastIteration();
    return damaris_commit_block_iteration(varname,0,iteration);
}

int damaris_commit_block(const char* varname, int32_t block)
{
    Environment::Log("damaris_commit_block method started. "  + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    int iteration = Environment::GetLastIteration();
    return damaris_commit_block_iteration(varname,block,iteration);
}

int damaris_commit_iteration(const char* varname, int32_t iteration)
{
    Environment::Log("damaris_commit_iteration method started. " + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    return damaris_commit_block_iteration(varname,0,iteration);
}

int damaris_commit_block_iteration(const char* varname, 
    int32_t block, int32_t iteration)
{
    Environment::Log("damaris_commit_block method II started. "  + std::string(varname),  EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    return Environment::GetClient()->Commit(varname,block,iteration);
}

int damaris_clear(const char* varname)
{
    Environment::Log("damaris_clear method started. " + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    int iteration = Environment::GetLastIteration();
    return damaris_clear_block_iteration(varname,0,iteration);
}

int damaris_clear_block(const char* varname, int32_t block)
{
    Environment::Log("damaris_clear_block method started. " + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    int iteration = Environment::GetLastIteration();
    return damaris_clear_block_iteration(varname,block,iteration);
}

int damaris_clear_iteration(const char* varname, int32_t iteration)
{
    Environment::Log("damaris_clear_iteartion method II started. "  + std::string(varname), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    return damaris_clear_block_iteration(varname,0,iteration);
}

int damaris_clear_block_iteration(const char* varname, int32_t block, 
    int32_t iteration)
{
    Environment::Log("damaris_clear_block_iteration method II started. "  + std::string(varname), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    return Environment::GetClient()->Clear(varname,block,iteration);
}


int damaris_signal(const char* signal_name)
{
    Environment::Log("damaris_signal method started. " + std::string(signal_name), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    return Environment::GetClient()->Signal(std::string(signal_name));
}

int damaris_bind(const char* signal_name, signal_t sig)
{
    Environment::Log("damaris_bind method started. " + std::string(signal_name), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    // TODO ensure this is called by ALL the processes
    std::shared_ptr<Action> a = BoundAction::New<Action>(sig,signal_name);
    if(not a) {
        Environment::Log("damaris_bind DAMARIS_BIND_ERROR.", EventLogger::Error);
        return DAMARIS_BIND_ERROR;
    }
    
    ActionManager::Add(a);
    return DAMARIS_OK;
}


int damaris_parameter_get(const char* name, 
    void* buffer, unsigned int size)
{
    Environment::Log("damaris_parameter_get method started." + std::string(name), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    std::shared_ptr<Parameter> p = ParameterManager::Search(name);
    if(p) return p->ToBuffer(buffer, size);
    else return DAMARIS_UNDEFINED_PARAMETER;
}

int damaris_parameter_set(const char* name, 
    const void* buffer, unsigned int size)
{
    Environment::Log("damaris_parameter_set method started. " + std::string(name), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    std::shared_ptr<Parameter> p = ParameterManager::Search(name);
    if(p) {
        return p->FromBuffer(buffer, size);
    } else {
        Environment::Log("damaris_parameter_set DAMARIS_UNDEFINED_PARAMETER.", EventLogger::Error);
        return DAMARIS_UNDEFINED_PARAMETER;
    }
}

int damaris_set_position(const char* var_name, const int64_t* position)
{
    Environment::Log("damaris_set_position method started. " + std::string(var_name), EventLogger::Info);
    
    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    
    return damaris_set_block_position(var_name,0,position);
}

int damaris_set_block_position(const char* var_name, int32_t block,
    const int64_t* position)
{
    Environment::Log("damaris_set_block_position method started." + std::string(var_name), EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    return Environment::GetClient()->SetPosition(var_name,block,position);
}

int damaris_client_comm_get(MPI_Comm* comm)
{
    Environment::Log("damaris_client_comm_get method started.", EventLogger::Info);

    if(Environment::Initialized()) {
        *comm = Environment::GetEntityComm();
        return DAMARIS_OK;
    } else {
        Environment::Log("damaris_client_comm_get DAMARIS_NOT_INITIALIZED.", EventLogger::Error);
        return DAMARIS_NOT_INITIALIZED;
    }
}

int damaris_end_iteration(  )
{
    Environment::Log("damaris_end_iteration method started.", EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    Environment::GetClient()->EndIteration();
    
    return DAMARIS_OK;
}

int damaris_get_iteration(int* iteration)
{
    Environment::Log("damaris_get_iteration method started.", EventLogger::Info);

    if(not Environment::Initialized()) {
        return DAMARIS_NOT_INITIALIZED;
    }
    *iteration = Environment::GetLastIteration();
    return DAMARIS_OK;
}

}
