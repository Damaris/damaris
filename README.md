## Damaris


[![pipeline status](https://gitlab.inria.fr/Damaris/damaris/badges/master/pipeline.svg)](https://gitlab.inria.fr/Damaris/damaris/-/commits/master)

This is the public source code repositiory for the Damaris library.

## Description
Damaris is a library for data managmement and visualisation that provides asynchronous, in situ processing capabilities to MPI based simulation codes.

## Documentation
The **official website and documentation** for Damaris is available here:  
  
[project.inria.fr/damaris](https://project.inria.fr/damaris/documentation/)

The current **dOxygen documentation** for Damaris source code can be found here:  
  
  [https://damaris.gitlabpages.inria.fr/damaris/master/html](https://damaris.gitlabpages.inria.fr/damaris/master/html/_damaris_8h.html)