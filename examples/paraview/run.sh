#!/bin/sh
export LD_LIBRARY_PATH=$HOME/local/lib:$LD_LIBRARY_PATH 
export PATH=$HOME/local/bin:$PATH


# Run an example and open the Paraview GUI and select Catalyst + Connect from the main menu.
# The port number must match what is in the catalyst python file (image.py etc.) as well as
# the FQDN of the system that the GUI is running on (or use localhost if GUI and simulation
# are running on the same machine)
# Ensure the path to the catalyst python script is correct in the relevant xml file.

#mpirun -np 4 --oversubscribe ./image "image.xml"

mpirun -np 4 --oversubscribe ./sector "sector.xml"

#mpirun -np 8 ./plate "plate.xml"

#mpirun -np 4 ./lollipop "lollipop.xml"
