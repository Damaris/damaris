/**************************************************************************
This file is part of Damaris.

Damaris is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Damaris is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Damaris.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDataSetMapper.h>
#include <vtkIdList.h>
#include <vtkNew.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyhedron.h>
#include <vtkProperty.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkDoubleArray.h>
#include <vtkCellArrayIterator.h>
#include <vtkTetra.h>

#include <vtkMultiBlockDataSet.h>
#include <vtkMultiPieceDataSet.h>
#include <vtkUnsignedCharArray.h>

#include <vtkMPI.h>
#include <vtkMPICommunicator.h>
#include <vtkMPIController.h>
#include <vtkXMLPMultiBlockDataWriter.h>

#ifdef USE_VTK_RENDERING_DEF
// Used when cmake variable USE_VTK_RENDERING=ON
#include <vtkNamedColors.h>
#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#endif

#define USE_DAMARIS 1

#ifdef USE_DAMARIS
    #include <Damaris.h>
#endif

#include <mpi.h>

#include <numeric>
#include <vector>

#include<unistd.h>

/**
To install VTK (9.x) on Ubuntu/Debian
sudo apt-get install qtcreator qt5-default 
For onscreen rendering: sudo apt-get install libglvnd-dev
For offscreen rendering use:  -DVTK_OPENGL_HAS_OSMESA=ON -DVTK_USE_X=OFF
cmake .. -DVTK_WRAP_PYTHON=ON -DVTK_PYTHON_VERSION=3 -DUSE_VTK_RENDERING=OFF -DDamaris_ROOT=/home/jbowden/local -DCMAKE_INSTALL_PREFIX=/home/jbowden/local -DVTK_DIR=<PATH/TO/VTK>/lib/cmake/vtk-9.2

# If using independent VTK
# -DVTK_DIR=/home/$USER/localvtk/lib/cmake/vtk-9.2
# If using VTK wrapped with Paraview:
# -DVTK_DIR=/home/jbowden/local/lib/cmake/paraview-5.10/vtk

Output of program are VTK VTU XML files, one for each Damaris client rank on each iteration.

Requires polyhedral.xml config in same directory as executable :
>cat polyhedral.xml
<?xml version="1.0"?>
<simulation name="poly" language="c" xmlns="http://damaris.gforge.inria.fr/damaris/model">
    <architecture>
        <domains count="2" />
        <dedicated cores="1" nodes="0" />
        <placement />        
        <buffer name="damaris-buffer" size="536870912" />
        <queue  name="damaris-queue" size="100" />
    </architecture>
    <data>
        <parameter name="size"     type="int" value="4" 
             comment="This will be set to the number of Damaris cients, so depends on mpirun -nm X value" />
        <parameter name="PRESSURE_param_0"   type="int" value="1" 
             comment="This will be set to the number cells per rank, on a particular iteration, each cell will have a pressure value"  />
        <layout   name="cells"    type="double" dimensions="PRESSURE_param_0 " />
        <variable name="PRESSURE" type="scalar" layout="cells" mesh="mesh" centering="nodal" store="MyStore" />
        
        
        <parameter name="n_elements_local"       type="int" value="1" comment="This is the total number of cells being described on the current MPI process (local process)"/> 
        <parameter name="n_vertices_local"       type="int" value="1" />
        <parameter name="n_connectivity_local"   type="int" value="1" />
        <parameter name="n_polycellconnections_local"   type="int" value="1" />
        <!--parameter name="n_polyfacespercell_local" type="int" value="1" -->
        <parameter name="n_polycellfaces_local"    type="int" value="1" />


        <layout name="umesh_points_xyz_layout"   type="double"  dimensions="n_vertices_local,3"         />
        <layout name="umesh_connectivity_layout" type="long"    dimensions="n_connectivity_local"  
              comment="This is the connectivity array, which are references into the vertex array used to specify edges between the vertices"   />
        <layout name="umesh_n_elements_char"     type="char"    dimensions="n_elements_local"            />
        <layout name="umesh_n_elements_long"     type="long"    dimensions="n_elements_local"            />
        <layout name="umesh_n_elements_p1_long"  type="long"    dimensions="n_elements_local + 1"        />
        <layout name="cell_faces_connectivity"   type="long"    dimensions="n_polycellconnections_local" />
        <!-- layout name="cell_faces_size"       type="long"    dimensions="n_polycellfaces_local"   comment="usefull for Mesh Blueprint but 
                                                                                                                can be formed from the offsets array -->
        <layout name="cell_faces_offsets_size"   type="long"    dimensions="n_polycellfaces_local + 1" 
              comment="One offset value for each element specifying -1 for non-polyhedral types"         />

        <group name="umesh_vars">    
            <variable name="umesh_points_xyz"    layout="umesh_points_xyz_layout"    type="scalar" visualizable="false" time-varying="true" store="MyStore" comment="The x,y,z coordinates of each vertex "  />
            <variable name="umesh_connectivity"  layout="umesh_connectivity_layout"  type="scalar" visualizable="false" time-varying="true" store="MyStore" comment="The id's into the umesh_points_xyz array for a cells vertex" />
            <variable name="umesh_types"         layout="umesh_n_elements_char"      type="scalar" visualizable="false" time-varying="true" store="MyStore" comment="The section geometry type - VTK_QUAD VTK_HEXAHEDRON etc." />
            <variable name="umesh_offsets"       layout="umesh_n_elements_p1_long"   type="scalar" visualizable="false" time-varying="true" store="MyStore"   
              comment="The offset into umesh_connectivity array to find the connectivities of non-polygonal/polyhedral elements, starts at 0 and has an extra value at end that is the total size of umesh_connectivity array" />
            <variable name="polyhedral_cell_faces_connectivity" layout="cell_faces_connectivity"  type="scalar" visualizable="false" time-varying="true" store="MyStore" comment="The connectivities of the faces that make up a polhedral cell" />
            <variable name="polyhedral_cell_faces_offsets"      layout="cell_faces_offsets_size"  type="scalar" visualizable="false" time-varying="true" store="MyStore" comment="The offsets into polyhedral_cell_faces array to find the face that makes up a polyhedra" />
            <variable name="polyhedral_n_faces_per_cell"        layout="umesh_n_elements_long"    type="scalar" visualizable="false" time-varying="true" store="MyStore" comment="Stores the number of cell faces for a polyhedral cell or -1 if not polyhedral or polygonal" />
            
            <mesh name="polyhedral_umesh" type="unstructured" topology="3" >
               <coord                 name="umesh_vars/umesh_points_xyz"         unit="m"     />
               <vertex_global_id      name="umesh_vars/umesh_connectivity"       offset="0"   />
               <section_types         name="umesh_vars/umesh_types"                           />
               <section_sizes         name="used_for_code_saturne_only"                      />
               <element_offsets       name="umesh_vars/umesh_offsets"                         />
               <polyhedral_cell_faces_connectivity name="umesh_vars/polyhedral_cell_faces_connectivity" /> 
               <polyhedral_cell_faces_offsets name="umesh_vars/polyhedral_cell_faces_offsets" />
               <polyhedral_n_faces_per_cell   name="umesh_vars/polyhedral_n_faces_per_cell"   />
             </mesh>
        </group>
        
    </data>
    <storage>
        <store name="MyStore" type="HDF5">
            <option key="FileMode">FilePerCore</option>
            <option key="XDMFMode">NoIteration</option>
            <option key="FilesPath"></option>
        </store>
    </storage>
    <actions>
    </actions>
    <log FileName="log/poly_" RotationSize="5" LogFormat="[%TimeStamp%]: %Message%" LogLevel="info" Flush="false" />
</simulation>
*/
#define MAX_ITER 15

  double pa[8][3] ={ {0.0, 0.0, 0.0},
                     {1.0, 0.0, 0.0},
                     {1.0, 1.0, 0.0},
                     {0.0, 1.0, 0.0},
                     {0.0, 0.0, 1.0},
                     {1.0, 0.0, 1.0},
                     {1.0, 1.0, 1.0},
                     {0.0, 1.0, 1.0}
                  } ;
                            
  vtkIdType fa[6][4] = { {0, 3, 2, 1},
                         {0, 4, 7, 3},
                         {4, 5, 6, 7},
                         {5, 1, 2, 6},
                         {0, 1, 5, 4},
                         {2, 3, 7, 6},
                       } ;



// Returns the number of points added
vtkIdType  AddAnotherPolyhedralCube(vtkPoints* points, vtkIdList* faces,  
                                      vtkIdList* face_connections_total, 
                                      std::vector<vtkIdType>& face_offsets_v, 
                                      std::vector<vtkIdType>& face_n_cell_faces_v,
                                      vtkIdType n_vertices_local,  
                                      double offsetX,  double offsetZ, int rank) 
{
  // Add the X and X coordinate offsets in-place 
  // X offsets correspond to the rank and Z offset to the iteration
  points->InsertNextPoint(pa[0][0]+offsetX, pa[0][1], pa[0][2]+offsetZ);
  points->InsertNextPoint(pa[1][0]+offsetX, pa[1][1], pa[1][2]+offsetZ);
  points->InsertNextPoint(pa[2][0]+offsetX, pa[2][1], pa[2][2]+offsetZ);
  points->InsertNextPoint(pa[3][0]+offsetX, pa[3][1], pa[3][2]+offsetZ);
  points->InsertNextPoint(pa[4][0]+offsetX, pa[4][1], pa[4][2]+offsetZ);
  points->InsertNextPoint(pa[5][0]+offsetX, pa[5][1], pa[5][2]+offsetZ);
  points->InsertNextPoint(pa[6][0]+offsetX, pa[6][1], pa[6][2]+offsetZ);
  points->InsertNextPoint(pa[7][0]+offsetX, pa[7][1], pa[7][2]+offsetZ);

 // std::cout << "INFO: rank: " << rank << "  Faces : " << std::endl ;
   // add the faces 
   for (int fid = 0 ; fid < 6; fid++){
      faces->InsertNextId(4) ;
      face_offsets_v.push_back(face_offsets_v[face_offsets_v.size()-1]+4) ;  // face_offsets_v must have first element == 0 added
     // std::cout << "4  " ;
      for (int vid = 0 ; vid < 4 ; vid++ ) {
          vtkIdType offsetFaceRef = fa[fid][vid] + n_vertices_local  ;
      //    std::cout << offsetFaceRef << " ";
          faces->InsertNextId(offsetFaceRef); 
          face_connections_total->InsertNextId(offsetFaceRef);
      }
     // std::cout << std::endl ;
  }
  face_n_cell_faces_v.push_back(6) ;  // There are 6 faces in this Polyhedron cell
  
  // std::cout << std::endl ;
  return 8 ;
}


// Returns the number of points added
vtkIdType  AddSomePoints(vtkPoints* points,  double offsetX,  double offsetZ, int rank) 
{
  // Add the X and X coordinate offsets in-place 
  // X offsets correspond to the rank and Z offset to the iteration
  points->InsertNextPoint(pa[0][0]+offsetX, pa[0][1], pa[0][2]+offsetZ);
  points->InsertNextPoint(pa[1][0]+offsetX, pa[1][1], pa[1][2]+offsetZ);
  points->InsertNextPoint(pa[2][0]+offsetX, pa[2][1], pa[2][2]+offsetZ);
  points->InsertNextPoint(pa[3][0]+offsetX, pa[3][1], pa[3][2]+offsetZ);
  points->InsertNextPoint(pa[4][0]+offsetX, pa[4][1], pa[4][2]+offsetZ);
  points->InsertNextPoint(pa[5][0]+offsetX, pa[5][1], pa[5][2]+offsetZ);
  points->InsertNextPoint(pa[6][0]+offsetX, pa[6][1], pa[6][2]+offsetZ);
  points->InsertNextPoint(pa[7][0]+offsetX, pa[7][1], pa[7][2]+offsetZ);

  return 8 ;
}


void WriteVTKMBGridToFile(vtkMultiBlockDataSet* vtkMBDSGrid, std::string filename, int rank )
{
    // vtkNew<vtkXMLMultiBlockDataWriter> writer;
    
    vtkNew<vtkXMLPMultiBlockDataWriter> writer;
    
    filename += "." ;
    filename += writer->GetDefaultFileExtension();
    writer->SetFileName(filename.c_str());
    writer->SetDataModeToAscii();
    writer->SetInputData(vtkMBDSGrid);
    //int numpieces = endpiece - startpiece + 1 ;
    //writer->SetNumberOfBlocks(numblocks);  // this is the 'virtual' size of the blocked dataset, 1 block for each MultiPiece dataset

    if (rank == 0)
    {
        writer->SetWriteMetaFile(1);
    }
    writer->Update();
}

void print_usage(char* exename) {
    fprintf(stderr,"Usage: mpirun -np X %s [-p filename]  [-h]\n",exename);
    fprintf(stderr,"     Requires a file name: polyhedral.xml for Damaris configuration");
    fprintf(stderr,"    -p    Print the vtkMultiBlockDataSet data to file on each iteration, using a base name 'filename' (default VTK_MBDATA_2B) \n");
    fprintf(stderr,"    X     Is the number of MPI ranks to launch \n");
    fprintf(stderr,"    -h    This help \n");
}


int main(int argc, char** argv)
{
#ifdef USE_VTK_RENDERING_DEF
    vtkNew<vtkNamedColors> colors;
#endif

    int current_arg = 0 ;
    std::string vtkfilename = "VTK_MBDATA";
    int printvtk = 0 ;
    int num_point_ids = 3 ;
    int is_client = 1;
    int client_size;
    int rank;
    

    MPI_Init(&argc,&argv) ;
    MPI_Comm_rank(MPI_COMM_WORLD , &rank);
    // Get command line args
    while (current_arg < argc ) 
    {
        if (strcmp(argv[current_arg],"-p") == 0) {
            current_arg++;
            printvtk =  1 ;
            if (current_arg < argc ) {
                if (strcmp(argv[current_arg],"-h") != 0) {
                    vtkfilename.assign(argv[current_arg]) ;
                } else {
                    if (rank == 0) print_usage(argv[0]) ;
                    exit(0);
                }
            }
        } else if (strcmp(argv[current_arg],"-h") == 0) {
            if (rank == 0) print_usage(argv[0]) ;
            exit(0);
        }
        current_arg++;
    }
    
#ifdef USE_DAMARIS
    damaris_initialize("polyhedral.xml", MPI_COMM_WORLD) ;
#endif

#ifdef USE_DAMARIS
    int err = damaris_start(&is_client);
    if(err != DAMARIS_OK) {
          std::cerr << " Damaris error: " <<  damaris_error_string(err) << std::endl ;
          exit(-1) ;
    }
#endif 
    

    if(is_client == 1) 
    {
        MPI_Comm comm;
#ifdef USE_DAMARIS
        damaris_client_comm_get(&comm);
#else
        comm = MPI_COMM_WORLD ;
#endif
        MPI_Comm_rank(comm , &rank);
        MPI_Comm_size(comm , &client_size);
        
        //if (rank == 0 ) std::cout << "INFO: The size of the VTK type vtkIdType is : " << sizeof(vtkIdType) << std::endl ;
#ifdef USE_DAMARIS
        err = damaris_parameter_set("size" , &client_size, sizeof(int)) ;
        if(err != DAMARIS_OK ){
           std::cerr << " Damaris error: " <<  damaris_error_string(err) << std::endl ;
        }
#endif
        vtkMPIController *vtkController = vtkMPIController::New();
        vtkMPICommunicator *vtkComm = vtkMPICommunicator::New();
        vtkMPICommunicatorOpaqueComm opaqueComm(&comm);
        vtkComm->InitializeExternal(&opaqueComm);
        vtkController->SetCommunicator(vtkComm);
        vtkMultiProcessController::SetGlobalController(vtkController);
    
        
        for (int iter = 0 ; iter < MAX_ITER ; iter++ ) {
            vtkNew<vtkMultiPieceDataSet> vtkMPGrid ;
            vtkNew<vtkMultiBlockDataSet> rootGrid;
            
            rootGrid->SetNumberOfBlocks(client_size);
             for (int i = 0 ; i < client_size ;i++) {
                rootGrid->SetBlock(i , nullptr);
             }
             
            vtkNew<vtkUnstructuredGrid> ugrid;
            
            int blockloops = 1 ;
             vtkMPGrid->Initialize();
             vtkMPGrid->SetNumberOfPieces(blockloops);
             
             
            int n_vertices_local = 0 ;     // The number of vertices
            int n_elements_local = 0 ;     // the number of cells in the mesh
            int n_connectivity_local = 0 ; // The number of references into the vertex array
            int n_polycellconnections_local = 0 ; // The number of references into the vertex array for the polygon faces of the polyhedrons
            int n_polycellfaces_local = 0 ;  // The number of polygon faces that make up the polyhedra. 
                                             // This (+1) is the size of the polyhedral_cell_faces_offsets array
            vtkNew<vtkPoints> points;
            
            double offsetX = (double)  rank ;
            std::vector<vtkIdType> pointIds_vect ;
            // cid_v will be an array of the connectivities (point ids) 
            // as it is not easy to extract the connectivity from the unstructured 
            // grid after it is created
            std::vector<vtkIdType> cid_v ;  
            std::vector<vtkIdType> cid_offsets_v ;
            std::vector<vtkIdType> face_offsets_v ;
            std::vector<vtkIdType> face_n_cell_faces_v ;
            vtkNew<vtkIdList> face_connections_total; 
            
            face_offsets_v.push_back(0) ;
            cid_offsets_v.push_back(0) ;
            for (int numInIt = 0 ; numInIt <  iter+1 ; numInIt++ ) {
                n_connectivity_local += 8 ;
                cid_offsets_v.push_back(n_connectivity_local) ;
                for (int point = 0 ; point <  8 ; point++ )
                    pointIds_vect.push_back(n_vertices_local+point) ;
                vtkNew<vtkIdList> faces; 
                double offsetZ = (double)  numInIt ;
                n_vertices_local += AddAnotherPolyhedralCube(points, faces, face_connections_total, face_offsets_v, face_n_cell_faces_v, n_vertices_local, offsetX, offsetZ, rank) ;
                ugrid->InsertNextCell(VTK_POLYHEDRON, 8, pointIds_vect.data(), 6, faces->GetPointer(0));
                cid_v.reserve(cid_v.size() + distance(pointIds_vect.begin(),pointIds_vect.end()));
                cid_v.insert(cid_v.end(),pointIds_vect.begin(),pointIds_vect.end());
                pointIds_vect.clear() ;
            }

            // These are the points to build the tetrahedrons from
            for (int point = 0 ; point <  8 ; point++ )
                    pointIds_vect.push_back(n_vertices_local+point) ;
            double offsetZ = (double)  (iter+1) ;
  
            AddSomePoints(points, offsetX, offsetZ, rank) ;
            ugrid->SetPoints(points);
            
            // Create 2 tetrahedrons from the new points.
            // vtkIdType ptIds0[] = {n_vertices_local+0, n_vertices_local+1, n_vertices_local+4, n_vertices_local+3};
            std::vector<vtkIdType> ptIds0 = {n_vertices_local+0, n_vertices_local+1, n_vertices_local+4, n_vertices_local+3};
            cid_v.reserve(cid_v.size() + distance(ptIds0.begin(),ptIds0.end()));
            cid_v.insert(cid_v.end(),ptIds0.begin(),ptIds0.end());
            n_connectivity_local += 4 ;
            cid_offsets_v.push_back(n_connectivity_local) ;
            ugrid->InsertNextCell(VTK_TETRA, 4, ptIds0.data());
            face_n_cell_faces_v.push_back(-1) ;  // when adding non-polygonal or polyhedral cells, then place a -1 for the number of polygon faces
           
            // vtkIdType ptIds1[] = {n_vertices_local+1, n_vertices_local+6, n_vertices_local+4, n_vertices_local+5};
            std::vector<vtkIdType> ptIds1 = {n_vertices_local+1, n_vertices_local+6, n_vertices_local+4, n_vertices_local+5} ;
            cid_v.reserve(cid_v.size() + distance(ptIds1.begin(),ptIds1.end()));
            cid_v.insert(cid_v.end(),ptIds1.begin(),ptIds1.end());
            n_connectivity_local += 4 ;
            cid_offsets_v.push_back(n_connectivity_local) ;
            ugrid->InsertNextCell(VTK_TETRA, 4, ptIds1.data());
            face_n_cell_faces_v.push_back(-1) ;  // when adding non-polygonal or polyhedral cells, then place a -1 for the number of polygon faces
             
            pointIds_vect.clear() ;
            n_vertices_local += 8 ; // add the last 8 vertices that were added for the tetrahedra
            
            
            


            // Set the field value 
            int param_size = iter + 3 ; // +3 as we have iter+1 polyhedrals + 2 tetrahedrals
#ifdef USE_DAMARIS
            damaris_parameter_set("PRESSURE_param_0",&param_size, sizeof(param_size)) ;
#endif
            std::vector<double> PRESSURE ;
            PRESSURE.reserve(iter+3) ;  // +3 as we have iter+1 polyhedrals + 2 tetrahedrals
            vtkNew<vtkDoubleArray> PRESSURE_VTK ;
            PRESSURE_VTK->SetNumberOfComponents(1);
            PRESSURE_VTK->SetNumberOfTuples(iter + 1 + 2);
            PRESSURE_VTK->SetName("PRESSURE");
            
            // std::cout << "PRESSURE: "  ;
            double val = 1.0 ;
            for (int i = 0 ; i < iter + 1 + 2 ; i++) {
                PRESSURE.push_back(val) ;
                PRESSURE_VTK->InsertNextValue(val) ;
                // std::cout << (PRESSURE_VTK->GetTuple(i))[i] << "  " ;
                val += 1.0 ;
           }
           // std::cout << "PRESSURE: has standard memory layout (1 == true): " << PRESSURE_VTK->HasStandardMemoryLayout() << std::endl ;
#ifdef USE_DAMARIS
           damaris_write("fields/PRESSURE", PRESSURE.data()) ;
#endif
           ugrid->GetPointData()->AddArray(PRESSURE_VTK) ;

#ifdef USE_DAMARIS
            // umesh_points_xyz has a layout with 2 dimensions: n_vertices_local,3
            damaris_parameter_set("n_vertices_local",&n_vertices_local, sizeof(n_vertices_local)) ;

            /* 
            VTK may store the data in a prefered layout. 
            This forces the layout into a single SoA array 
            */
            vtkNew<vtkDoubleArray> pointdata;
            vtkIdType startRange = 0 ;
            vtkDataArray * da = points->GetData() ;
            da->GetData(startRange, n_vertices_local-1 , 0, 2, pointdata);
            damaris_write("umesh_vars/umesh_points_xyz", (void *) pointdata->GetVoidPointer(0)) ;

            // vtkUnsignedCharArray* types = ugrid->GetCellTypesArray() ;
            vtkUnsignedCharArray* types = ugrid->GetCellTypesArray() ;
            n_elements_local = (int) types->GetNumberOfValues() ;
            // std::cout << "INFO: n_elements_local : " << n_elements_local << std::endl ;
            damaris_parameter_set("n_elements_local",&n_elements_local, sizeof(n_elements_local)) ;
            damaris_write("umesh_vars/umesh_types" , types->GetVoidPointer(0)) ;
             
            
            // std::cout <<  std::endl << " Total: " << cid_v.size() <<  std::endl ;          
            assert(cid_v.size() == n_connectivity_local) ;
            damaris_parameter_set("n_connectivity_local",&n_connectivity_local, sizeof(n_connectivity_local)) ;
            damaris_write("umesh_vars/umesh_connectivity" , cid_v.data()) ;
            
            //for ( const auto& p : cid_offsets_v ) {
             //  std::cout << p <<  ",  " ;
            //} 
            // std::cout <<  std::endl << " Total: " << cid_offsets_v.size() <<  std::endl ;
            assert(cid_offsets_v[cid_offsets_v.size()-1] == cid_v.size()) ;
            damaris_write("umesh_vars/umesh_offsets" ,   cid_offsets_v.data()) ;
            
            // std::cout << " faces_total: " << face_connections_total->GetNumberOfIds() <<  std::endl ;
            n_polycellconnections_local = face_connections_total->GetNumberOfIds() ;
            damaris_parameter_set("n_polycellconnections_local",&n_polycellconnections_local, sizeof(n_polycellconnections_local)) ;
            damaris_write("umesh_vars/polyhedral_cell_faces_connectivity", face_connections_total->GetPointer(0));
            
            // std::cout << " faces offsets : " <<  std::endl ;
            //for ( const auto& p : face_offsets_v ) {
            //   std::cout << p <<  ",  " ;
            //} 
            // std::cout <<  std::endl << " Total: " << face_offsets_v.size() <<  std::endl ;  
            n_polycellfaces_local = face_offsets_v.size() - 1 ;
            
            damaris_parameter_set("n_polycellfaces_local",&n_polycellfaces_local, sizeof(n_polycellfaces_local)) ;
            damaris_write("umesh_vars/polyhedral_cell_faces_offsets", face_offsets_v.data());
            face_offsets_v.clear() ;
            
            damaris_write("umesh_vars/polyhedral_n_faces_per_cell", face_n_cell_faces_v.data()) ;
 
            std::cout << "INFO:  Iter: " << iter  << "  offsetX: " << offsetX << " n_elements_local : " << n_elements_local << std::endl << std::flush ;
            MPI_Barrier(comm) ;
            if (rank == 0) std::cout << "INFO: All ranks calling: damaris_end_iteration() : " << iter << std::endl ;
            damaris_end_iteration() ;
#endif

            vtkMPGrid->SetPiece(0  , ugrid);
            rootGrid->SetBlock(rank , vtkMPGrid);
            
            
            cid_v.clear() ;
            cid_offsets_v.clear() ;
            PRESSURE.clear() ;
            
            std::string filename = vtkfilename ;
            filename += "_iter_" ;
            filename += std::to_string(iter) ;
            int startpiece = rank  ;
            int numblocks = client_size ;
            if (printvtk > 0)  WriteVTKMBGridToFile(rootGrid, filename, rank) ;
            
            
            face_n_cell_faces_v.clear() ;
            cid_v.clear() ;
            cid_offsets_v.clear() ;
            PRESSURE.clear() ;
            // ugrid->Delete() ;
            // PRESSURE_VTK->Delete() ;
            
            sleep(5);
            
#ifdef USE_VTK_RENDERING_DEF
            if (client_size == 1) {
                // only render to screen when there is a single damaris client rank 
                vtkNew<vtkDataSetMapper> mapper;
                mapper->SetInputData(ugrid);

                vtkNew<vtkActor> actor;
                actor->SetMapper(mapper);
                actor->GetProperty()->SetColor(colors->GetColor3d("Silver").GetData());

                // Visualize
                vtkNew<vtkRenderer> renderer;
                vtkNew<vtkRenderWindow> renderWindow;
                renderWindow->SetWindowName("Polyhedron");
                renderWindow->AddRenderer(renderer);
                vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
                renderWindowInteractor->SetRenderWindow(renderWindow);

                renderer->AddActor(actor);
                renderer->SetBackground(colors->GetColor3d("Salmon").GetData());
                renderer->ResetCamera();
                renderer->GetActiveCamera()->Azimuth(30);
                renderer->GetActiveCamera()->Elevation(30);
                renderWindow->Render();
                renderWindowInteractor->Start();
            }
#endif
        }
#ifdef USE_DAMARIS
       std::cout << "INFO: Calling: damaris_stop() "<< std::endl ;
       damaris_stop( );
#endif
    }
#ifdef USE_DAMARIS
    err = damaris_finalize();
    if(err != DAMARIS_OK ){
       std::cerr << " Daamris error: " <<  damaris_error_string(err) << std::endl ;
    }
#endif

    MPI_Finalize();
  
  return EXIT_SUCCESS;
}
