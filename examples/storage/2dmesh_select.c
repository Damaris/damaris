#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <assert.h>

#include "Damaris.h"

#define MAX_CYCLES 3

/**
* Description: A test example for the HDF5Store class OutputCollectiveSelect().
*      This example exercises the HDF5 collective ouput that uses a seperate array
*      of indices to order the data on disk. A Damaris variable can be indicated as
*      using this method by providing a select="select_cells" attribute to the 
*      <variable ...> tag.
*      The indicated variable (e.g. select_cells) can be filled with the positions
*      at which to write the variable data. The select variable must be single dimensional
*      as the multi-dimensional indeces (if required) are packed together in the array.
*
*      This example code writes a variable named "space_select" and uses the select 
*      functionality to write a row of a ranks data (corresponding to the iteration number)
*      in the row of the previous ranks array on disk.
*
* Example run: Run with 5 MPI ranks: 4 clients and 1 Damaris server
*     >mpirun -np 5 ./2dmesh_select 2dmesh_select.xml -p 2 -i 0
*      
*     
*     Example output:
*     >h5dump -d /space_select 2dmesh_select_It0.h5
*
*      HDF5 "2dmesh_It0.h5" {
*      DATASET "/space_select" {
*         DATATYPE  H5T_IEEE_F32LE
*         DATASPACE  SIMPLE { ( 16, 8 ) / ( 16, 8 ) }
*         DATA {
*                    |--------------------<- wrapped around from rank 1
*         (0,0) : 1, 1, 1, 1, 2, 2, 2, 2, <- wrapped around from rank 2
*         (1,0) : 0, 0, 0, 0, 1, 1, 1, 1,
*         (2,0) : 0, 0, 0, 0, 1, 1, 1, 1,
*         (3,0) : 0, 0, 0, 0, 1, 1, 1, 1,
*         (4,0) : 0, 0, 0, 0, 1, 1, 1, 1,
*         (5,0) : 0, 0, 0, 0, 1, 1, 1, 1,
*         (6,0) : 0, 0, 0, 0, 1, 1, 1, 1,
*         (7,0) : 0, 0, 0, 0, 1, 1, 1, 1,
*         (8,0) : 3, 3, 3, 3, 0, 0, 0, 0,  <- wrapped around from rank 0
*         (9,0) : 2, 2, 2, 2, 3, 3, 3, 3,
*         (10,0): 2, 2, 2, 2, 3, 3, 3, 3,
*         (11,0): 2, 2, 2, 2, 3, 3, 3, 3,
*         (12,0): 2, 2, 2, 2, 3, 3, 3, 3,
*         (13,0): 2, 2, 2, 2, 3, 3, 3, 3,
*         (14,0): 2, 2, 2, 2, 3, 3, 3, 3,
*         (15,0): 2, 2, 2, 2, 3, 3, 3, 3
*         }
*      }
*      }
*
*/


int WIDTH;
int HEIGHT;
int dims;         // the number of dimensions of the domain (currently 2D, so equals 2)
int size;         // the number of blocks in either x or y direction
int total_ranks;  // the total number of client ranks
int rank;         // the MPI process rank
int local_width;
int local_height;

int which_rank ;  // used for printing specific ranks data
int which_iter ;  // used for printing data at specific iteration

// What row are we in for this rank (returns 0 based row)
int whatRow( int rankIn, int sizeIn) {
    return (rankIn / sizeIn) ;
}

// What column are we in for this rank (returns 0 based column)
int whatCol( int rankIn, int sizeIn) {
    return (rankIn % sizeIn) ;
}

void printArray(char* tag, int* select_cells ) 
{  
    int i,j;
    //if (rank == which_rank)
    {
        printf("%s rank :%d, data from position [ %d ][ %d ], iteration %d =  ", 
                          tag, rank, whatRow(rank,size)*local_height,  whatCol(rank,size)* local_width, which_iter);
        for (i = 0; i < 1 ; i++) {
            for (j = 0; j < local_width* 2  ; j+=2) {
                int pos = i * local_width * 2  ;
                printf("%3d,",  select_cells[pos + j]);
                printf("%3d  ", select_cells[pos + j + 1]);
            }
            printf("\n");
        }

    }
    
}

void writeData(int iteration , float* space, int* select_cells ) {

    float (*parray)[local_width] =  (float (*)[local_width]) space;
    int i,j, k;

    int global_width  = whatCol(rank,size) * local_width;
    int global_height = whatRow(rank,size) * local_height ;
    if (iteration == 0) {
        if (rank == which_rank) {
            printf("rank = %3d global_offset = %3d\n", which_rank, global_width) ;
            printf("rank = %3d global_offset = %3d\n", which_rank, global_height) ;
        }
    }

    int64_t position_space[dims];

    position_space[0] = global_height ;
    position_space[1] = global_width;

    for(i = 0; i < local_height; i++) {
        for(j = 0; j < local_width; j++){
                parray[i][j] = (float) rank ;
        }
    }
    
    for(i = 0; i < local_height; i++) {
        for(j = 0; j < local_width*dims; j+=dims){
                int pos = i * local_width*dims + j  ;
                select_cells[pos]     = global_height + i ;
                select_cells[pos + 1] = global_width  + (j/dims)  ;
         }
    }
    
    
    if (iteration  == which_iter)
        printArray("mem ", select_cells ) ;

    // Set the global positions of the current domain 
    damaris_set_position("space",position_space);
    damaris_set_position("space_select",position_space);
    
    // Set poistion of the cell sellectioon array - it is 1D with dims indices 
    position_space[0] = rank * (local_height * local_width * dims);
    damaris_set_position("select_cells",position_space);
    // Write the data to Damaris server -both variables will have the same data, 
    // however the space_select will be reordered in the HDF5 output file
    damaris_write("space",parray);
    damaris_write("space_select",parray);
    
    // Now we reorder the select indices so the a row (corresponding to the iteration number) 
    // of each rank is moved to the same row of the prior rank
    int offset_rank = rank - 1 ;
    if (offset_rank < 0)
        offset_rank = total_ranks - 1 ;
    
    global_width  = whatCol(offset_rank,size) * local_width;
    global_height = whatRow(offset_rank,size) * local_height ;
    
     for(i = iteration; i < iteration+1; i++) {
         // A single row of data will be changed
         // corresponding to the iteration number
        for(j = 0; j < local_width*dims; j+=dims){
                int pos = i * local_width*dims  + (j) ;
                select_cells[pos]     = global_height + i ;
                select_cells[pos + 1] = global_width  + (j/dims)  ;
        }
    }

    // time-varying="true" in the XML and by designed
    // if not then only write data on first (0th) iteration
    damaris_write("select_cells_var",select_cells);
    if (iteration  == which_iter)
        printArray("mem ", select_cells ) ;
    
}




int main(int argc, char** argv)
{
    if(argc < 2)
    {
        fprintf(stderr,"Usage: %s <2dmesh_select.xml> [-p rank] [-i iter] \n",argv[0]);
        fprintf(stderr,"      -p rank...   the rank from which to print the select_cells array \n");
        fprintf(stderr,"      -i iter...   the iteration from which to print the select_cells array \n");
        exit(0);
        
    }
    
    
    MPI_Init(&argc,&argv);
    
    damaris_initialize(argv[1],MPI_COMM_WORLD);

    int is_client;
    int err = damaris_start(&is_client);
    
    if((err == DAMARIS_OK || err == DAMARIS_NO_SERVER) && is_client) {
    
        MPI_Comm comm;
        damaris_client_comm_get(&comm);

        damaris_parameter_get("WIDTH" , &WIDTH , sizeof(int));
        damaris_parameter_get("HEIGHT" , &HEIGHT , sizeof(int));
        damaris_parameter_get("dims", &dims , sizeof(int));
        damaris_parameter_get("size", &size , sizeof(int));

        MPI_Comm_rank(comm , &rank);
        MPI_Comm_size(comm , &total_ranks);
        
        which_rank = 0 ;
        // printf("argc = %s\n",argv[3]) ;
        if(argc >= 4)
           which_rank = atoi(argv[3]) ;
        if (which_rank >= total_ranks) 
            which_rank = 0 ;
        
        which_iter = 0 ;
        if(argc >= 6)
           which_iter = atoi(argv[5]) ;
        if (which_iter >= MAX_CYCLES) 
            which_iter = 0 ;
        
        // This example is designed for a square decomposition, e.g. if total_ranks==4 then we will have 2x2 domains
        assert((size*size) == total_ranks) ;

        // Cut domain into 4 tiles as size x size blocks 
        local_width      = WIDTH/size;
        local_height     = HEIGHT/size;
        
        // We have to divide exactly - user must ensure paramaters in XML fit correctly
        assert((WIDTH%size) == 0) ;
        assert((HEIGHT%size) == 0) ;
        if (rank == which_rank)
        {
            printf("rank = %3d dims         = %d\n",rank, dims) ;
            printf("rank = %3d HEIGHT       = %d\n",rank, HEIGHT) ;
            printf("rank = %3d WIDTH        = %d\n",rank, WIDTH) ;
            printf("rank = %3d local_height = %d\n",rank, local_height) ;
            printf("rank = %3d local_width  = %d\n",rank, local_width) ;
            
        }

        // Allocate our 2D floating point array, the size of the local block
        float (*space)[local_width] = malloc((local_height)*(local_width)*sizeof(float));
        // Allocate the 1D index array
        int *select_cells           = malloc((dims)*(local_height)*(local_width)*sizeof(float));
        int i,j;

        for(i=0; i < MAX_CYCLES; i++) {
            double t1 = MPI_Wtime();

            writeData(i , (float*)space, (int*) select_cells);
            damaris_end_iteration();
            
            MPI_Barrier(comm);

            double t2 = MPI_Wtime();

            if(rank == 0) {
                printf("2dmesh: Iteration %d done in %f seconds\n",i,(t2-t1));
            }
        }

        damaris_stop();
        free(space);
        free(select_cells);
    }
     
    damaris_finalize();
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
