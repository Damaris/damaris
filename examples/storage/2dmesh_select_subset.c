#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <assert.h>

#include "Damaris.h"

#define MAX_CYCLES 3

/**
* Description: A test example for the HDF5Store class OutputCollectiveSelect().
*      This example exercises the HDF5 collective ouput that uses a seperate array
*      of indices to order the data on disk. A Damaris variable can be indicated as
*      using this method by providing a select="select_cells" attribute to the 
*      <variable ...> tag.
*      The indicated variable (e.g. select_cells) can be filled with the positions
*      at which to write the variable data. The select variable must be single dimensional
*      as the multi-dimensional indeces (if required) are packed together in the array.
*
*      This example code writes a variable named "space_select" and uses the select 
*      functionality to write a row of a ranks data (corresponding to the iteration number)
*      in the row of the previous ranks array. It also requires three other Damaris variables
*      used to describe the shape of the selected data:
*              
*
*      This code is designed for a equal decomposition in each axis, to form a retangular/ cuboid
*      structure that is compatible with HDF5 dataset shapes. If this is not possible then a 1D
*      dimension dataset will be required.
*        e.g. if client_ranks == 4 then we will have size x size domains with size == 2
*      N.B. The number of values in a domain can be different, but must add to a
*           equivalent value per row or column 
*           e.g.
*            ranks:              sizes:
*                ------ ---   
*               |   0  | 1 |            1,6   1,3
*                ------ ---
*               | 2 |   3  |            2,3   2,6
                |   |      |  
                 --- ------ 
*
*            This example is set up to be symmetric:
*                ---- ----   
*               | 0  | 1  |            1,4   1,4
*                ---- ---- 
*               | 2  | 3  |            1,4   1,4
                 ---- ----
* Example run: Run with 5 MPI ranks: 4 clients and 1 Damaris server
*     >mpirun -np 5 ./2dmesh_select_subset 2dmesh_select_subset.xml -p 2 -i 0
*      
*     
*     Example output:
*     >h5dump  2dmesh_select_subset_It0.h5
*       ...

*      HDF5 "2dmesh_It0.h5" {
*      DATASET "/space_select" {
*         DATATYPE  H5T_IEEE_F32LE
*         DATASPACE  SIMPLE { ( 16, 8 ) / ( 16, 8 ) }
*         DATA {
*         (0,0) : 1, 1, 1, 1, 2, 2, 2, 2, 
*         (1,0) : 3, 3, 3, 3, 0, 0, 0, 0,  <- wrapped around from rank 0
*         }
*      }
*      }
*
*/


int WIDTH;
int HEIGHT;
int dims;         // the number of dimensions of the domain (currently 2D, so equals 2)
int size;         // the number of blocks in either x or y direction
int client_ranks;  // the total number of client ranks
int rank;         // the MPI process rank
int local_width;
int local_height;

int which_rank ;  // used for printing specific ranks data
int which_iter ;  // used for printing data at specific iteration

// What row are we in for this rank (returns 0 based row)
int whatRow( int rankIn, int sizeIn) {
    return (rankIn / sizeIn) ;
}

// What column are we in for this rank (returns 0 based column)
int whatCol( int rankIn, int sizeIn) {
    return (rankIn % sizeIn) ;
}

void printArray(char* tag, int* select_cells ) 
{  
    int i,j;
    //if (rank == which_rank)
    {
        printf("%s rank :%d, data from position [ %d ][ %d ], iteration %d =  ", 
                          tag, rank, whatRow(rank,size)*local_height,  whatCol(rank,size)* local_width, which_iter);
        for (i = 0; i < 1 ; i++) {
            for (j = 0; j < local_width* 2  ; j+=2) {
                int pos = i * local_width * 2  ;
                printf("%3d,",  select_cells[pos + j]);
                printf("%3d  ", select_cells[pos + j + 1]);
            }
            printf("\n");
        }

    }
    
}

void writeData(int iteration , float* space, int* select_cells_mem, int* select_cells_file, int * file_desc_data ) {

    float (*parray)[local_width] =  (float (*)[local_width]) space;
    int i,j ;

    int offset_height = whatRow(rank,size) * local_height ;
    int offset_width  = whatCol(rank,size) * local_width;
    if (iteration == 0) {
        if (rank == which_rank) {
            printf("rank = %3d global_offset = [%3d, %3d]\n", rank, offset_height, offset_width) ;

        }
    }

    int64_t position_space[dims];

    position_space[0] = offset_height ;
    position_space[1] = offset_width;

    for(i = 0; i < local_height; i++) {
        for(j = 0; j < local_width; j++){
                parray[i][j] = (float) rank ;
        }
    }
    
    // Set the global positions of the current domain 
    damaris_set_position("space",position_space);
    damaris_set_position("space_select",position_space);
    
    // Set position of the cell selection array - it is 1D with dims indices 
    // position_space[0] = rank * (local_width * dims);
    // damaris_set_position("select_cells_var",position_space);
    
    // Write the data to Damaris server -both variables will have the same data, 
    // however the space_select output data will be a subset of the initial array 
    // and reordered in the HDF5 output file
    damaris_write("space",parray);
    damaris_write("space_select",parray);
    
    // Now we reorder the select indices so the a row (corresponding to the iteration number) 
    // of each rank is moved to the same row of the prior rank
    int offset_rank = rank - 1 ;
    if (offset_rank < 0)
        offset_rank = client_ranks - 1 ;
    
    offset_width  = whatCol(rank,size) * local_width;
    offset_height = whatRow(rank,size) * local_height ;
    
     for(i = 0; i < 1; i++) {
         // A single row of data will be changed
         // corresponding to the iteration number
        for(j = 0; j < local_width*dims; j+=dims){
                int pos = j + i ;
                select_cells_file[pos]     = whatRow(offset_rank,size)  ;
                select_cells_file[pos + 1] = offset_width  + (j/dims)  ;
                select_cells_mem[pos]     = iteration ;
                select_cells_mem[pos + 1] = (j/dims)  ;
        }
    }
    
    // We are specifying the size and shape of the output selected data using this data
    file_desc_data[0] = 2 ;
    file_desc_data[1] = 2 ;
    file_desc_data[2] = 1 ;
    file_desc_data[3] = local_width ;
    file_desc_data[4] = whatRow(rank,size) ;
    file_desc_data[5] = whatCol(rank,size) * local_width ;
    damaris_write("select_subset_var",file_desc_data) ;
    
    position_space[0] = rank * (local_width * dims);
    damaris_set_position("select_cells_mem_var", position_space);
    damaris_set_position("select_cells_file_var",position_space);
    
    // time-varying="true" in the XML and by design
    // if not then only write data on first (0th) iteration
    damaris_write("select_cells_mem_var",select_cells_mem);
    damaris_write("select_cells_file_var",select_cells_file);
    if (iteration  == which_iter) {
        printArray("mem ", select_cells_mem ) ;
        printArray("file", select_cells_file ) ;
        printf("\n");
    }
    
}




int main(int argc, char** argv)
{
    if(argc < 2)
    {
        fprintf(stderr,"Usage: %s <2dmesh_select_subset.xml> [-p rank] [-i iter] \n",argv[0]);
        fprintf(stderr,"      -p rank...   the rank from which to print the select_cells array \n");
        fprintf(stderr,"      -i iter...   the iteration from which to print the select_cells array \n");
        exit(0);
        
    }
    
    
    MPI_Init(&argc,&argv);
    
    damaris_initialize(argv[1],MPI_COMM_WORLD);

    int is_client;
    int err = damaris_start(&is_client);
    
    if((err == DAMARIS_OK || err == DAMARIS_NO_SERVER) && is_client) {
    
        MPI_Comm comm;
        damaris_client_comm_get(&comm);

        damaris_parameter_get("WIDTH" , &WIDTH , sizeof(int));
        damaris_parameter_get("HEIGHT" , &HEIGHT , sizeof(int));
        damaris_parameter_get("dims", &dims , sizeof(int));  // We have a 2D domain
        damaris_parameter_get("size", &size , sizeof(int));

        MPI_Comm_rank(comm , &rank);
        MPI_Comm_size(comm , &client_ranks);
        
        // Very simple, unflexible argument parsing
        which_rank = 0 ;
        // get -p argument: MPI client process on which to print from
        if(argc >= 4)
           which_rank = atoi(argv[3]) ;
        if (which_rank >= client_ranks) 
            which_rank = 0 ;
        //if (rank == 0)
        //  printf("2dmesh_select_subset: we will print from rank %d\n",which_rank);
        
        // get -i argument: iteration on which to print from
        which_iter = 0 ;
        if(argc >= 6)
           which_iter = atoi(argv[5]) ;
        if (which_iter >= MAX_CYCLES) {
            printf("2dmesh_select_subset: MAX_CYCLES = %d and you tried to print interation  %d\n",MAX_CYCLES,which_iter);
            which_iter = 0 ;
        }
        if (rank == 0)
          printf("2dmesh_select_subset: we will print interation %d\n",which_iter);
        
        // This example is designed for a equal decomposition in both axes,
        // e.g. if client_ranks==4 then we will have a 2x2 domain and size == 2
        assert((size*size) == client_ranks) ;

        // We have to divide exactly - user must ensure paramaters in XML fit correctly
        assert((WIDTH%size) == 0) ;
        assert((HEIGHT%size) == 0) ;
        
        // Cut domain into equal sized blocks 
        local_width      = WIDTH/size;
        local_height     = HEIGHT/size;
        
        if (rank == which_rank)
        {
            printf("rank = %3d dims         = %d\n",rank, dims) ;
            printf("rank = %3d HEIGHT       = %d\n",rank, HEIGHT) ;
            printf("rank = %3d WIDTH        = %d\n",rank, WIDTH) ;
            printf("rank = %3d local_height = %d\n",rank, local_height) ;
            printf("rank = %3d local_width  = %d\n",rank, local_width) ;
        }

        // Allocate our 2D floating point array, the size of the local block
        float (*space)[local_width] = malloc((local_height)*(local_width)*sizeof(float));
        // Allocate the 1D index array
        int *select_cells_mem           = malloc((dims)*(1)*(local_width)*sizeof(int));  // selects from memory
        int *select_cells_file          = malloc((dims)*(1)*(local_width)*sizeof(int));  // selects placement in file
        
        // This array is the select_subset information variable in the Damaris XML file.
        // It is used to specify the size of the selected data, and only needed if the selected data
        // does not match the size of the original array from which the data is selected from. 
        // We also need to compute the global size of the selected data, so we store the number 
        // of sections we have decomposed out domain into here also. This computation is done
        // using an MPI reduction via the Damaris server HDF5Store class.
        // The select_subset variable will be sized as 2 x (dimensions of domain). 
        // e.g. for this example: 2 x 2 integers.
        // The first dims positions are the number of sections our domain is cut into e.g. size x size = 2 x 2
        // The second dims positions will store the size of our local domain e.g. 1,4 for our small array case.
        // So the array will store: 2,2,1,4 for each Damaris client rank
        
        int *select_subset_data     = malloc(((3)*(dims))*sizeof(int)); 
        // int *select_subset_data     = malloc(2*sizeof(int)); 
        int i,j;

        for(i=0; i < MAX_CYCLES; i++) {
            double t1 = MPI_Wtime();

            writeData(i , (float*)space, (int*) select_cells_mem, (int*)  select_cells_file, (int*) select_subset_data);
            damaris_end_iteration();
            
            MPI_Barrier(comm);

            double t2 = MPI_Wtime();

            if(rank == 0) {
                printf("2dmesh_select_subset: Iteration %d done in %f seconds\n",i,(t2-t1));
            }
        }

        damaris_stop();
        free(space);
        free(select_cells_mem);
        free(select_cells_file);
    }
     
    damaris_finalize();
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
