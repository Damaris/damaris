#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <assert.h>

#include "Damaris.h"

#define MAX_CYCLES 1

/**
* Description: A test example for the HDF5Store class OutputCollectiveSelect().
*      Example 1b from Damaris wiki https://gitlab.inria.fr/Damaris/damaris/-/wikis/Damaris-HDF5-Select-Points-Support 
*
* Example run: Run with 3 MPI ranks: 2 clients and 1 Damaris server
*     >mpirun -np 3 ./2dmesh_select_ex1b 2dmesh_select_ex1b.xml 
*      
*     
*     Example output:
*     >h5dump -d /space_select 2dmesh_select_ex1b_It0.h5
*
*      
*
*/


int WIDTH;
int HEIGHT;
int dims;         // the number of dimensions of the domain (currently 2D, so equals 2)
int size;         // the number of blocks in either x or y direction
int client_ranks;  // the total number of client ranks
int rank;         // the MPI process rank
int local_width;
int local_height;

int which_rank ;  // used for printing specific ranks data
int which_iter ;  // used for printing data at specific iteration

// What row are we in for this rank (returns 0 based row)
int whatRow( int rankIn, int sizeIn) {
    return (rankIn / sizeIn) ;
}

// What column are we in for this rank (returns 0 based column)
int whatCol( int rankIn, int sizeIn) {
    return (rankIn % sizeIn) ;
}



void writeData(int iteration , float* data_var, int* file_var ) {

    float (*data_var_2d)[local_width] =  (float (*)[local_width]) data_var;
    int i,j, k;

    int64_t position_space[dims];

    
    
    
    if (rank == 0) {
        data_var[0] = 1.0 ; 
        data_var[1] = 2.0 ; 
        data_var[2] = 3.0 ; 
        data_var[3] = 4.0 ; 
        data_var[4] = 9.0 ; 
        data_var[5] = 10.0 ;
        data_var[6] = 11.0 ;
        data_var[7] = 12.0 ;
        position_space[0] = 0 ;
        position_space[1] = 0;
    } else if (rank == 1) {
        data_var[0] = 5.0 ; 
        data_var[1] = 6.0 ; 
        data_var[2] = 7.0 ; 
        data_var[3] = 8.0 ; 
        data_var[4] = 13.0 ; 
        data_var[5] = 14.0 ;
        data_var[6] = 15.0 ;
        data_var[7] = 16.0 ;
        position_space[0] = 0 ;
        position_space[1] = 4;
    }
    
    damaris_set_position("data_var",position_space);
    
 
    if (rank == 0) {
        file_var[0] = 0 ; file_var[1] = 0 ;
        file_var[2] = 0 ; file_var[3] = 1 ;
        file_var[4] = 0 ; file_var[5] = 2 ;
        file_var[6] = 1 ; file_var[7] = 4 ;
        file_var[8] = 1 ; file_var[9] = 0 ;
        file_var[10] = 1 ; file_var[11] = 1 ;
        file_var[12] = 1 ; file_var[13] = 2 ;
        file_var[14] = 0 ; file_var[15] = 4 ;
        
        position_space[0] = 0 ;
    } else if (rank == 1) {
        file_var[0] = 0 ; file_var[1] = 3 ;
        file_var[2] = 0 ; file_var[3] = 5 ;
        file_var[4] = 0 ; file_var[5] = 6 ;
        file_var[6] = 0 ; file_var[7] = 7 ;
        file_var[8] = 1 ; file_var[9] = 3 ;
        file_var[10] = 1 ; file_var[11] = 5 ;
        file_var[12] = 1 ; file_var[13] = 6 ;
        file_var[14] = 1 ; file_var[15] = 7 ;
        
        position_space[0] = 16 ;
    }
    
     damaris_set_position("file_var",position_space);
    
   
    // Write the data to Damaris server
    // file_var will be reordered in the HDF5 output file
    damaris_write("data_var",data_var);
    damaris_write("file_var",file_var);

    
}




int main(int argc, char** argv)
{
    if(argc < 2)
    {
        fprintf(stderr,"Usage: %s <2dmesh_select.xml> [-p rank] [-i iter] \n",argv[0]);
        fprintf(stderr,"      -p rank...   the rank from which to print the select_cells array \n");
        fprintf(stderr,"      -i iter...   the iteration from which to print the select_cells array \n");
        exit(0);
        
    }
    
    
    MPI_Init(&argc,&argv);
    
    damaris_initialize(argv[1],MPI_COMM_WORLD);

    int is_client;
    int err = damaris_start(&is_client);
    
    if((err == DAMARIS_OK || err == DAMARIS_NO_SERVER) && is_client) {
    
        MPI_Comm comm;
        damaris_client_comm_get(&comm);

        damaris_parameter_get("WIDTH" , &WIDTH , sizeof(int));
        damaris_parameter_get("HEIGHT" , &HEIGHT , sizeof(int));
        damaris_parameter_get("dims", &dims , sizeof(int));
        damaris_parameter_get("size", &size , sizeof(int));

        MPI_Comm_rank(comm , &rank);
        MPI_Comm_size(comm , &client_ranks);
       
        
        // This example is designed for a square decomposition, e.g. if client_ranks==4 then we will have 2x2 domains
        assert((size) == client_ranks) ;

        // Cut domain into 4 tiles as size x size blocks 
        local_width      = WIDTH/size;
        local_height     = HEIGHT;
        
        // We have to have correct values exactly 
        assert((WIDTH) == 8) ;
        assert((HEIGHT) == 2) ;
        assert((dims) == 2) ;
        if (rank == 0)
        {
            printf("rank = %3d dims         = %d\n",rank, dims) ;
            printf("rank = %3d HEIGHT       = %d\n",rank, HEIGHT) ;
            printf("rank = %3d WIDTH        = %d\n",rank, WIDTH) ;
            printf("rank = %3d local_height = %d\n",rank, local_height) ;
            printf("rank = %3d local_width  = %d\n",rank, local_width) ;
            
        }

        // Allocate our 2D floating point array, the size of the local block
        float (*data_var)[local_width] = malloc((local_height)*(local_width)*sizeof(float));
        // Allocate the 1D index array
        int *file_var           = malloc((dims)*(local_height)*(local_width)*sizeof(float));
        int i,j;

        for(i=0; i < MAX_CYCLES; i++) {
            double t1 = MPI_Wtime();

            writeData(i , (float*)data_var, (int*) file_var);
            damaris_end_iteration();
            
            MPI_Barrier(comm);

            double t2 = MPI_Wtime();

            if(rank == 0) {
                printf("2dmesh: Iteration %d done in %f seconds\n",i,(t2-t1));
            }
        }

        damaris_stop();
        free(data_var);
        free(file_var);
    }
     
    damaris_finalize();
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
