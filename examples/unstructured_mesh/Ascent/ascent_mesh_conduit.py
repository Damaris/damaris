'''
    Damaris example of using Ascent for in situ visualisation.
    
    Script to accept simulation mesh data and output png image of mesh 
    model using Ascent.
    
    This script is designed to be run by a Damaris server process that 
    is part of a simulation (in this case OPM Flow).

'''

def main(DD):
    # conduit + ascent imports
    import conduit
    import conduit.blueprint
    import ascent.mpi

    import math
    import numpy as np
    
    from damaris4py.server import listknownclients
    from damaris4py.dask import damaris_dask
    from damaris4py.server import getservercomm

    try:
        # These two dictionaries are set up in the PyAction constructor 
        # and are static.
        damaris_dict = DD['damaris_env']
        dask_dict    = DD['dask_env']
        # This third dictionary is set up in PyAction::PassDataToPython() and is 
        # typically different each iteration.
        iter_dict    = DD['iteration_data']   
        # This is the iteration value the from Damaris perspective
        # i.e. It is the number of times damaris_end_iteration() has been called
        it       = iter_dict['iteration']

        # There will be one key and corresponding NumPy array for each block of the variable
        # Each Damaris server can look after multiple client ranks
        client_list = listknownclients()      
        iteration    = damaris_dask.return_iteration(DD)
        
        
        # if it == 0:
        print('The DamarisData dictionaries are:')
        keys = list(DD.keys())
        print(keys)
        

        damaris_comm = getservercomm()
        
        # These are the variables that have been published to Python by the Damaris server process:
        print('The DamarisData variables available are :')
        keys = list(iter_dict.keys())
        for data_key in keys :
            if (data_key != 'iteration'):
                print('From Python,  Iteration: ', iteration, 'Variable name: ', data_key)
        
        print("From Python,  Iteration: ", iteration, "client_list: " , client_list) ;        
        total_sum = 0
        
        a = ascent.mpi.Ascent()
        
        
        ascent_opts = conduit.Node()
        ascent_opts["exeeptions"] = "forward"
        ascent_opts["mpi_comm"].set(damaris_comm.py2f())

        a.open(ascent_opts)
        
        multi_mesh_data = conduit.Node()
        
        for client_rank in client_list :
            # We know the variable names as they match what is in the Damaris XML file
            print('From Python,  Iteration: ', iteration)
            numpy_x = damaris_dask.return_numpy_array(DD, 'coordset/coords/values/x', client_rank=client_rank )
            numpy_y = damaris_dask.return_numpy_array(DD, 'coordset/coords/values/y', client_rank=client_rank )
            numpy_z = damaris_dask.return_numpy_array(DD, 'coordset/coords/values/z', client_rank=client_rank )
            print("From Python , Iteration: ", iteration, "x, y, z: ", numpy_x[0], ", ", numpy_y[0], ", ",  numpy_z[0])
            
            numpy_connectivity = damaris_dask.return_numpy_array(DD, "topologies/topo/elements/connectivity", client_rank=client_rank )
            numpy_offsets = damaris_dask.return_numpy_array(DD, "topologies/topo/elements/offsets", client_rank=client_rank )
            numpy_types = damaris_dask.return_numpy_array(DD, "topologies/topo/elements/types", client_rank=client_rank )
            
            # create container node
            mesh_data = conduit.Node()
            #  provide state information
            mesh_data["state/time"].set(iteration)
            mesh_data["state/cycle"].set(iteration)
            mesh_data["state/domain_id"] = client_rank


            # coordinate system data
            mesh_data["coordsets/coords/type"] = "explicit"
            mesh_data["coordsets/coords/values/x"].set_external(numpy_x)
            mesh_data["coordsets/coords/values/y"].set_external(numpy_y)
            mesh_data["coordsets/coords/values/z"].set_external(numpy_z)
            
            # topologies: 
            mesh_data["topologies/topo/type"] = "unstructured"
            mesh_data["topologies/topo/coordset"] = "coords"
            mesh_data["topologies/topo/elements/shape"] = "hex"
            mesh_data["topologies/topo/elements/connectivity"].set_external(numpy_connectivity) 

            numpy_PRESSURE = damaris_dask.return_numpy_array(DD, 'PRESSURE', client_rank=client_rank )
            
            # one or more scalar fields
            mesh_data["fields/pressure/type"]        = "scalar";
            mesh_data["fields/pressure/topology"]    = "topo";
            mesh_data["fields/pressure/association"] = "element";
            
            try:
                mesh_data["fields/pressure/values"].set_external(numpy_PRESSURE);
            except TypeError as err:    
                print('TypeError: No damaris data of name PRESSURE. Client rank: ', client_rank, '; Iteration: ' , iteration, '; Error: ', err)
            
            mesh_name = 'mesh_' + str(client_rank)
            multi_mesh_data[mesh_name] = mesh_data
            
            
        a.publish(multi_mesh_data);             
        actions = conduit.Node()
        add_act = actions.append()
        add_act["action"] = "add_scenes"
        
        scenes = add_act["scenes"]
        
        scenes["s1/plots/p1/type"]  = "pseudocolor"
        scenes["s1/plots/p1/field"] = "pressure"
        
        scenes["s1/image_name"] = "damaris_ascent_opmflow_pressure_iter_" + str(iteration)
        
        a.execute(actions)
        
        a.close()
        
    except KeyError as err: 
        print('KeyError: No damaris data of name: ', err)
    except PermissionError as err:
        print('PermissionError!: ', err)
    except ValueError as err:
        print('Damaris Data is read only!: ', err)
    except UnboundLocalError as err:
        print('Damaris data not assigned!: ', err)   
    finally:
        pass


if __name__ == '__main__':
    main(DamarisData)

