# Python code: stats_3dmesh_dask.py
# Author: Josh Bowden, Inria
# Description: 
# Part of the Damaris examples of using Python integration with Dask distributed
# To run this example on Grid5000 (OAR job scheduler), use the script: stats_launcher.sh
# The script is designed to test the damaris4py.damaris_stats class named DaskStats
#  

import numpy as np 
np.set_printoptions(threshold=np.inf)


# N.B. This file is read by each Damaris server process on each iteration that is 
#      specified by the frequency="" in the <pyscript> XML sttribute.


def main(DD):
    import time
    
    from   dask.distributed import Client, TimeoutError, Pub
    from   damaris4py.server import getservercomm
    from   damaris4py.server import magicnumber_string
    from   damaris4py.dask import damaris_dask


    try:

        damaris_comm = getservercomm()   
        rank = damaris_comm.Get_rank()
        # This is the iteration value the from Damaris perspective
        # i.e. It is the number of times damaris_end_iteration() has been called
        # iteration        = iter_dict['iteration']
        iteration    = damaris_dask.return_iteration(DD)         
        # assert iteration == iteration_dam
        if iteration == 0:
            print('Py  Iteration ' + str(iteration) + '  magicnumber_string ' + magicnumber_string())
        damaris_comm.Barrier()

        # Do some Dask stuff - running statistics
        # If this scheduler_file exists (is not an empty string) then a Dask scheduler was 
        # found (and access has been tested on the Damaris server C++ Python side).
        scheduler_file  = damaris_dask.return_scheduler_filename(DD)        
        if iteration == 0:
            print('Python INFO: Scheduler file '+ scheduler_file) 
        if (scheduler_file != ""):                        
            try:      
                client =  Client(scheduler_file=scheduler_file, timeout='10s')

                # We now create the Dask dask.array
                # We now scatter the numpy arrays to the dask scheduler
                damaris_dask.convert_to_scattered_future(DD, client, 'cube_d', print_time=True )
                # return_dask_string_v5 requires convert_to_scattered_future() to be called first
                # 'client' has to match the name of the client variable in the subscriber process (Process using the Dask Sub(name='mydata') call)
                str_plus_list  =  damaris_dask.return_dask_string_v5(DD, 'client', 'cube_d', damaris_comm, print_time=True ) 

                # Only rank 0 returns the dask.array string, the others return None
                try:
                    VAR_dask_str = str_plus_list[0]
                except TypeError as err: 
                    VAR_dask_str = None
                    
                if (VAR_dask_str is not None):
                    mydatadict_scatterd = str_plus_list[1] 
                    pub = Pub(name='mydata')
                    to_stats_server = dict({'iteration':iteration, 'dask_string':VAR_dask_str, 'client_datasets':mydatadict_scatterd, 'variable':'cube_d', 'continue':True})
                    pub.put(to_stats_server) # cube_d may be a future

                client.close()
                
            except TimeoutError as err:
                print('Python ERROR: TimeoutError!: ', err) 
            except OSError as err:
                print('Python ERROR: OSError!: ', err)

        else:
             print('Python INFO: Scheduler file not found:', scheduler_file)
    except KeyError as err: 
        print('Python ERROR: KeyError: No damaris data of name: ', err)
    except PermissionError as err:
        print('Python ERROR: PermissionError!: ', err)
    except ValueError as err:
        print('Python ERROR: Damaris Data problem!: ', err)
    except UnboundLocalError as err:
        print('Python ERROR: Damaris data not assigned!: ', err)
    except NameError as err:
        print('Python ERROR: NameError: ', err)
    # finally: is always called.    
    finally:
        pass



if __name__ == '__main__':
    main(DamarisData)
