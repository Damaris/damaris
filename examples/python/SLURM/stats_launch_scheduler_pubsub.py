#!/usr/bin/python

if __name__ == "__main__":
    from dask.distributed import Client
    from dask.distributed import Sub, Pub
    from damaris4py.dask  import damaris_stats

    import dask.array as da
    import numpy as np
    import sys, getopt, time, os, gc
    import datetime
    
    from dask.distributed import performance_report

    # main(sys.argv[1:])
    argv = sys.argv[1:]
    sched_file = ''
    try:
        opts, args = getopt.getopt(argv,"hs:",["scheduler-file="])
    except getopt.GetoptError:
        print( str(sys.argv[0:],) + ' -s <schedulerfile.json> ')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print( str(sys.argv[0:],) + ' -s <schedulerfile.json> ')
            sys.exit()
        elif opt in ("-s", "--scheduler-file"):
            sched_file = arg

    print('Scheduler file is '+ sched_file)
    SCHED_JOB_ID_str = os.environ.get('SLURM_JOB_ID')
    print('Mem:SLURM_JOB_ID',SCHED_JOB_ID_str, flush=True)

    total_time = time.perf_counter()
    stime = time.perf_counter()
    client = Client(scheduler_file=sched_file) 
    ftime = time.perf_counter()
    print(str(0), f'STIME: client = Client(scheduler_file=sched_file) took {ftime - stime:0.8f} seconds', flush=True)
    ##########
    ## Collect the results of the simulations
    ##########
    sub = Sub(name='mydata', client=client)
    print('Mem: have the subscriber: sub.get() ' , flush=True) 
    mydata = sub.get()  # this will block until a result is published from a simulation client
    print('Mem: returned from: sub.get() ', flush=True)
    if (mydata['continue'] == True):
        dask_str = mydata['dask_string']
        variable = mydata['variable']
        iteration = mydata['iteration']
        datasets_to_delete = mydata['client_datasets']
        global data
        exec( "global data; " + dask_str )
        # This creates the Dask dask.array from the client.datastets shared dask.arrays
        not_fut_da = da.block( data )
        
    array_num = 1
    datasets_to_delete_old = []

    # Assumes mydata is a dask.array
    while (mydata['continue'] == True):
        if array_num == 1:
            if variable == 'cube_d':
                daskstats_pressure = damaris_stats.DaskStats(mydata['dask_array'], client)

        if variable == 'cube_d':
            daskstats_pressure.update(not_fut_da, client)
        
        for ds in datasets_to_delete_old:
            del client.datasets[ds]
        
        mydata = sub.get()  # this will block, needs to be set to a bool to escape the loop.
        
        if (mydata['continue'] == True):
            dask_str = mydata['dask_string']
            variable = mydata['variable']
            iteration = mydata['iteration']
            datasets_to_delete_old = datasets_to_delete
            datasets_to_delete = mydata['client_datasets']
            exec( "global data; " + dask_str )
            not_fut_da = da.block( data )
            array_num = array_num + 1
            
            
    ########## 
    ## Get the results 
    ##########
    if (array_num > 1):
        (mean, populationVariance) =  daskstats_pressure.return_mean_var_p_tuple(client)
        count_dask = daskstats_pressure.return_count()
        print('')
        print('STIME:                                  array_num  value is: ', array_num, flush=True)
        print('STIME: daskstats_pressure.return_count  count_dask value is: ', count_dask, flush=True)
        mean_res = mean.compute()
        var_res = populationVariance.compute()
        
        print('mean_res[0]:      ',mean_res.flatten()[0])
        print('var_res[0]:      ', var_res.flatten()[0])
        
        with open('mean_res_dask.npy', 'wb') as f:
            np.save(f, mean_res)
        with open('var_res_dask.npy', 'wb') as f:
            np.save(f, var_res)
        
    else:
        print(str(array_num), f'Mem:_Scheduler_Python_Script sis NOT produce any results')
    
    print(str(array_num), f'Mem:_Scheduler_Python_Script took {ftime - total_time:0.8f} seconds')

            

