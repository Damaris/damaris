#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --mem-per-cpu=1GB
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32
#SBATCH --ntasks-per-socket=16
#SBATCH --cpus-per-task=4
#SBATCH --output=scheduler_%j.std
#SBATCH --error=scheduler_%j.err
#SBATCH --job-name=scheduler


# Author: Josh Bowden, INRIA
# Date: 29/07/2022
# Description: Launch Dask scheduler
# Damaris Dask example 3dmesh_dask_stats on a SLURM based cluster
echo "We are now running the stats_launch_scheduler.sh script..."

module list

DASK_SCHEDULER_FILE=$DASK_SCHEDULER_FILE
echo  "$0 : scheduler file: $DASK_SCHEDULER_FILE)"

# number of OpenMP threads
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 
# OpenMP binding
export OMP_PLACES=sockets

# collectl -sZ -i:1 --procopts m --procfilt cdask
free -s 10 -m &

echo "INFO $0 : Launching scheduler...  --scheduler-file = $DASK_SCHEDULER_FILE"
dask scheduler --scheduler-file $DASK_SCHEDULER_FILE &
sleep 5

echo "INFO $0 : Launching worker...  --scheduler-file = $DASK_SCHEDULER_FILE  --nthreads $SLURM_CPUS_PER_TASK"
# This should block until the scheduler is shutdown by a client.shutdown()
dask-worker --scheduler-file $DASK_SCHEDULER_FILE --nworkers 30 --nthreads $SLURM_CPUS_PER_TASK  &

echo "Info Launch scheduler: calling python dask_scheduler_node_pubsub.py"
echo "Mem:stats_launch_scheduler_pubsub.py"  
python  stats_launch_scheduler_pubsub.py -s $DASK_SCHEDULER_FILE 









