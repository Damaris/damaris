# Python code: stats_3dmesh_dask.py
# Author: Josh Bowden, Inria
# Description: 
# Returns causes the statistics collection script to exit its collection loop and compute and save the statsitics by Dask.
# 
# Part of the Damaris examples of using Python integration with Dask distributed
# To run this example use the script: stats_launcher.sh
# The script is designed to test the damaris4py.damaris_stats class named DaskStats
# 
# N.B. If 4 simulations are launched which each runs 4 iterations, 
#      and each adding a integer 1, 2, 3, or 4 distributed over the 4 simulations 
#      on each iteration, then: 
#      The mean of the first blocks will be 2.5 
#      and the variance for 4 iterations will be 1.333...

from   dask.distributed import Client, Pub
from   damaris4py.dask import damaris_stats
import sys
import numpy as np
import traceback

np.set_printoptions(threshold=np.inf)

def print_help():
    print(sys.argv[0])
    print('                -f <path to Dask scheduler json file>  ')
    print('                -h  this help message ')


scheduler_file = ''
if len(sys.argv) > 1: 
    if (sys.argv[1] == '-f'):
        scheduler_file = sys.argv[2]
    elif (sys.argv[1] == '-h'):
        print_help()
        sys.exit()
    else:
        print_help()
        sys.exit()
else:
    print_help() 
    sys.exit()
    
try:

    if (scheduler_file != ''):             
        try:      
            client =  Client(scheduler_file=scheduler_file, timeout='2s')
            
            pub = Pub(name='mydata')
            to_stats_server = dict('continue':False})  # This will exit the statistics collection loop
            pub.put(to_stats_server) 
            
            time.sleep(10)
            # close the client only:
            client.close()
            
        except TimeoutError as err:
            print(sys.argv[0], '  ERROR: TimeoutError!: ', err)
            traceback.print_exception(*sys.exc_info())
        except OSError as err:
            print(sys.argv[0], '  ERROR: OSError!: ', err)
            traceback.print_exception(*sys.exc_info())
    else:
        print(sys.argv[0], '  INFO: Scheduler file not found:', scheduler_file)
        print_help() 
         
except KeyError as err: 
    print(sys.argv[0], ' ERROR: KeyError: No damaris data of name: ', err)
    traceback.print_exception(*sys.exc_info())
except PermissionError as err:
    print(sys.argv[0],' ERROR: PermissionError!: ', err)
    traceback.print_exception(*sys.exc_info())
except ValueError as err:
    print(sys.argv[0],' ERROR: Damaris Data problem!: ', err)
    traceback.print_exception(*sys.exc_info())
except UnboundLocalError as err:
    print(sys.argv[0],' ERROR: Damaris data not assigned!: ', err)
    traceback.print_exception(*sys.exc_info())
except NameError as err:
    print(sys.argv[0],' ERROR: NameError: ', err)
    traceback.print_exception(*sys.exc_info())
# finally: is always called.    
finally:
    pass
