#!/bin/bash
#OAR -l /nodes=1/core=16,walltime=01:00:00
#OAR -O scheduler_pubsub_%jobid%.std
#OAR -E scheduler_pubsub_%jobid%.err
#OAR -n scheduler_pubsub

# Author: Josh Bowden, INRIA
# Date: 20/07/2022
# Description: Launch Dask scheduler 
# Damaris Dask example 3dmesh_dask_stats on Grid5000
echo "We are now running the $0 script..."

DASK_SCHEDULER_FILE=$1
echo "Info Launch scheduler: $0 was passed argument: $DASK_SCHEDULER_FILE"
# DASK_SCHEDULER_FILE=/home/jbowden/dask_file.json

dask scheduler --scheduler-file $DASK_SCHEDULER_FILE &

sleep 10

# This should block until the scheduler is shutdown by a client.shutdown()
dask worker --scheduler-file $DASK_SCHEDULER_FILE --nworkers 4 --nthreads 2 &

echo "Info Launch scheduler: calling python stats_launch_scheduler_pubsub.py"
date
python stats_launch_scheduler_pubsub.py -s $DASK_SCHEDULER_FILE 
date
echo "Info Launch scheduler: returned from stats_launch_scheduler_pubsub.py"



