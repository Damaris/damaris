# Scripts for running multiple test case simulations

The driver script is:

```
stats_launcher_pubsub.sh <num_sims>
```

The driver script goes through the following steps:

1. Get value for number of simulations to run from the command line (if present).

2. Shutdown and remove any previous Dask scheduler.

3. Launch the Dask scheduler - all jobs subsequent will attach to this scheduler
   The name of the scheduler-file must match what is stored in the Damaris XML <pyscript> tag 
   of the simulations (launched in stats_launch_one_job_pubsub.sh).

4. Poll to wait until Dask scheduler job has stared.

5. Launch the simulations, passing in a different value to add to the dataset ($i)
   N.B. these jobs implicitly use a Dask scheduler, which is specified in the Damaris
   XML file <pyscript> tag - it must match the path to DASK_SCHEDULER_FILE variable 
   that is used in the stats_launcher_pubsub.sh script.

6. Wait for OAR jobs to finish.

7. Collect the summary statistics - save to file or print to screen (if small enough).

8. Remove the Dask scheduler job.

   
To check results:
```
cat scheduler_pubsub_1911306.std | grep '\[\[\['
[[[2.500000e+00 3.500000e+00 4.500000e+00 5.500000e+00]]
[[[1.33333333 1.33333333 1.33333333 1.33333333]]

# Check Damaris server time
find workdir -type f -name *.log  | xargs cat | grep TIME
[2022-09-13 11:20:30.584792]: TIME: server_pid, iteration , t_server_run, t_server_free
[2022-09-13 11:20:32.879537]: TIME:  0, 0, 2.294714, 0.000000
[2022-09-13 11:20:37.223239]: TIME:  0, 1, 1.632972, 2.710757
[2022-09-13 11:20:42.225612]: TIME:  0, 2, 1.629612, 3.372752
[2022-09-13 11:20:47.248084]: TIME:  0, 3, 1.646211, 3.376259

```
  
To remove artifacts:
```
rm -fr workdir/*
rm *.err *.std
```
