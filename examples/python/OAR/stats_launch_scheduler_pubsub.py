#!/usr/bin/python

from dask.distributed import Client
from dask.distributed import Sub
from damaris4py.dask  import damaris_stats
import numpy as np
import dask.array as da

import sys, getopt

def main(argv):
    sched_file = ''
    try:
        opts, args = getopt.getopt(argv,"hs:",["scheduler-file="])
    except getopt.GetoptError:
        print( str(sys.argv[0:],) + ' -s <schedulerfile.json> ')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print( str(sys.argv[0:],) + ' -s <schedulerfile.json> ')
            sys.exit()
        elif opt in ("-s", "--scheduler-file"):
            sched_file = arg

    print('Scheduler file file is '+ sched_file)
    client = Client(scheduler_file=sched_file)

    ##########
    ## Collect the results of the simulations
    ##########
    sub = Sub(name='mydata', client=client)
    print('Mem: have the subscriber: sub.get() ' , flush=True) 
    mydata = sub.get()  # this will block until a result is published from a simulation client
    print('Mem: returned from: sub.get() ', flush=True)
    # Assumes mydata a dictionary
    if (mydata['continue'] == True):
        dask_str = mydata['dask_string']
        variable = mydata['variable']
        iteration = mydata['iteration']
        datasets_to_delete = mydata['client_datasets']
        print('subscriberd to mydata and found variable: ', variable, flush=True)
        global data
        exec( "global data; " + dask_str )
        # This creates the Dask dask.array from the client.datastets shared dask.arrays
        not_fut_da = da.block( data )
        
    array_num = 1
    datasets_to_delete_old = []

    # Assumes mydata a dictionary
    while (mydata['continue'] == True):
        if array_num == 1:
            if variable == 'cube_d':
                daskstats_cube_d = damaris_stats.DaskStats(not_fut_da, client)

        if variable == 'cube_d':
            daskstats_cube_d.update(not_fut_da, client)
        
        # for ds in datasets_to_delete_old:
            # print('deleting client.dataset[]: ', ds, flush=True)
            # del client.datasets[ds]
        
        mydata = sub.get()  # this will block, needs to be set to a bool to escape the loop.
        
        if (mydata['continue'] == True):
            dask_str = mydata['dask_string']
            variable = mydata['variable']
            iteration = mydata['iteration']
            print('subscriberd to mydata and found variable: ', variable, flush=True)
            datasets_to_delete_old = datasets_to_delete
            datasets_to_delete = mydata['client_datasets']
            exec( "global data; " + dask_str )
            not_fut_da = da.block( data )
            array_num = array_num + 1
            
    ########## 
    ## Get the results 
    ##########
    if (array_num > 1):
        (mean, populationVariance) =  daskstats_cube_d.return_mean_var_p_tuple(client)
        count_dask = daskstats_cube_d.return_count()
        print('')
        print('STIME:                                array_num  value is: ', array_num, flush=True)
        print('STIME: daskstats_cube_d.return_count  count_dask value is: ', count_dask, flush=True)
        mean_res = mean.compute()
        var_res = populationVariance.compute()
        
        print('mean_res[0]:      ',mean_res.flatten()[0])
        print('var_res[0]:      ',var_res.flatten()[0])
        
        with open('mean_res_dask.npy', 'wb') as f:
            np.save(f, mean_res)
        with open('var_res_dask.npy', 'wb') as f:
            np.save(f, var_res)
        
    else:
        print(str(array_num), f'Mem:_Scheduler_Python_Script sis NOT produce any results')
        
    # print(str(array_num), f'Mem:_Scheduler_Python_Script shutting down the scheduler')
    # client.shutdown()


if __name__ == "__main__":
   main(sys.argv[1:])