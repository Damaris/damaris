# A simple data viewer using Bokeh to view streaming data published via Dask Pub-Sub system

## Introduction
Original code from : https://discourse.bokeh.org/t/how-to-send-plot-updates-to-the-bokeh-server/1499/4

This is a demonstration app that alows a simple 2d plot of incoming data to be updated live as data is being generated.

The data source is a Dask 'publisher', to which this app subscribes, so this app can 
be running on a different computing resource from the data source, it just needs to 
know the Dask scheduler being used, by passing in a Dask server scehduler file location.

To pass data to this app, a Python script must be connected to the same Dask scheduler 
and then can publish data using something like the following:

```python
from dask.distributed import Client
from dask.distributed import Sub, Pub

...

# The 'name' of the Pub object is important and must match what is used in the Bokeh app (main.py).
pub_data_to_bokeh = Pub(name='SIMULATION_DATA') 

...

my_data = <some calculation>
pub_data_to_bokeh.put(my_data)

```


## Setup port forwarding for access to Bokeh server from your local computer
If running on a cluster with a login node, you need to reverse tunnel from the compute node to the cluster login node:

This would be run inside a cluster job manager shell script when launching a simulation
```bash
# launch in directory above the view_app directory
cd <path_to_app>\view_app\.. 

# launch the bokeh server application. Note the port specified is needed for the ssh commands that follow.
bokeh serve view_app --port 5100 --args -s <scheduler file> & 

# Setup reverse port frowarding from the compute node back to the login node
ssh -NR 5100:localhost:5100 user@login.node &
```
  
Then from your machine where you want to run the web browser to view the app:
```bash
ssh -NL 5100:localhost:5100 user@login.node &
```

Now, again, on your local maching (from where you ran the second ssh port forwarding command above) 
open a browser and point it to: localhost:51000/view_app
