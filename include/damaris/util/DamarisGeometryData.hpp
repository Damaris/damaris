#ifndef DAMARIS_UNSTRUCTURED_MESH_MODEL_DATA_HPP
#define DAMARIS_UNSTRUCTURED_MESH_MODEL_DATA_HPP

#include <string>
#include <iostream>
#include <sstream>
#include <cassert>

#include <Damaris.h>
#include "damaris/util/DamarisVar.hpp"

/*
    This is a set of classes to do either of two things, or both combined:
    1/ Fill in programatically the details of the damaris variables and mesh details for use with the XML creation 
       using the damaris::model::ModifyModel class. The DamarisGeometryData class is designed to model Unstructured Mesh data
       with various ways of defining vertex data.
    2/ The DamarisVar class can be used to allocate memory in the Damaris shared memory region and a user can supply
       the data required for the variable to pass to Damaris.

    The entries in the map below will be filled by corresponding Damaris
    Keywords.
*/


namespace damaris
{
    namespace model
    {
    
    // These relate to Coduit Blueprint MCArray data types (see https://llnl-conduit.readthedocs.io/en/v0.8.1/blueprint_mcarray.html )
    typedef enum { VERTEX_SEPARATE_X_Y_Z, VERTEX_INTERLEAVED_A_OF_S, VERTEX_CONTIGUOUS_S_OF_A,  VERTEX_INTERLEAVED_MIXED_A_OF_S } vertex_data_structure ;
            
    /**
    *  class to store a Damaris variables needed to create an (unstructured) geometric mesh
    *  If being used to collect together mesh data, we need to test for the data type used for verticies and indices
    *    e.g. 
    *    DAMARIS_TYPE_STR vartype ;
    *    damaris_err = damaris_get_type(coordset_coords_values_x.c_str(), &vartype)  ;
    *    if (damaris_err != DAMARIS_OK) {
    *        std::cerr << "ERROR rank =" << rank << " : DamarisVisDataWriter : damaris_get_type(\"coordset_coords_values_x.c_str(), &vartype)\" "<< std::endl ;
    *    }
    *              
    *    
    */
    class DamarisGeometryData 
    {
            int rank_ ;                                        //!< MPI rank of process
            // So the user can choose the data structure to use 
            vertex_data_structure vertex_structure_ ;           //!< Defines the data layout of the mesh vertex data. Choices are: individual x,y,z arrays, structure of arrays, or array of structures

            bool polyhedralCellsPresent_;                       //!< true if polyhedral cells are present in the grid. Set by a helper function/method (Dune::Grid in OPMN Flow case).
        public:
            // Storing the 'expected' name of the variables in the Damaris XML file. 
            // They should match what is in the Damaris XML file.
            // These names will be used for introspection on the data type that Damaris 
            // is expecting which is defined in the Layouts of the variables in the XML file
            DamarisVarBase* coordset_coords_values_x ;          //!< Seperate variable for each vertex axis; size = nvertices
            DamarisVarBase* coordset_coords_values_y ;          //!< Seperate variable for each vertex axis; size = nvertices
            DamarisVarBase* coordset_coords_values_z ;          //!< Seperate variable for each vertex axis; size = nvertices
            
            DamarisVarBase* coordset_coords_values_xyz_a_of_s ; //!< interleaved, array of structures: xyz,xyz,xyz,...   size = 3 x nvertices
            DamarisVarBase* coordset_coords_values_xyz_s_of_a ; //!< blocked, structure of arrays    : xxxx,yyyyy,zzzzz  size = 3 x nvertices
            
            DamarisVarBase* index_local_to_global ;             //!< This has size of ncells, will be used by the new HDF5 plugin 
                                                               //!< const std::vector<int>& local_to_global =       
                                                               //!<                           eclwriter->collectToIORank_.localIdxToGlobalIdxMapping();
                                                            
            DamarisVarBase* index_vertex_global_id ;            //!< to be required by the origina Damaris unstructured mesh <mesh type="unstructured" ...>  
                                                            
            
            DamarisVarBase* topologies_topo_elements_connectivity ;  //!< This has size of ncorners
            DamarisVarBase* topologies_topo_elements_offsets ;       //!< This has size of ncells
            DamarisVarBase* topologies_topo_elements_types ;         //!< This has size of ncells
            
            // Only needed if polyhedralCellsPresent_ == true
            DamarisVarBase* topologies_topo_subelements_faces ;      //!< This size needs to be computed - I think it is a set of indices into the vertices array
            DamarisVarBase* topologies_topo_subelements_offsets ;    //!< This has size of ncells
            
            std::vector<DamarisVarBase*> active_geom_data_ ;          //!< add each pointer to a vector so we an iterate over them easily - should use shared_ptr<>
            
              
            
            int nvertices_ ;
            int ncells_ ;
            int ncorners_ ;
            int subelements_faces_ ;  //!< This size needs to be computed - Only needed if polyhedralCellsPresent_ == true
        public:   
            DamarisGeometryData( vertex_data_structure vertex_structure, int rank=0 ) :
              vertex_structure_(vertex_structure),
              rank_(rank)
            {
                this->coordset_coords_values_x = nullptr ; 
                this->coordset_coords_values_y = nullptr;
                this->coordset_coords_values_z = nullptr;
               
                this->coordset_coords_values_xyz_a_of_s = nullptr; 
                this->coordset_coords_values_xyz_s_of_a = nullptr; 
                
                this->index_local_to_global = nullptr ;
                
                this->topologies_topo_elements_connectivity = nullptr;  
                this->topologies_topo_elements_offsets = nullptr;      
                this->topologies_topo_elements_types = nullptr;         
                
                // Only needed if polyhedralCellsPresent_ == true
                this->topologies_topo_subelements_faces = nullptr;    
                this->topologies_topo_subelements_offsets = nullptr; 

                nvertices_ = 0 ;
                ncells_    = 0 ;
                ncorners_  = 0 ;
            }
            
            DamarisGeometryData( vertex_data_structure vertex_structure, int nvertices,  int ncells, int ncorners, int rank=0 ) :
              vertex_structure_(vertex_structure),
              nvertices_(nvertices),
              ncells_(ncells),
              ncorners_(ncorners),
              rank_(rank)
            {
                this->coordset_coords_values_x = nullptr ;
                this->coordset_coords_values_y = nullptr;
                this->coordset_coords_values_z = nullptr;
               
                this->coordset_coords_values_xyz_a_of_s = nullptr;
                this->coordset_coords_values_xyz_s_of_a = nullptr;
                
                this->index_local_to_global = nullptr ;
                
                this->topologies_topo_elements_connectivity = nullptr;
                this->topologies_topo_elements_offsets = nullptr;
                this->topologies_topo_elements_types = nullptr;
                
                // Only needed if polyhedralCellsPresent_ == true
                this->topologies_topo_subelements_faces = nullptr;
                this->topologies_topo_subelements_offsets = nullptr;
            }
              
            ~DamarisGeometryData( void ) {
                if (this->coordset_coords_values_x != nullptr) delete coordset_coords_values_x ;
                if (this->coordset_coords_values_y != nullptr) delete coordset_coords_values_y ;
                if (this->coordset_coords_values_z != nullptr) delete coordset_coords_values_z ;
               
                if (this->coordset_coords_values_xyz_a_of_s != nullptr) delete coordset_coords_values_xyz_a_of_s ;
                if (this->coordset_coords_values_xyz_s_of_a != nullptr) delete coordset_coords_values_xyz_s_of_a ;
                
                if (this->index_local_to_global != nullptr)  delete this->index_local_to_global ;
                
                if (this->topologies_topo_elements_connectivity != nullptr) delete topologies_topo_elements_connectivity ;
                if (this->topologies_topo_elements_offsets != nullptr) delete topologies_topo_elements_offsets ;
                if (this->topologies_topo_elements_types != nullptr) delete topologies_topo_elements_types ;
                
                // Only needed if polyhedralCellsPresent_ == true
                if (this->topologies_topo_subelements_faces != nullptr) delete topologies_topo_subelements_faces ;
                if (this->topologies_topo_subelements_offsets != nullptr) delete topologies_topo_subelements_offsets ;
            }
            
            void PrintVarErrorStrings( void )
            {
                this->FillObjectPtrs() ;
                //this->active_geom_data_ 
                
                for ( auto& it : this->active_geom_data_) {
                    it->PrintError() ;
                }
                
                // .pop_back()->PrintError() ;
            }        

            void FillObjectPtrs( void )
            { 
                this->active_geom_data_.clear() ;
                
                if ( get_x() )
                   return( active_geom_data_.push_back( get_x() )) ;
                if ( get_y() )
                   return( active_geom_data_.push_back( get_y() )) ;
                if ( get_z() )
                    return( active_geom_data_.push_back( get_z( ) )) ;
                if ( get_xyz_a_of_s() )
                   return( active_geom_data_.push_back( get_xyz_a_of_s() )) ;
                if ( get_xyz_s_of_a() )
                   return( active_geom_data_.push_back( get_xyz_s_of_a() )) ;
               if ( get_index_local_to_global() )
                   return( active_geom_data_.push_back( get_index_local_to_global() )) ;  
                if ( get_connectivity() )
                   return( active_geom_data_.push_back( get_connectivity() )) ;
                if ( get_offsets() )
                   return( active_geom_data_.push_back( get_offsets() )) ;
                if ( get_types() )
                   return( active_geom_data_.push_back( get_types() )) ;
                if ( get_poly_faces() )
                   return( active_geom_data_.push_back( get_poly_faces() )) ;
                if ( get_poly_offsets() )
                   return( active_geom_data_.push_back( get_poly_offsets() )) ;
            }
            
            void   printGridDetails()
            {
              printNCells() ;
              printNVertices() ;
              printNCorners() ;
              // std::cout << "Mesh Type = " << getTypeString()  << std::endl ;
            }

            void printNCells()
            {
              std::cout << "ncells = " << ncells_ << std::endl ;
            }

            void printNVertices()
            {
              std::cout << "nvertices = " << nvertices_ << std::endl ;
            }

            void printNCorners()
            {
              std::cout << "ncorners = " << ncorners_ << std::endl ;
            }
            
            void printSubElemFaces()
            {
              std::cout << "subelements_faces = " << subelements_faces_ << std::endl ;
            }
                  
            std::string getMeshVertexTypeString( void ) 
            {
                if (vertex_structure_ == VERTEX_SEPARATE_X_Y_Z)
                    return ("VERTEX_SEPARATE_X_Y_Z") ;
                else if   (vertex_structure_ == VERTEX_INTERLEAVED_A_OF_S)
                     return ("VERTEX_INTERLEAVED_A_OF_S") ;
                else if   (vertex_structure_ == VERTEX_CONTIGUOUS_S_OF_A)
                     return ("VERTEX_CONTIGUOUS_S_OF_A") ;
                else //  (vertex_structure_ == VERTEX_INTERLEAVED_MIXED_A_OF_S)
                     return ("VERTEX_INTERLEAVED_MIXED_A_OF_S") ;
            }
            

            int getNCells()
            {
              return(ncells_) ;
            }

            int getNVertices()
            {
              return(nvertices_) ;
            }

            int getNCorners()
            {
              return(ncorners_) ;
            }
            
            int getNSubElemFaces()
            {
              return(subelements_faces_) ;
            }
            
            void setNCells(int ncells)
            {
              ncells_ = ncells ;
            }

            void setNVertices(int nverts)
            {
              nvertices_ = nverts ;
            }

            void setNCorners(int ncorners)
            {
              
               ncorners_ = ncorners ;
            }
            
            void setNSubElemFaces(int subelements_faces)
            {
              
               subelements_faces_ = subelements_faces ;
            }

            bool hasPolyhedralCells( void )
            {
              return (polyhedralCellsPresent_) ;
            }
            
            void set_hasPolyhedralCells( bool polyhedralCellsPresent  )
            {
              polyhedralCellsPresent_ = polyhedralCellsPresent ;
            }
            
            
            DAMARIS_TYPE_STR get_damaris_var_type( std::string& variable_name )
            {
                DAMARIS_TYPE_STR vartype ;
                int damaris_err = damaris_get_type(variable_name.c_str(), &vartype)  ;
                if (damaris_err != DAMARIS_OK) {
                   std::cerr << "ERROR rank =" << rank_ << " : DamarisGeometryData : damaris_get_type(\"" << variable_name << ".c_str(), &vartype)\" "<< std::endl ;
                   vartype = DAMARIS_TYPE_UNDEFINED ;
                }
                return (vartype) ;
            }
            
            DamarisVarBase* get_x( void ) {
                return (this->coordset_coords_values_x) ;
            }
            
            DamarisVarBase* get_y( void ) {
                return (this->coordset_coords_values_y) ;
            }
            
            DamarisVarBase* get_z( void ) {
                return (this->coordset_coords_values_z) ;
            }
            
            
            /**
            *  Getter methods to return the pointers to the objects
            */
            
            // interleaved, array of structures: xyz,xyz,xyz,...   size = 3 x nvertices
            DamarisVarBase* get_xyz_a_of_s( void )
            {
                return( coordset_coords_values_xyz_a_of_s  ) ;
            }
            // blocked, structure of arrays    : xxxx,yyyyy,zzzzz  size = 3 x nvertices            
            DamarisVarBase* get_xyz_s_of_a( void )
            {
                return( coordset_coords_values_xyz_s_of_a  ) ;
            }
            
            /**
            *  The global id's of the verticies
            */
            DamarisVarBase* get_index_local_to_global( void )
            {
                return( index_vertex_global_id ) ;
            }
            /** 
            *  Connectivity data
            */
            DamarisVarBase* get_connectivity( void )
            {
                return( topologies_topo_elements_connectivity ) ;
            }
            DamarisVarBase* get_offsets( void )
            {
                return( topologies_topo_elements_offsets ) ;
            }
            DamarisVarBase* get_types( void )
            {
                return( topologies_topo_elements_types ) ;
            }

            /** 
            *  Polyhedral Data
            */ 
            DamarisVarBase* get_poly_faces( void )
            {
                return( topologies_topo_subelements_faces ) ;
            }
            DamarisVarBase* get_poly_offsets( void )
            {
                return( topologies_topo_subelements_offsets ) ;
            }
            
            
            void SetAll_VERTEX_SEPARATE_X_Y_Z_shmem( std::vector<int>& paramSizeVal ) 
            {
                if (this->coordset_coords_values_x != nullptr ) 
                    this->coordset_coords_values_x->SetDamarisParameterAndShmem(  paramSizeVal ) ;
                else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_x is not set" << std::endl ; 
                if (this->coordset_coords_values_y != nullptr ) 
                    this->coordset_coords_values_y->SetDamarisParameterAndShmem(  paramSizeVal ) ;
                 else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_y is not set" << std::endl ; 
                if (this->coordset_coords_values_z != nullptr ) 
                    this->coordset_coords_values_z->SetDamarisParameterAndShmem(  paramSizeVal ) ;
                 else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_z is not set" << std::endl ; 
                
                // Errors may indicate set_damaris_var_name_vertex_x() and equivalents for y and z have not been called.
            }
            
            void CommitAll_VERTEX_SEPARATE_X_Y_Z_shmem(  ) 
            {
                if (this->coordset_coords_values_x != nullptr ) 
                    this->coordset_coords_values_x->CommitVariableDamarisShmem(   ) ;
                else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_x is not set" << std::endl ; 
                if (this->coordset_coords_values_y != nullptr ) 
                    this->coordset_coords_values_y->CommitVariableDamarisShmem(   ) ;
                 else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_y is not set" << std::endl ; 
                if (this->coordset_coords_values_z != nullptr ) 
                    this->coordset_coords_values_z->CommitVariableDamarisShmem(   ) ;
                 else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_z is not set" << std::endl ; 
                
                // Errors may indicate set_damaris_var_name_vertex_x() and equivalents for y and z have not been called.
            }
            
            void ClearAll_VERTEX_SEPARATE_X_Y_Z_shmem( ) 
            {
                if (this->coordset_coords_values_x != nullptr ) {
                    this->coordset_coords_values_x->ClearVariableDamarisShmem(   ) ;
                    this->coordset_coords_values_x = nullptr ;
                }
                else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_x is not set" << std::endl ; 
                if (this->coordset_coords_values_y != nullptr ) {
                    this->coordset_coords_values_y->ClearVariableDamarisShmem(   ) ;
                    this->coordset_coords_values_y = nullptr ;
                }
                 else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_y is not set" << std::endl ; 
                if (this->coordset_coords_values_z != nullptr ) {
                    this->coordset_coords_values_z->ClearVariableDamarisShmem(   ) ;
                    this->coordset_coords_values_z = nullptr ;
                }
                 else 
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::SetAll_VERTEX_SEPARATE_X_Y_Z_shmem() : coordset_coords_values_z is not set" << std::endl ; 
                
                // Errors may indicate set_damaris_var_name_vertex_x() and equivalents for y and z have not been called.
            }
            
            DamarisVarBase* ReturnDamarisVarofType(DAMARIS_TYPE_STR vartype,  int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                DamarisVarBase* retDamVar ;
                // DAMARIS_TYPE_STR vartype = get_damaris_var_type( variable_name );
                if (vartype == DAMARIS_TYPE_DOUBLE)
                {
                    retDamVar = new  DamarisVar<double>(dims, param_names, variable_name, this->rank_) ;
                } 
                else if (vartype == DAMARIS_TYPE_FLOAT) 
                {
                     retDamVar = new  DamarisVar<float>(dims, param_names, variable_name, this->rank_) ;
                } 
                else if (vartype == DAMARIS_TYPE_CHAR)
                {
                    retDamVar = new  DamarisVar<char>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_UCHAR)
                {
                    retDamVar = new  DamarisVar<unsigned char>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_SHORT)
                {
                    retDamVar = new  DamarisVar<short>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_USHORT)
                {
                    retDamVar = new  DamarisVar<unsigned short>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_INT)
                {
                    retDamVar = new  DamarisVar<int>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_UINT)
                {
                    retDamVar = new  DamarisVar<unsigned int>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_LONG)
                {
                    retDamVar = new  DamarisVar<long>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_ULONG)
                {
                    retDamVar = new  DamarisVar<unsigned long>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_UNDEFINED)
                {
                    std::cerr << "ERROR rank =" << rank_ << " : DamarisGeometryData:: ReturnDamarisVarofType()\"" << variable_name << " has type DAMARIS_TYPE_UNDEFINED"<< std::endl ;
                } else 
                {
                    std::cerr << "ERROR rank =" << rank_ << " : DamarisGeometryData:: ReturnDamarisVarofType()\"" << variable_name << " is not of available type "<< std::endl ;
                }
                return retDamVar ;
            }

            /** 
            * Used if we need to change the variable name as used in the Damaris XML file
            * The defaults are set in the DamarisVisDataWriter constructor.
            *
            *  Vertex data
            */
            void set_damaris_var_name_vertex_x( int dims, std::vector<std::string>& param_names, std::string& variable_name)
            {
                if (vertex_structure_ == VERTEX_SEPARATE_X_Y_Z) {
                    DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                    if ( vartype == DAMARIS_TYPE_DOUBLE )
                    {
                        this->coordset_coords_values_x = dynamic_cast<damaris::model::DamarisVar<double>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    } else if (vartype == DAMARIS_TYPE_FLOAT ) {
                        
                        this->coordset_coords_values_x = dynamic_cast<damaris::model::DamarisVar<float>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    }
                    else {
                       std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_x() : vertex data is not defined in Damaris XML file as float or double - other data types are not supported" << std::endl ;
                    }
                }
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_x() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_x is attempted to be set" << std::endl ; 
            }
            
            void set_damaris_var_name_vertex_y( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                if (vertex_structure_ == VERTEX_SEPARATE_X_Y_Z) {
                     DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                    if ( vartype == DAMARIS_TYPE_DOUBLE )
                    {
                        this->coordset_coords_values_y = dynamic_cast<damaris::model::DamarisVar<double>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    } else if (vartype == DAMARIS_TYPE_FLOAT ) {
                        
                        this->coordset_coords_values_y = dynamic_cast<damaris::model::DamarisVar<float>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    } 
                    else {
                       std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_y() : vertex data is not defined in Damaris XML file as float or double - other data types are not supported" << std::endl ;
                    }
                }
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_y() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_y is attempted to be set" << std::endl ; 
            }
            
            void set_damaris_var_name_vertex_z( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
              if (vertex_structure_ == VERTEX_SEPARATE_X_Y_Z) {
                     DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                    if ( vartype == DAMARIS_TYPE_DOUBLE )
                    {
                        this->coordset_coords_values_z = dynamic_cast<damaris::model::DamarisVar<double>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    } else if (vartype == DAMARIS_TYPE_FLOAT ) {
                        
                        this->coordset_coords_values_z = dynamic_cast<damaris::model::DamarisVar<float>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    } 
                    else {
                       std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_z() : vertex data is not defined in Damaris XML file as float or double - other data types are not supported" << std::endl ;
                    }
                }
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_z() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_z is attempted to be set" << std::endl ; 
            }
            
            // DamarisVar coordset_coords_values_xyz_a_of_s ; //!< interleaved, array of structures: xyz,xyz,xyz,...   size = 3 x nvertices
            // DamarisVar coordset_coords_values_xyz_s_of_a ; //!< blocked, structure of arrays    : xxxx,yyyyy,zzzzz  size = 3 x nvertices
            void set_damaris_var_name_vertex_xyz_a_of_s( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                if (vertex_structure_ == VERTEX_INTERLEAVED_A_OF_S) {                    
                    DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                    if ( vartype == DAMARIS_TYPE_DOUBLE )
                    {
                        this->coordset_coords_values_xyz_a_of_s = dynamic_cast<damaris::model::DamarisVar<double>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    } else if (vartype == DAMARIS_TYPE_FLOAT ) {
                        
                        this->coordset_coords_values_xyz_a_of_s = dynamic_cast<damaris::model::DamarisVar<float>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    }
                    else {
                       std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_xyz_a_of_s() : vertex data is not defined in Damaris XML file as float or double - other data types are not supported" << std::endl ;
                    } 
                } 
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_xyz_a_of_s() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_xyz_a_of_s is attempted to be set" << std::endl ; 
            }
            
            void set_coordset_coords_values_xyz_s_of_a( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                if (vertex_structure_ == VERTEX_CONTIGUOUS_S_OF_A) {                    
                    DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                    if ( vartype == DAMARIS_TYPE_DOUBLE )
                    {
                        this->coordset_coords_values_xyz_s_of_a = dynamic_cast<damaris::model::DamarisVar<double>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    } else if (vartype == DAMARIS_TYPE_FLOAT ) {
                        
                        this->coordset_coords_values_xyz_s_of_a = dynamic_cast<damaris::model::DamarisVar<float>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                    }
                    else {
                       std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_coordset_coords_values_xyz_s_of_a() : vertex data is not defined in Damaris XML file as float or double - other data types are not supported" << std::endl ;
                    } 
                }
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_coordset_coords_values_xyz_s_of_a() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_xyz_s_of_a is attempted to be set" << std::endl ; 
            }
            
            void set_index_local_to_global( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {               
                DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
 
                if ( vartype == DAMARIS_TYPE_INT ) {
                    this->index_local_to_global = dynamic_cast<damaris::model::DamarisVar<int>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } 
                else if ( vartype == DAMARIS_TYPE_UINT )
                {
                    this->index_local_to_global = dynamic_cast<damaris::model::DamarisVar<unsigned int>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } 
                else if ( vartype == DAMARIS_TYPE_LONG ) 
                {
                    this->index_local_to_global = dynamic_cast<damaris::model::DamarisVar<long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } 
                else if (vartype == DAMARIS_TYPE_ULONG )
                {
                    this->index_local_to_global = dynamic_cast<damaris::model::DamarisVar<unsigned long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                }  else {
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_index_local_to_global() : local to global index mapping data is not defined in Damaris XML file as int, unsigned int, long or unsigned long - other data types are not supported" << std::endl ;
                } 
            }  
            
            /** 
            * Used if we need to change the variable name as used in the Damaris XML file
            * The defaults are set in the DamarisVisDataWriter constructor.
            *
            *  Connectivity data
            */
            void set_damaris_var_name_connectivity( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                if ( vartype == DAMARIS_TYPE_INT ) {
                    this->topologies_topo_elements_connectivity = dynamic_cast<damaris::model::DamarisVar<int>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_UINT ) 
                {
                    this->topologies_topo_elements_connectivity = dynamic_cast<damaris::model::DamarisVar<unsigned int>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_LONG ) 
                {
                    this->topologies_topo_elements_connectivity = dynamic_cast<damaris::model::DamarisVar<long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if (vartype == DAMARIS_TYPE_ULONG ) 
                {
                    this->topologies_topo_elements_connectivity = dynamic_cast<damaris::model::DamarisVar<unsigned long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else  {
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_connectivity() : connectivity data is not defined in Damaris XML file as int, unsigned int, long or unsigned long - other data types are not supported" << std::endl ;
                }
            }
            
            void set_damaris_var_name_offsets( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                if ( vartype == DAMARIS_TYPE_INT ) {
                    this->topologies_topo_elements_offsets = dynamic_cast<damaris::model::DamarisVar<int>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_UINT ) 
                {
                    this->topologies_topo_elements_offsets = dynamic_cast<damaris::model::DamarisVar<unsigned int >* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_LONG ) 
                {
                    this->topologies_topo_elements_offsets = dynamic_cast<damaris::model::DamarisVar<long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if (vartype == DAMARIS_TYPE_ULONG ) 
                {
                    this->topologies_topo_elements_offsets = dynamic_cast<damaris::model::DamarisVar<unsigned long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else  {
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_offsets() : offset data is not defined in Damaris XML file as int, unsigned int, long or unsigned long - other data types are not supported" << std::endl ;
                }
            }
            
            void set_damaris_var_name_types( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                if ( vartype == DAMARIS_TYPE_CHAR ) {
                     this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<char>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_UCHAR ) 
                {
                    this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<unsigned char>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_SHORT ) 
                {
                    this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<short >* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_USHORT ) 
                {
                    this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<unsigned short>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                }
                else if ( vartype == DAMARIS_TYPE_INT ) 
                {
                    this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<int>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_UINT ) 
                {
                    this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<unsigned int >* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if ( vartype == DAMARIS_TYPE_LONG ) 
                {
                    this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else if (vartype == DAMARIS_TYPE_ULONG ) 
                {
                    this->topologies_topo_elements_types = dynamic_cast<damaris::model::DamarisVar<unsigned long>* > (ReturnDamarisVarofType(vartype, dims, param_names, variable_name)) ;
                } else {
                    std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_types() : type data is not defined in Damaris XML file as char, unsigned char, short, unsigned short, int, unsigned int, long or unsigned long - other data types are not supported" << std::endl ;
                }
                
            }

            /** 
            * Used if we need to change the variable name as used in the Damaris XML file
            * The defaults are set in the DamarisVisDataWriter constructor.
            *
            *  Polyhedral Data
            */ 
            void set_damaris_var_name_poly_faces( int dims, std::vector<std::string>& param_names, std::string& variable_name )
            {
                DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                if (vartype == DAMARIS_TYPE_INT )
                {
                    topologies_topo_subelements_faces = new  DamarisVar<int>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_UINT )
                {
                    topologies_topo_subelements_faces = new  DamarisVar<unsigned int>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_LONG )
                {
                    topologies_topo_subelements_faces = new  DamarisVar<long>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_ULONG )
                {
                    topologies_topo_subelements_faces = new  DamarisVar<unsigned long>(dims, param_names, variable_name, this->rank_) ;
                }
            }
            void set_damaris_var_name_poly_offsets(int dims, std::vector<std::string>& param_names, std::string& variable_name  )
            {
                DAMARIS_TYPE_STR vartype = get_damaris_var_type(variable_name) ;
                if (vartype == DAMARIS_TYPE_INT )
                {
                    topologies_topo_subelements_offsets = new  DamarisVar<int>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_UINT )
                {
                    topologies_topo_subelements_offsets = new  DamarisVar<unsigned int>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_LONG )
                {
                    topologies_topo_subelements_offsets = new  DamarisVar<long>(dims, param_names, variable_name, this->rank_) ;
                }
                else if (vartype == DAMARIS_TYPE_ULONG )
                {
                    topologies_topo_subelements_offsets = new  DamarisVar<unsigned long>(dims, param_names, variable_name, this->rank_) ;
                }
            }
            
            
            /** 
            * These versions are used if we are creating a DamarisVar of known template type.
            * The information about which can be used to write a Damaris XML variable entry
            * So, we just copy the pointer.
            *
            *  Vertex data
            */
            void set_damaris_var_name_vertex_x( DamarisVarBase* myDamarisVar)
            {
                if (vertex_structure_ == VERTEX_SEPARATE_X_Y_Z)
                    this->coordset_coords_values_x = myDamarisVar ;
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_x() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_x is attempted to be set" << std::endl ; 
            }
            void set_damaris_var_name_vertex_y( DamarisVarBase* myDamarisVar )
            {
              if (vertex_structure_ == VERTEX_SEPARATE_X_Y_Z)
                    this->coordset_coords_values_y = myDamarisVar ;
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_y() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_y is attempted to be set" << std::endl ; 
            }
            void set_damaris_var_name_vertex_z( DamarisVarBase* myDamarisVar )
            {
              if (vertex_structure_ == VERTEX_SEPARATE_X_Y_Z)
                    this->coordset_coords_values_z = myDamarisVar ;
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_z() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_z is attempted to be set" << std::endl ; 
            }
            
            // DamarisVar coordset_coords_values_xyz_a_of_s ; //!< interleaved, array of structures: xyz,xyz,xyz,...   size = 3 x nvertices
            // DamarisVar coordset_coords_values_xyz_s_of_a ; //!< blocked, structure of arrays    : xxxx,yyyyy,zzzzz  size = 3 x nvertices
            void set_damaris_var_name_vertex_xyz_a_of_s( DamarisVarBase* myDamarisVar )
            {
                if (vertex_structure_ == VERTEX_INTERLEAVED_A_OF_S)
                    this->coordset_coords_values_xyz_a_of_s = myDamarisVar ;
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_damaris_var_name_vertex_xyz_a_of_s() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_xyz_a_of_s is attempted to be set" << std::endl ; 
            }
            
            void set_coordset_coords_values_xyz_s_of_a( DamarisVarBase* myDamarisVar )
            {
                if (vertex_structure_ == VERTEX_CONTIGUOUS_S_OF_A)
                    this->coordset_coords_values_xyz_s_of_a = myDamarisVar ;
                else 
                   std::cerr  << "ERROR rank =" << rank_ << " : method DamarisGeometryData::set_coordset_coords_values_xyz_s_of_a() : Class was initialised as: " << getMeshVertexTypeString() << ", however coordset_coords_values_xyz_s_of_a is attempted to be set" << std::endl ; 
            }
            
            
            void set_index_local_to_global( DamarisVarBase* myDamarisVar )
            {
                this->index_local_to_global = myDamarisVar ; 
            }  
            
            /** 
            * Used if we need to change the variable name as used in the Damaris XML file
            * The defaults are set in the DamarisVisDataWriter constructor.
            *
            *  Connectivity data
            */
            void set_damaris_var_name_connectivity( DamarisVarBase* myDamarisVar )
            {
              topologies_topo_elements_connectivity = myDamarisVar ;
            }
            void set_damaris_var_name_offsets( DamarisVarBase* myDamarisVar )
            {
              topologies_topo_elements_offsets = myDamarisVar ;
            }
            void set_damaris_var_name_types( DamarisVarBase* myDamarisVar )
            {
              topologies_topo_elements_types = myDamarisVar ;
            }

            /** 
            * Used if we need to change the variable name as used in the Damaris XML file
            * The defaults are set in the DamarisVisDataWriter constructor.
            *
            *  Polyhedral Data
            */ 
            void set_damaris_var_name_poly_faces( DamarisVarBase* myDamarisVar )
            {
              topologies_topo_subelements_faces = myDamarisVar ;
            }
            void set_damaris_var_name_poly_offsets( DamarisVarBase* myDamarisVar )
            {
              topologies_topo_subelements_offsets = myDamarisVar ;
            }
            
  
    } ;
    

} // namespace model

} // namespace damaris

#endif 