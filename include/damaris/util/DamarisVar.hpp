#ifndef DAMARIS_MODEL_DATA_HPP
#define DAMARIS_MODEL_DATA_HPP

#include <string>
#include <iostream>
#include <sstream>
#include <cassert>
#include <typeinfo>

#include <Damaris.h>


/*
    File: DamarisVar.hpp
    Author: Joshua Bowden, Inria
    Date: 06/02/2023
    Modified: 12/08/2024
    The DamarisVar class can be used to allocate memory in the Damaris shared memory region and a user can supply
    the data required for the variable to pass to Damaris.
*/


namespace damaris
{
    namespace model
    {
        
    struct DamarisParameterXML {
            std::string param_name_ ;        //!< Reference string to the XML attribute layout being used to describe the shape of the variable. This is a required attribute.
            std::string param_datatype_ ;    //!< The data type of the varibale (C or Fortran types accepted - no unsigned variables available)
            std::string param_value_ ;  //!< The dimension of the local block of data. Required an entry for every dimension, can reference an XML "paramater" element (by name)
           
        public:
            DamarisParameterXML(){
                // Additional data needed to complete an XML <variable> element
                param_name_ = "" ;
                param_datatype_ = "" ;
                param_value_ = "" ; 
            }

            /**
            * Creates the XML representation of the variable from the available strings
            */
            std::string ReturnXMLForParameter ( void )
            {
                std::ostringstream  var_sstr ;

                var_sstr << "<parameter name=\"" << this->param_name_ << "\"" ;
                if (this->param_datatype_ != "") var_sstr  << " type=\"" << this->param_datatype_ << "\"" ;
                if (this->param_value_ != "") var_sstr  << " value=\"" << this->param_value_ << "\"" ;
                var_sstr  <<  "/>" ;

                return (var_sstr.str()) ;
            }
    } ;

    struct DamarisLayoutXML {
            std::string layout_name_ ;        //!< Reference string to the XML attribute layout being used to describe the shape of the variable. This is a required attribute.
            std::string layout_dimensions_ ;  //!< The dimension of the local block of data. Required an entry for every dimension, can reference an XML "paramater" element (by name)
            std::string layout_dims_global_ ; //!< The global size of a variable's blocks of data. This value is not required, but is useful for HDF5 Collective output. Requires and entry for each dimension.
            std::string layout_ghosts_ ;
			bool layout_visualizable_;
            std::string layout_comment_ ;
        protected:
            std::string layout_datatype_ ;    //!< The data type of the varibale (C or Fortran types accepted - no unsigned variables available)
            std::string layout_language_ ;

        public:
            DamarisLayoutXML(){
                // Additional data needed to complete an XML <variable> element
                layout_name_ = "" ;
                layout_datatype_ = "" ;
                layout_dimensions_ = "" ;  // This is probably not needed as vector data is defined using the Layout paramter. Could be useful for cross checking
                layout_dims_global_ = "#";
                layout_ghosts_ = "#" ;
                layout_language_ = "" ;
                layout_visualizable_ = true;
                layout_comment_  = "" ;
            }

            void set_language(std::string in_language){

                std::transform(in_language.begin(), in_language.end(), in_language.begin(), ::tolower);

                if (in_language == std::string("unknown") )
                    layout_language_ = in_language ;
                else if (in_language == std::string("fortran") )
                    layout_language_ = in_language ;
                else if (in_language == std::string("c") )
                    layout_language_ = in_language ;
                else if (in_language == std::string("cpp") )
                    layout_language_ = in_language ;
                else if (in_language == std::string("python") )
                    layout_language_ = in_language ;
                else if (in_language == std::string("r") )
                    layout_language_ = in_language ;
                else {
                    std::cerr << "ERROR: DamarisLayoutXML unrecogognized layout language: " << in_language << std::endl ;
                }
            }

            void set_datatype(std::string in_datatype ){

                std::transform(in_datatype.begin(), in_datatype.end(), in_datatype.begin(), ::tolower);

                if (in_datatype == std::string("string") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("short") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("int") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("integer") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("long") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("float") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("real") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("double") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("char") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("character") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("label") )
                    layout_datatype_ = in_datatype ;
                else if (in_datatype == std::string("undefined") )
                    layout_datatype_ = in_datatype ;
                else {
                    std::cerr << "ERROR: DamarisLayoutXML unrecogognized layout datatype: " << in_datatype << std::endl ;
                }
            }

            /**
            * Creates the XML representation of the variable from the available strings
            */
            std::string ReturnXMLForLayout ( void )
            {
                std::ostringstream  var_sstr ;

                // remove whitespace
                layout_name_.erase(std::remove_if(layout_name_.begin(), layout_name_.end(), ::isspace), layout_name_.end());

                if (this->layout_name_ != std::string("")) {
                    var_sstr << "<layout name=\"" << this->layout_name_ << "\"" ;
                    var_sstr  << " type=\"" << this->layout_datatype_ << "\"" ;
                    var_sstr  << " dimensions=\"" << this->layout_dimensions_ << "\"" ;
                    if (this->layout_dims_global_ != "") var_sstr  << " global=\"" << this->layout_dims_global_ << "\"" ;
                    if (this->layout_ghosts_ != "" && this->layout_ghosts_ != "#") var_sstr  << " ghosts=\"" << this->layout_ghosts_ << "\"" ;
                    if (this->layout_language_ != "") var_sstr  << " language=\"" << this->layout_language_ << "\"" ;
                    if (layout_visualizable_)
                        var_sstr  << " visualizable=\"true\"" ;
                    else
                        var_sstr  << " visualizable=\"false\"" ;
                    if (this->layout_comment_ != "") var_sstr  << " language=\"" << this->layout_comment_ << "\"" ;
                    var_sstr  <<  "/>" ;
                } else {
                    std::cerr << "ERROR: DamarisLayoutXML::ReturnXMLForLayout() this->layout_name_ is" ;
                    std::cerr <<  " not defined when converting to XML string: " << this->layout_name_ << std::endl ;
                    return ("") ;
                }

                return (var_sstr.str()) ;
            }
    } ;

    /**
    *  This class contains the extra elements that need to be part of a Damaris <variable> type.
    *  They are simple string values that may reference other XML elements (and could be checked for existence etc.)
    */
    struct DamarisVarXML {
            std::string var_name_ ;
            std::string layout_name_ ;    //!< Reference string to the XML attribute layout being used to describe the shape of the variable. This is a required attribute.
            std::string mesh_ ;           //!< Reference string to the XML attribute mesh element - the mesh is used to define the spatial layout of data and is used by visualization backends to generate 2D/3D model images
            bool visualizable_ ;   //!< Reference string to the XML attribute property that data can be sent to vis backends - "true" | "false"
            std::string unit_ ;           //!< Reference string to the XML attribute element denoting unit of the data
            bool time_varying_ ;   //!< Reference string to the XML attribute to indicate if data changes over iterations - "true" | "false"
            std::string store_ ;          //!< Reference string to the XML attribute to indicate if data should be passed to I/O store (e.g. to HDF5 plugin)
            std::string script_ ;         //!< Reference string to the XML attribute to indicate if data should be published as Python NumPy data
            std::string select_mem_ ;     //!< Reference string to the XML attribute select. The referenced variables data is used as indices to  select dat from memory to reorder output in the collective HDF5 data writer (Damaris version 1.8+)
            std::string select_file_ ;    //!< Reference string to the XML attribute select. The referenced variables data is used as indices to select positions in the dataset file to reorder output in the collective HDF5 data writer (Damaris version 1.8+)
            std::string select_subset_ ;  //!< Reference string to the XML attribute select. Used to specify the output dataset shape and how much data each rank contributes to it and the global offsets to the ranks data (Damaris version 1.8+)
            std::string ref_ ;
            bool enabled_ ;
            std::string comment_ ;
        protected:
            std::string centering_ ;      //!< Reference string to the XML attribute to indicate where data aligns on a mesh - "zonal" | "nodal"
            std::string type_ ;           //!< Reference string to the XML attribute type of data - "scalar" or "vector" (others tensor maybe). TODO: check if this attribute is used by the Damaris library anywhere.

        public:
            DamarisVarXML(){
                // Additional data needed to complete an XML <variable> element
                layout_name_ = "#" ;
                mesh_ = "#" ;
                type_ = "scalar" ;  // This is probably not needed as vector data is defined using the Layout paramter. Could be useful for cross checking
                visualizable_ = false ;
                unit_ = "#" ;
                time_varying_ = true ;
                enabled_ = true ;
                centering_ = "zonal" ;
                store_ = "#" ;
                script_ = "#" ;
                select_mem_ = "#" ;
                ref_ == "#" ;
            }

            void set_type(std::string in_type){

                std::transform(in_type.begin(), in_type.end(), in_type.begin(), ::tolower);

                if (in_type == std::string("scalar") )
                    type_ = in_type ;
                else if (in_type == std::string("vector") )
                    type_ = in_type ;
                else if (in_type == std::string("tensor") )
                    type_ = in_type ;
                else if (in_type == std::string("symmetric-tensor") )
                    type_ = in_type ;
                else if (in_type == std::string("material") )
                    type_ = in_type ;
                else if (in_type == std::string("matspecies") )
                    type_ = in_type ;
                else if (in_type == std::string("label") )
                    type_ = in_type ;
                else if (in_type == std::string("array") )
                    type_ = in_type ;
                else if (in_type == std::string("mesh") )
                    type_ = in_type ;
                else if (in_type == std::string("curve") )
                    type_ = in_type ;
                else {
                    std::cerr << "ERROR: DamarisVarXML unrecogognized variable type: " << in_type << std::endl ;
                }
            }

            void set_centering(std::string in_centering){

                std::transform(in_centering.begin(), in_centering.end(), in_centering.begin(), ::tolower);

                if (in_centering == std::string("nodal") )
                    centering_ = in_centering ;
                else if (in_centering == std::string("zonal") )
                    centering_ = in_centering ;
                else {
                    std::cerr << "ERROR: DamarisVarXML unrecogognized centering: " << in_centering << std::endl ;
                }
            }

            /**
            * Creates the XML representation of the variable from the available strings
            */
            std::string ReturnXMLForVariable ( void )
            {
                std::ostringstream  var_sstr ;

                layout_name_.erase(std::remove_if(layout_name_.begin(), layout_name_.end(), ::isspace), layout_name_.end());

                if (this->var_name_ != std::string("")) {
                    var_sstr << "<variable name=\"" << this->var_name_  << "\"" ;
                    var_sstr << " layout=\"" << this->layout_name_ << "\"" ;
                    if (this->mesh_ != "") var_sstr  << " mesh=\"" << this->mesh_ << "\"" ;
                    if (this->type_ != "") var_sstr  << " type=\"" << this->type_ << "\"" ;
                    if (visualizable_)
                        var_sstr  << " visualizable=\"true\"" ;
                    else
                        var_sstr  << " visualizable=\"false\"" ;
                    if (this->unit_ != "") var_sstr  << " unit=\"" << this->unit_ << "\"" ;
                    if (time_varying_)
                        var_sstr  << " time-varying=\"true\"" ;
                    else
                        var_sstr  << " time-varying=\"false\"" ;
                    if (enabled_)
                        var_sstr  << " enabled=\"true\"" ;
                    else
                        var_sstr  << " enabled=\"false\"" ;
                    if (this->centering_ != "") var_sstr  << " centering=\"" << this->centering_ << "\"" ;
                    if (this->store_ != "") var_sstr  << " store=\"" << this->store_ << "\"" ;
                    if (this->script_ != "") var_sstr  << " script=\"" << this->script_  << "\"" ;
                    if (this->select_mem_ != "") var_sstr  << " select-mem=\"" << this->select_mem_ << "\"" ;
                    if (this->select_file_ != "") var_sstr  << " select-file=\"" << this->select_file_ << "\"" ;
                    if (this->select_subset_ != "") var_sstr  << " select-subset=\"" << this->select_subset_ << "\"" ;
                    var_sstr << "/>" ;
                } else {
                    std::cerr << "ERROR: DamarisVarXML::ReturnXMLForVariable() this->var_name_ is not defined" ;
                    std::cerr << " when converting to XML string. " << std::endl ;
                    return ("") ;
                }

                return (var_sstr.str()) ;
            }
    } ;


    /**
      *  This class contains a coordinate of a mesh/curve.
      */
    struct DamarisCoordXML {
        std::string coord_name_ ;
        std::string unit_ ;
        std::string label_ ;
        std::string comment_ ;

    public:
        DamarisCoordXML(){
            coord_name_ = "#" ;
            unit_ = "#" ;
            label_ = "#" ;
        }

        /**
        * Creates the XML representation of the coord
        */
        std::string ReturnXMLForCoord ( void )
        {
            std::ostringstream  var_sstr ;

            var_sstr << "<coord name=\"" << this->coord_name_ << "\"" ;
            if (this->unit_ != "") var_sstr  << " unit=\"" << this->unit_ << "\"" ;
            if (this->label_ != "") var_sstr  << " label=\"" << this->label_ << "\"" ;
            if (this->comment_ != "") var_sstr  << " comment=\"" << this->comment_ << "\"" ;
            var_sstr  <<  "/>" ;

            return (var_sstr.str()) ;
        }
    } ;

    /**
    *  This class contains the extra elements that need to be part of a Damaris <mesh> type.
    */
    class DamarisMeshXML {
            std::string mesh_name_ ;
            std::string mesh_type_ ;
            int topology_ ;
            bool time_varying_ ;
            std::string comment_ ;

        protected:
            std::vector<DamarisCoordXML> coords {} ;

        public:
            DamarisMeshXML(){
                // Additional data needed to complete an XML <mesh> element
                mesh_name_ = "#" ;
                mesh_type_ = "scalar" ;
                topology_ = 1 ;
                time_varying_ = false ;
                comment_ = "";
            }

            void set_name(std::string in_name){ mesh_name_ = in_name;}
            std::string get_name() const{ return  mesh_name_ ;}
            std::string get_type() const{ return  mesh_type_ ;}
            void set_type(std::string in_type){

                if (in_type == std::string("rectilinear")
                         || in_type == std::string("curvilinear")
                         || in_type == std::string("unstructured")
                         || in_type == std::string("point")
                         || in_type == std::string("csg")
                         || in_type == std::string("amr") )
                    mesh_type_ = in_type ;
                else {
                    std::cerr << "ERROR: DamarisMeshXML unrecogognized mesh type: " << in_type << std::endl ;
                }
            }

            void set_topology(int in_topology){ topology_ = in_topology;}
            int  get_topology() const{ return  topology_ ;}

            void set_time_varying(bool in_time_varying){ time_varying_ = in_time_varying;}
            bool get_time_varying() const{ return time_varying_ ;}

            void set_comment(std::string in_comment){ comment_ = in_comment;}
            std::string get_comment() const{ return  comment_ ;}

            void add_coord (std::string coord_name_, std::string unit_, std::string label_, std::string comment_){
                    DamarisCoordXML coord{} ;
                    coord.coord_name_ = coord_name_;
                    if (unit_ != "") coord.unit_ = unit_;
                    if (label_ != "") coord.label_ = label_;
                    if (comment_ != "") coord.comment_ = comment_;
                    coords.emplace_back(coord) ;
            }

            /**
            * Creates the XML representation of the mesh from the available strings
            */
            std::string ReturnXMLForMesh ( void )
            {
                std::ostringstream  var_sstr ;

                if (this->mesh_name_ != std::string("")) {
                    var_sstr << "<mesh name=\"" << this->mesh_name_  << "\"" ;
                    if (this->mesh_type_ != "") var_sstr  << " type=\"" << this->mesh_type_ << "\"" ;
                    if (this->topology_) var_sstr  << " topology=\"" << this->topology_ << "\"" ;
                    if (this->time_varying_)
                        var_sstr  << " time-varying=\"true\"" ;
                    else
                        var_sstr  << " time-varying=\"false\"" ;

                    var_sstr  <<  ">" ;

                    for (int coord_id = 0; coord_id < this->topology_; coord_id++) {
                        var_sstr << this->coords[coord_id].ReturnXMLForCoord() ;
                    }

                    var_sstr << "</mesh>" ;
                } else {
                    std::cerr << "ERROR: DamarisMeshXML::ReturnXMLForMesh() this->mesh_name_ is not defined" ;
                    std::cerr << " when converting to XML string. " << std::endl ;
                    return ("") ;
                }

                return (var_sstr.str()) ;
            }
    } ;

    /**
    *  This class contains the extra elements that need to be part of a Damaris <mesh> type.
    */
    class DamarisCurveXML {
            std::string curve_name_ ;

        protected:
            std::vector<DamarisCoordXML> coords {} ;

        public:
            DamarisCurveXML(){
                // Additional data needed to complete an XML <mesh> element
                curve_name_ = "#" ;
            }

            void set_name(std::string in_name){ curve_name_ = in_name;}
            std::string get_name() const{ return  curve_name_ ;}

            void add_coord (std::string coord_name_, std::string unit_, std::string label_, std::string comment_){
                    DamarisCoordXML coord{} ;
                    coord.coord_name_ = coord_name_;
                    if (unit_ != "") coord.unit_ = unit_;
                    if (label_ != "") coord.label_ = label_;
                    if (comment_ != "") coord.comment_ = comment_;
                    coords.emplace_back(coord) ;
            }

            /**
            * Creates the XML representation of the mesh from the available strings
            */
            std::string ReturnXMLForCurve ( void )
            {
                std::ostringstream  var_sstr ;

                if (this->curve_name_ != std::string("")) {
                    var_sstr << "<curve name=\"" << this->curve_name_  << "\"" ;
                    var_sstr  <<  ">" ;

                    for (int coord_id = 0; coord_id < this->coords.size(); coord_id++) {
                        var_sstr << this->coords[coord_id].ReturnXMLForCoord() ;
                    }

                    var_sstr << "</curve>" ;
                } else {
                    std::cerr << "ERROR: DamarisCurveXML::ReturnXMLForCurve() this->curve_name_ is not defined" ;
                    std::cerr << " when converting to XML string. " << std::endl ;
                    return ("") ;
                }

                return (var_sstr.str()) ;
            }
    } ;

    /**
    *  This class contains the extra elements that need to be part of a Damaris <group> type.
	*  	A group can contain layout, variable, mesh, mesh [, curve], or another group
    */
    class DamarisGroupXML {
        private:
            std::string group_name_ ;
            std::string group_enabled_ = "true" ;
            std::string group_store_ = "#" ;

            std::string parent_group_name_ = "#" ;

            std::vector<DamarisVarXML> variables {} ;
            std::vector<DamarisLayoutXML> layouts {} ;
            std::vector<DamarisMeshXML> meshes {} ;
            std::vector<DamarisCurveXML> curves {} ;
            std::vector<DamarisGroupXML> groups {} ;

        public:

            DamarisGroupXML(std::string group_name){ group_name_ = group_name ; }
            DamarisGroupXML(std::string group_name, std::string group_enabled)
            {
              group_name_ = group_name ;
              group_enabled_ = group_enabled;
            }
            DamarisGroupXML(std::string group_name, std::string group_enabled, std::string group_store)
            {
              group_name_ = group_name ;
              group_enabled_ = group_enabled;
              group_store_ = group_store;
            }

            void set_name(std::string in_name){ group_name_ = in_name;}
            std::string get_name() const{ return  group_name_ ;}

            void add_variable (DamarisVarXML variable){
                variables.emplace_back(variable) ;
            }
            void add_ds_element (DamarisVarXML variable){
                add_variable(variable) ;
            }
            std::vector<DamarisVarXML> get_variables (){ return variables ; }

            void add_layout (DamarisLayoutXML layout){
                layouts.emplace_back(layout) ;
            }
            void add_ds_element (DamarisLayoutXML layout){
                add_layout(layout) ;
            }
            std::vector<DamarisLayoutXML> get_layouts (){ return layouts ; }

            void add_mesh (DamarisMeshXML mesh){
                meshes.emplace_back(mesh) ;
            }
            void add_ds_element (DamarisMeshXML mesh){
                add_mesh(mesh) ;
            }
            std::vector<DamarisMeshXML> get_meshes (){ return meshes ; }

            void add_curve (DamarisCurveXML curve){
                curves.emplace_back(curve) ;
            }
            void add_ds_element (DamarisCurveXML curve){
                add_curve(curve) ;
            }
            std::vector<DamarisCurveXML> get_curves (){ return curves ; }

            void add_sub_group (DamarisGroupXML group){
                groups.emplace_back(group) ;
            }
            std::vector<DamarisGroupXML> get_sub_groups (){ return groups ; }

            /**
            * Creates the XML representation of the mesh from the available strings
            */
            std::string ReturnXMLForGroup ( void )
            {
                std::ostringstream  var_sstr ;

                if (this->group_name_ != std::string("")) {
                    var_sstr << "<group name=\"" << this->group_name_  << "\"" ;
                    var_sstr  <<  ">" ;
					//Append sub-groups XML representations
                    for (int group_id = 0; group_id < this->groups.size(); group_id++) {
                        var_sstr << this->groups[group_id].ReturnXMLForGroup() ;
                    }
					//Append variables XML representations
                    for (int variable_id = 0; variable_id < this->variables.size(); variable_id++) {
                        var_sstr << this->variables[variable_id].ReturnXMLForVariable() ;
                    }
					//Append layouts XML representations
                    for (int layout_id = 0; layout_id < this->layouts.size(); layout_id++) {
                        var_sstr << this->layouts[layout_id].ReturnXMLForLayout() ;
                    }
					//Append meshes XML representations
                    for (int mesh_id = 0; mesh_id < this->meshes.size(); mesh_id++) {
                        var_sstr << this->meshes[mesh_id].ReturnXMLForMesh() ;
                    }
					//Append curves XML representations
                    for (int curve_id = 0; curve_id < this->curves.size(); curve_id++) {
                        var_sstr << this->curves[curve_id].ReturnXMLForCurve() ;
                    }

                    var_sstr << "</group>" ;
                } else {
                    std::cerr << "ERROR: DamarisGroupXML::ReturnXMLForGroup() this->group_name_ is not defined" ;
                    std::cerr << " when converting to XML string. " << std::endl ;
                    return ("") ;
                }

                return (var_sstr.str()) ;
            }
    } ;

    /**
    *  This class contains the extra elements that need to be part of a Damaris <store> type.
    */
    struct DamarisStoreXML {
            std::string store_name_ ;        //!< Reference string to the XML attribute store being used to describe the storage of the variable. This is a required attribute.
            std::string store_type_ ;    //!< The data type of the varibale (HDF5...)

            //Option
            std::string store_opt_FileMode_ ;
            std::string store_opt_XDMFMode_ ;
            std::string store_opt_FilesPath_ ;

        public:
            DamarisStoreXML(){
                // Additional data needed to complete an XML <variable> element
                store_name_ = "" ;
                store_type_ = "HDF5" ;

                store_opt_FileMode_ = "Collective" ;
                store_opt_XDMFMode_ = "NoIteration" ;
                store_opt_FilesPath_ = "" ;
            }

            /**
            * Creates the XML representation of the variable from the available strings
            */
            std::string ReturnXMLForStore ( void )
            {
                std::ostringstream  var_sstr ;

                var_sstr << "<store name=\"" << this->store_name_ << "\"" ;
                if (this->store_type_ != "") var_sstr  << " type=\"" << this->store_type_ << "\"" ;
                var_sstr  <<  ">" ;
                if (this->store_opt_FileMode_ != "") var_sstr  <<  "  <option key=\"FileMode\">" << this->store_opt_FileMode_ << "</option>" ;
                if (this->store_opt_XDMFMode_ != "") var_sstr  <<  "  <option key=\"XDMFMode\">" << this->store_opt_XDMFMode_ << "</option>" ;
                //if (this->store_opt_FilesPath_ != "")
                                                     var_sstr  <<  "  <option key=\"FilesPath\">" << this->store_opt_FilesPath_ << "</option>" ;
                var_sstr  <<  "</store>" ;

                return (var_sstr.str()) ;
            }
    } ;

    /**
    *  This class contains the extra elements that need to be part of a Damaris <paraview> type.
    */
    class DamarisParaviewXML {
            int update_frequency_ = -1; //update-frequency: int [1]
            float realtime_timestep_ = -1;//realtime-timestep: float [0.1]
            int end_iteration_  = -1;//end-iteration int [0]
            int write_vtk_  = -1;//write-vtk int [0]
            bool write_vtk_binary_  = true;//write-vtk-binary bool [true]
            std::string comment_ ;

        protected:
            std::vector<std::string> scripts {} ;

        public:
            DamarisParaviewXML(){
            }

            void set_update_frequency(int in_update_frequency){ update_frequency_ = in_update_frequency;}
            int  get_update_frequency() const{ return  update_frequency_ ;}

            void set_realtime_timestep(float in_realtime_timestep){ realtime_timestep_ = in_realtime_timestep;}
            float  get_realtime_timestep() const{ return  realtime_timestep_ ;}

            void set_end_iteration(int in_end_iteration){ end_iteration_ = in_end_iteration;}
            int  get_end_iteration() const{ return  end_iteration_ ;}

            void set_write_vtk(int in_write_vtk){ write_vtk_ = in_write_vtk;}
            int  get_write_vtk() const{ return  write_vtk_ ;}

            void set_write_vtk_binary(bool in_write_vtk_binary){ write_vtk_binary_ = in_write_vtk_binary;}
            bool  get_write_vtk_binary() const{ return  write_vtk_binary_ ;}

            void set_comment(std::string in_comment){ comment_ = in_comment;}
            std::string get_comment() const{ return  comment_ ;}

            void add_script (std::string script){
                    scripts.emplace_back(script) ;
            }

            /**
            * Creates the XML representation of the mesh from the available strings
            */
            std::string ReturnXMLForParaview ( void )
            {
                std::ostringstream  var_sstr ;

                if (this->scripts.size()) {
                    var_sstr << "<paraview" ;
                    if (this->update_frequency_ >= 0) var_sstr  << " update-frequency=\"" << this->update_frequency_ << "\"" ;
                    if (this->realtime_timestep_ >= 0) var_sstr  << " realtime-timestep=\"" << this->realtime_timestep_ << "\"" ;
                    if (this->end_iteration_ >= 0) var_sstr  << " end-iteration=\"" << this->end_iteration_ << "\"" ;
                    if (this->write_vtk_ >= 0) var_sstr  << " write-vtk=\"" << this->write_vtk_ << "\"" ;
                    if (this->write_vtk_binary_)
                        var_sstr  << " write-vtk-binary=\"true\"" ;
                    else
                        var_sstr  << " write-vtk-binary=\"false\"" ;

                    var_sstr  <<  ">" ;

                    for (int script_id = 0; script_id < this->scripts.size(); script_id++) {
                        var_sstr << "<script>" << this->scripts[script_id] << "</script>" ;
                    }

                    var_sstr << "</paraview>" ;
                } else {
                    std::cerr << "ERROR: DamarisParaviewXML::ReturnXMLForParaview() no script is defined" ;
                    std::cerr << " when converting to XML string. " << std::endl ;
                    return ("") ;
                }

                return (var_sstr.str()) ;
            }
    } ;// class DamarisParaviewXML

    /**
    *  This class contains the extra elements that need to be part of a Damaris <paraview> type.
    */
    struct DamarisPyScriptXML {
            std::string pyscript_name_ = "";//name
            std::string pyscript_file_ = "";//file
            std::string execution_ = "remote";//execution: [remote]
            std::string language_ = "python";//language: [python]
            std::string scope_  = "core";//scope [core]
            bool external_  = false;//external [false]
            int frequency_  = 1;//frequency [1]
            std::string scheduler_file_ = "";//scheduler_file
            int nthreads_  = 1;//nthreads [1]
            int timeout_  = 2;//timeout [2]
            std::string keep_workers_ = "no" ; //keep-workers
            std::string comment_ ;

        public:
            DamarisPyScriptXML(){
            }
            DamarisPyScriptXML(std::string in_pyscript_name){
              pyscript_name_ = in_pyscript_name ;
            }

            /**
            * Creates the XML representation of the mesh from the available strings
            */
            std::string ReturnXMLForPyScript ( void )
            {
                std::ostringstream  var_sstr ;

                if (this->pyscript_name_ != "" && this->pyscript_file_ != "") {
                    var_sstr << "<pyscript" ;
                    var_sstr  << " name=\"" << this->pyscript_name_ << "\"" ;
                    var_sstr  << " file=\"" << this->pyscript_file_ << "\"" ;
                    if (this->execution_ != "") var_sstr  << " execution=\"" << this->execution_ << "\"" ;
                    var_sstr  << " language=\"" << this->language_ << "\"" ;
                    if (this->scope_ != "") var_sstr  << " scope=\"" << this->scope_ << "\"" ;
                    if (this->external_)
                        var_sstr  << " external=\"true\"" ;
                    else
                        var_sstr  << " external=\"false\"" ;
                    if (this->frequency_ >= 0) var_sstr  << " frequency=\"" << this->frequency_ << "\"" ;
                    if (this->scheduler_file_ != "") var_sstr  << " scheduler-file=\"" << this->scheduler_file_ << "\"" ;
                    if (this->nthreads_ >= 0) var_sstr  << " nthreads=\"" << this->nthreads_ << "\"" ;
                    if (this->timeout_ >= 0) var_sstr  << " timeout=\"" << this->timeout_ << "\"" ;
                    if (this->keep_workers_ != "") var_sstr  << " keep-workers=\"" << this->keep_workers_ << "\"" ;
                    if (this->comment_ != "") var_sstr  << " comment=\"" << this->comment_ << "\"" ;

                    var_sstr << " />" ;
                } else {
                    std::cerr << "ERROR: DamarisPyScriptXML::ReturnXMLForPyScript() this->pyscript_name_ or this->pyscript_file_ is not defined" ;
                    std::cerr << " when converting to XML string. " << std::endl ;
                    return ("") ;
                }

                return (var_sstr.str()) ;
            }
    } ;// class DamarisPyScriptXML


    class DamarisVarBase { 
        public:
            virtual ~DamarisVarBase( void )  {}  ;
            virtual void PrintError ( void ) = 0  ;
            virtual bool HasError( void ) = 0  ;
            virtual void SetDamarisParameterAndShmem( std::vector<int> paramSizeVal )  = 0  ;
            virtual void SetDamarisParameter( std::vector<int>& paramSizeVal )  = 0  ;
            virtual void SetDamarisPosition(  std::vector<int64_t> positionsVals ) = 0  ;
            virtual void SetPointersToDamarisShmem( void ) = 0  ;
            virtual void CommitVariableDamarisShmem( void ) = 0 ;
            virtual void ClearVariableDamarisShmem( void ) = 0  ;
            virtual std::string & variable_name( void ) = 0  ;
    };  // class DamarisVarBase

     /**
     *  class to store a Damaris variable representation for the XML file
     *  (can be used with \ref class DamarisKeywords).
     *
     *  It is thought that the details stored in the object can be used to pass
     *  into an XML generation function e.g. DamarisKeywords
     *
     */
    template <typename T>
    class DamarisVar : public DamarisVarBase 
    {
            int   dims_ ;
            int   num_params_ ;       //!< Each paramater name string will need a value and they are set in SetDamarisParameter()
            int  * param_sizes_ ;     //!< The value for any paramaters that are being used to describe the size of the variables data array
            int64_t * positions_ ;    //!< The offsets into the array that the data in the Variable starts from for this rank.
            int   rank_ ;             //!< Rank of process - used for error reporting.
            bool  paramaters_set_ ;   //!< set to true after SetDamarisParameter() is call to ensure the variable has correct 
                                      //!< size for memory allocation in SetPointersToDamarisShmem()

            std::vector<std::string> param_names_ ;  //!< Contains one paramater name for each paramater that a variable depends on (via it's Layout)
            std::string variable_name_ ;         //!< Reference string to the XML attribute name of the variable.

            int dam_err_ ;                       //!<  Set to != DAMARIS_OK if a Daamris error was returned by a Damaris API function call
            bool has_error_ ;
            std::ostringstream  dam_err_sstr_ ;  //!< Use dam_err_sstr.str() to return an error string describing detected error

            DamarisVarXML xml_attributes_ ; //!< The extra elements that need to be part of a Damaris <variable> type. They are simple string values 
                                                      //!< that may reference other XML elements (and could be checked for existence etc.)

            T * data_ptr_ ;                      //!< This pointer will be mapped to the Damaris shared memory area for the variable in the SetPointersToDamarisShmem() 
                                                 //!< method. The type T will match the Layout type

        public:

            /**
            * Constructor - sets private data values and dos not initialise the shared memory area.
            *
            * Two usages:
            *         Example XML definition:
            *          <parameter name="my_param_name1"     type="int" value="1" />
            *          <parameter name="my_param_name2"     type="int" value="1" />
            *          <layout    name="my_layout"    type="int" dimensions="my_param_name1,my_param_name2"   comment="This is a 2D variable"  />
            *          <variable name="MYVARNAME"  layout="my_layout"  visualizable="true"/>
            *
            * 1/ The variable's layout needs to be initialised via parameters :
            *          // Create the DamarisVar object:
            *          damaris::model::DamarisVar<int>  MYVARNAME_2d = new damaris::model::DamarisVar<int>(2, 
                                                                                            {std::string("my_param_name1"),std::string("my_param_name2")}, 
            *                                                                                std::string("MYVARNAME"), rank_) ;
            *           // Set the paramter sizes
            *          MYVARNAME_2d->SetDamarisParameterAndShmem( {25, 100 } } ;  // sets the parameters  (here, my_param_name1 == 25 and my_param_name2 == 100)
            *          // Get a pointer to the memeory and use it
            *          T * mymemory = MYVARNAME_2d->data_ptr() ;
            *          ... write data to mymemory ....
            *          delete MYVARNAME_2d ;
            * or,
            *  2/  The variable's layout has been initialised via parameters in another variable 
            *      (i.e. "my_param_name1" and "my_param_name2"  have been previously set in the code)
            *          // Create the DamarisVar object:
            *          damaris::model::DamarisVar<int>  MYVARNAME_2d = new damaris::model::DamarisVar<int>(2, {std::string("my_param_name1"), std::string("my_param_name2")}, 
            *                                                                                                                                 std::string("MYVARNAME"), rank_) ;
            *          // explicitly state that the paramater values have been set somewhere else in the code previously.
            *          MYVARNAME_2d->ParameterIsSet() ;
            *          MYVARNAME_2d->SetPointersToDamarisShmem() 
            *          // Get a pointer to the memeory and use it
            *          T * mymemory = MYVARNAME_2d->data_ptr() ;
            *          ... write data to mymemory ....
            *          delete MYVARNAME_2d ;
            * 
            *  /param [IN] dims           Used to check that the inputs to SetDamarisPosition() have the same number of values - one value for each dimension
            *  /param [IN] param_names    The name the Damaris paramaters. These names (in typical use) control a Damaris variables size (names are defined in the Damaris XML file).
            *  /param [IN] variable_name  The name of the Damaris variable (defined in the Dmaaris XML file)
            *  /param [IN] rank           The rank of the process. Used for error output.
            */
            DamarisVar(int dims, 
                       std::vector<std::string> param_names, 
                       std::string variable_name, 
                       int rank, 
                       bool test_type=true) 
            : dims_( dims ),
              param_names_( param_names ),
              variable_name_( variable_name ),
              rank_(rank)
            {
                dam_err_ = DAMARIS_OK ;
                
                assert( param_names_.size() == dims ) ;
                assert( dims > 0 ) ;
                
                // Check that our template type T matches out Damaris XML <layout> type
                if (test_type ) {
                    if ( !TestType(variable_name) ) {
                        std::exit(-1) ;
                    }
                }

                num_params_ = param_names_.size() ; 
                param_sizes_ = new int(num_params_) ;
                positions_ = new int64_t(dims) ;

                data_ptr_ = nullptr ;
                paramaters_set_ = false ;
                has_error_ = false ;
            }

           /**
            *  Constructor - Sets private data values and also initialises the Damaris shared memory area for writing (and
            * reading) by specifying the values for the variables parameters . i.e. makes the data() pointer available and
            * sets the size of the memory block it points to.
            *
            *  N.B. These objects need a matching <variable ...> and <paramater ...> entries in the Damaris XML file
            *
            *  Example use:
            *         Example XML definition:
            *          <parameter name="my_param_name1"     type="int" value="1" />
            *          <parameter name="my_param_name2"     type="int" value="1" />
            *          <layout    name="my_layout"    type="int" dimensions="my_param_name1,my_param_name2"   comment="This
            * is a 2D variable"  /> <variable name="MYVARNAME"  layout="my_layout"  visualizable="true"/>
            *  // The paramaters are intialized in the constructor code
            *  damaris::model::DamarisVar<int>  MYVARNAME_2d(2,{std::string("my_param_name1"),
            * std::string("my_param_name2")}, {100, 25}, std::string("MYVARNAME"), rank_); T * mymemory =
            * MYVARNAME_2d.data();
            *  ... write data to mymemory ....
            *   // Damaris shared memory is tidied up when object MYVARNAME_2d is out of scope.
            *
            *  /param [IN] dims           Used to check that the inputs to SetDamarisPosition() have
            *                             the same number of values - one value for each dimension
            *  /param [IN] param_names    The name the Damaris paramaters. These names (in typical use)
            *                             control a Damaris variables size (names are defined in the Damaris XML file).
            *  /param [IN] param_values   The values of the paramaters - this defines how much memory we will
            *                             have access to in the shared memory area (on the current and ongoing iterations,
            *                             until later modified to new values)
            *  /param [IN] variable_name  The name of the Damaris variable (defined in the Damaris XML file)
            *  /param [IN] rank           The rank of the process. Used for error output.
            */ 
            DamarisVar(int dims, 
                       std::vector<std::string> param_names, 
                       std::vector<int> param_values, 
                       std::string variable_name, 
                       int rank, bool test_type=true) 
            : dims_( dims ),
              param_names_( param_names ),
              variable_name_( variable_name ),
              rank_(rank)
            {
                dam_err_ = DAMARIS_OK ;
                
                assert( param_names_.size() == dims ) ;
                assert( dims > 0 ) ;
                
                // Check that our template type T matches out Damaris XML <layout> type
                if (test_type ) {
                    if ( !TestType(variable_name) ) {
                        std::exit(-1) ;
                    }
                }

                num_params_ = param_names_.size() ; 
                param_sizes_ = new int(num_params_) ;
                positions_ = new int64_t(dims) ;

                data_ptr_ = nullptr ;
                paramaters_set_ = false ;
                has_error_ = false ;
                
                SetDamarisParameterAndShmem( param_values ) ;  // Initialise the memory size in the constructor.
            }

            ~DamarisVar( void ) {
                  delete [] param_sizes_ ;
                  delete [] positions_ ;
                  if (data_ptr_ != nullptr) 
                  {
                      CommitVariableDamarisShmem() ; 
                      ClearVariableDamarisShmem() ; 
                  }
                  if (this->HasError()) 
                      PrintError() ; // flush out any error messages
            }

            /**
            *  Method to check that the template paramater T is the same as the requested type for the variable in the XML file
            */
            bool TestType(std::string variable_name)
            {
                bool resbool = true ;
                // This gets the type of the Damaris XML <variable>'s <layout>
                DAMARIS_TYPE_STR vartype ;
                dam_err_ = damaris_get_type(variable_name.c_str(), &vartype) ;
                if (dam_err_ != DAMARIS_OK) {
                    dam_err_sstr_ << "  ERROR rankDamarisVar::DamarisVar ()  damaris_get_type(\"" 
                                     << variable_name_ << "\", vartype);  Damaris error = " <<  damaris_error_string(dam_err_) << std::endl ;
                    has_error_ = true ;
                    return( false ) ;
                }
                T test_id;
                const std::type_info& t1 = typeid(test_id) ;
                
                if (vartype == DAMARIS_TYPE_DOUBLE)
                {
                    double td = 0.0 ;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                } 
                else if (vartype == DAMARIS_TYPE_FLOAT) 
                {
                    float td = 0.0f ;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                } 
                else if (vartype == DAMARIS_TYPE_CHAR)
                {
                    char td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_UCHAR)
                {
                    unsigned char td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_SHORT)
                {
                    short td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_USHORT)
                {
                    unsigned short td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_INT)
                {
                    int td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_UINT)
                {
                    unsigned int td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_LONG)
                {
                    long td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_ULONG)
                {
                   unsigned long td = 0;
                    const std::type_info& t2 = typeid(td) ;
                    if (t1 != t2) {
                        OutputErrorAndAssert(variable_name, t1.name(), t2.name()) ;
                        resbool = false ;
                    }
                }
                else if (vartype == DAMARIS_TYPE_UNDEFINED)
                {
                    std::cerr << "ERROR rank =" << rank_ << " : DamarisVar::DamarisVar()::  \"" << variable_name << "\" has type DAMARIS_TYPE_UNDEFINED"<< std::endl ;
                    resbool = false ;
                } 
                else 
                {
                    std::cerr << "ERROR rank =" << rank_ << " : DamarisVar::DamarisVar():: \"" << variable_name << "\" is not of available type "<< std::endl ;
                    resbool = false ;
                }
                
                return resbool ;
            }

            /**
            *  Allow a user to indicate that the Damaris variable has allocated a size -
             * This method is usefull as a single paramater can control one or more layouts and
             * a single layout can describe the size of multiple <variable> elements.
             * i.e. The current variable has had it's paramater(s) set through via another variable.
            */
            void ParameterIsSet() 
            {
                paramaters_set_ = true ;
            }

            void PrintError ( void )
            {
                std::cerr << dam_err_sstr_.str() ;
            }

            bool HasError( void )
            {
               return (has_error_) ;
            }

            /**
            *  Returns the data pointer to shared memory, or nullptr if it has not been allocated
            */
            T * data_ptr( void ) 
            {
               return (data_ptr_) ;
            }

            std::string & variable_name( void )
            {
               return (variable_name_) ;
            }

             /**
            * Creates the XML representation of the variable from the available strings
            */
            std::string ReturnXMLForVariable ( void )
            {

                return xml_attributes_.ReturnXMLForVariable() ;

            }
            

            /**
            *  Method to set the Damaris paramater values and set the shmem region \ref data_ptr_
            *
            *  /param [IN] paramSizeVal : A vector of values to set the Damaris paramters to. One element per param_names_ string
            *
            *
            */
            void SetDamarisParameterAndShmem( std::vector<int> paramSizeVal ) 
            {
                this->SetDamarisParameter( paramSizeVal )  ;
                this->SetPointersToDamarisShmem() ;
            }

            /**
            *  Method to set the Damaris paramater values.
            *
            *  /param [IN] paramSizeVal : An pointer to a value or array of values to set. One element per param_names_ string
            *
            *  /implicit                : Implicitly uses the array of paramater names: \ref param_names_
            */
            void SetDamarisParameter( std::vector<int>& paramSizeVal ) 
            {
                assert(paramSizeVal.size() == num_params_) ;

                bool resbool = true ;
                for (int varnum = 0 ; varnum < num_params_ ; varnum++)
                {
                    param_sizes_[varnum] = paramSizeVal[varnum] ;

                    dam_err_ = damaris_parameter_set(param_names_[varnum].c_str(), &paramSizeVal[varnum], sizeof(int));
                    if (dam_err_ != DAMARIS_OK) {
                        dam_err_sstr_ << "  ERROR rank =" << rank_ << " : class DamarisVar : damaris_parameter_set(\"" << param_names_[varnum] 
                                          << "\", paramSizeVal, sizeof(int));  Damaris error = " <<  damaris_error_string(dam_err_) << std::endl ;
                        resbool = false ;
                        has_error_ = true ;
                    }
                }
                if (resbool == true) 
                    paramaters_set_ = true ;
            }

            /**
            *  Method to set the Damaris position values.
            *
            *  /param [IN] positionsVals : A pointer to a value or array of values to set as the offset into the array. 
            *                              One element per dimension (one value for each dim_)
            *
            *  /implicit                 : Implicitly uses the variable name: \ref variable_name_
            */
            void SetDamarisPosition( std::vector<int64_t> positionsVals ) 
            {
                assert(positionsVals.size() == dims_) ;

                for (int pos_dim = 0 ; pos_dim < dims_ ; pos_dim++)
                {
                    positions_[pos_dim] = positionsVals[pos_dim] ;
                }
                dam_err_ = damaris_set_position(variable_name_.c_str(), positionsVals.data());
                if (dam_err_ != DAMARIS_OK) {
                    dam_err_sstr_ << "  ERROR rank =" << rank_ << " : class DamarisVar : damaris_set_position(\"" 
                                     << variable_name_ << "\", positionsVals);  Damaris error = " <<  damaris_error_string(dam_err_) << std::endl ;
                    has_error_ = true ;
                }
            }

            /**
            *  Method to set the Damaris paramater values.
            *
            *  /implicit                : Implicitly uses the Damaris variable name string  \ref variable_name_
            *  /implicit                : Implicitly uses the class data element : \ref data_ptr_
            */
            void SetPointersToDamarisShmem( void )
            {
                if (paramaters_set_ == true )
                {
                    // Allocate memory in the shared memory section... 
                    dam_err_ = damaris_alloc(variable_name_.c_str(), (void **) &data_ptr_) ;
                    if (dam_err_ != DAMARIS_OK) {
                        dam_err_sstr_ << "  ERROR rank =" << rank_ << " : class DamarisVar : damaris_alloc(\"" 
                                      << variable_name_ <<"\", (void **) &ret_ptr)" << ", Damaris error = " <<  damaris_error_string(dam_err_) << std::endl ;
                        has_error_ = true ;
                    }
                } else {
                    dam_err_ = -1 ;
                    dam_err_sstr_ << "ERROR rank =" << rank_  << " : class DamarisVar : SetDamarisParameter() should be called first so as to define"
                                                                  " the size of the memory block required for variable : " << variable_name_ << std::endl ;
                    has_error_ = true ;
                }
            }

            void SetPointersToDamarisShmem( T ** ptr_in )
            {
                if (paramaters_set_ == true )
                {
                    T * temp_ptr ;
                    // Allocate memory in the shared memory section... 
                    dam_err_ = damaris_alloc(variable_name_.c_str(), (void **) &temp_ptr) ;
                    if (dam_err_ != DAMARIS_OK) {
                        dam_err_sstr_ << "  ERROR rank =" << rank_ << " : class DamarisVar : damaris_alloc(\"" 
                                          << variable_name_ <<"\", (void **) &ret_ptr)" << ", Damaris error = " <<  damaris_error_string(dam_err_) << std::endl ;
                        has_error_ = true ;
                    }

                    *ptr_in = temp_ptr ;
                    data_ptr_ = temp_ptr ;
                } else {
                    dam_err_ = -1 ;
                    dam_err_sstr_ << "  ERROR rank =" << rank_  << " : class DamarisVar : SetDamarisParameter() should be called first so as to define "
                                                                   " the size of the memory block required for variable : " << variable_name_ << std::endl ;
                    has_error_ = true ;
                }
            }

            /**
            *  Method to commit the memory of the data written to the Damaris variable - 
            *  Indicates that we will not write any more data to \ref data_ptr_
            *
            *  /implicit                : Implicitly uses the variable name string  \ref variable_name_
            */
            void CommitVariableDamarisShmem( void )
            {
                // Signal to Damaris we are done writing data for this iteration
                dam_err_ = damaris_commit (variable_name_.c_str()) ;
                if (dam_err_ != DAMARIS_OK) {
                    dam_err_sstr_ << "  ERROR rank =" << rank_ << " : class DamarisVar : damaris_commit(\"" 
                                      << variable_name_ <<"\")"  << ", Damaris error = " <<  damaris_error_string(dam_err_) << std::endl ;
                    has_error_ = true ;
                }
            }

            /**
            *  Method to release the memory of the data written to the Damaris variable - 
            *  Indicates that Damaris may take control of the shared memory area that was used for the variable \ref data_ptr_
            *
            *  /implicit                : Implicitly uses the variable name string  \ref variable_name_
            */
            void ClearVariableDamarisShmem( void )
            {
                // Signal to Damaris it has complete charge of the memory area
                dam_err_ = damaris_clear(variable_name_.c_str()) ;
                if (dam_err_ != DAMARIS_OK) {
                    dam_err_sstr_ << "  ERROR rank =" << rank_ << " : class DamarisVar : damaris_clear(\"" 
                                      << variable_name_ << "\")"  << ", Damaris error = " <<  damaris_error_string(dam_err_) << std::endl ;
                    has_error_ = true ;
                }
                data_ptr_ = nullptr ;
            }
        private:
            void OutputErrorAndAssert(std::string& var_name, std::string type_name1, std::string type_name2)
            {
                dam_err_sstr_ << "ERROR rank =" << rank_ << " : DamarisVar::DamarisVar () variable_name_: \"" << var_name 
                    << "\" The template type of Type of DamarisVar<T> in the code: " << type_name1 << " does not match type in XML: " << type_name2 << std::endl ;
                PrintError() ;
                assert( type_name1 == type_name2  ) ;
            }
    };  // class DamarisVar

    } // namespace model

} // namespace damaris

#endif 