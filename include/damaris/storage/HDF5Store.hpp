/**************************************************************************
This file is part of Damaris.

Damaris is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Damaris is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Damaris.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/
#ifndef __DAMARIS_HDF5_STORE_H
#define __DAMARIS_HDF5_STORE_H

#include <stdint.h>
#include <string>
#include <map>

#include "storage/Store.hpp"
#include "data/VariableManager.hpp"
#include "hdf5.h"



namespace damaris {

/** 
 * HDF5Store.hpp is a Store that writes HDF5 files, either file per process, 
 * or collective writes as single HDF5 file. Data to write is specified in
 * the Damaris XML file by specifying the name in the  <variable ... store="MyStore", />
 * tag where "MyStore" is the <store name="" > element
 */
class HDF5Store : public Store {

    friend class Deleter<HDF5Store>;    
    /**
     * Constructor. 
     */
    HDF5Store(const model::Store& mdl);
            
    /**
     * Destructor.
     */
    virtual ~HDF5Store() {
        // make sure our std::vector<hsize_t> pointers are deleted
        for ( auto& it : this->memSelect_) {
            if (it.second != nullptr)
                delete it.second  ;
        }
        
        // make sure our std::vector<hsize_t> pointers are deleted
        for ( auto& it : this->fileSelect_) {
            if (it.second != nullptr)
                delete it.second  ;
        }
    }

private:
    enum FileMode {FilePerCore , FilePerNode , Collective, CollectiveTest};
    enum XdmfMode {NoIteration, FirstIteration, EveryIteration};

    FileMode fileMode_;
    XdmfMode xdmfMode_;
    std::string path_;

    /**
    * Updates the memory space and the dimentions according to ghost zones.
    */
    bool UpdateGhostZones(std::shared_ptr<Variable> v , hid_t &memSpace , hsize_t* localDim);
    /**
    * Returns the full name of the variable, including the groups (separated by a "/")
    */
    std::string GetVariableFullName(std::shared_ptr<Variable> v , std::shared_ptr<Block> *b = NULL);
    /**
    * Returns the output file name of the result .h5 file.
    */
    std::string GetOutputFileName(int32_t iteration);
    /**
    * This function gets a type from the model type and retuns its equivalient HDF5 type as the output parameter.
    * If the type could not be found, the return value is false, otherwise it is true.
    */
    bool GetHDF5Type(model::Type mdlType , hid_t& hdfType );

    /**
    * This function reads the related key-values from the store xml element and
    * initializes the corresponding values in the class.
    */
    bool ReadKeyValues(const model::Store& mdl);

   /**
    * This function writes the data of an iteration into a single HDF5 file using collective I/O.
    * Each iteration writes to a new dataset in the same file.
    * Parallel HDF5 has been used as an I/O middleware here.
    * Collective I/O is used by specifying "Collective" in the Damaris XML config file <store> section:
    *  <option key="FileMode">Collective</option>
    */
    void OutputCollective(int32_t iteration);
    
    /**
    * This is the default collective output mode, where each block of a variable is treated as a hyperslab section
    * of the full data set array
    */
    void OutputCollectiveOrdered(int32_t iteration, std::shared_ptr<Variable> v, hid_t& fileId, hid_t& lcplId) ;
    
    /**
    * This function is enabled when the Damaris variable XML includes a select="varname" attribute. If this
    * attribute is present (and  varname is a valid variable) then the select variable data is used to order the writing
    * of the data variable. The underlying HDF5 function deals with multi-dimensional indices by using each set of 
    * values (set size equal to the rank of the data variable) as the indices into the data array. A subset of values,  
    *  can be selected for writing this way. 
    */
    void OutputCollectiveSelect(int32_t iteration, std::shared_ptr<Variable> v, std::shared_ptr<Variable> v_select, hid_t& fileId, hid_t& lcplId) ;
    
    /**
    * This sets up the cellSelect_<> arrays, which converts the Damaris select variable data type to the hsize_t
    * data type required for HDF5 H5Sselect_elements() function.
    *
    *  \return returns the number of blocks found within the variable and -1 on failure
    */
    int SetSelectData(int32_t iteration, std::shared_ptr<Variable> v_select, std::map< std::pair<int, int>,   std::vector<hsize_t> * > & mem_or_file_select) ;
    
    
    /**
    * This function is enabled when the Damaris variable XML includes  select-subset="subset_var" attribute.  select-file="varname_file"  and
    * select-mem="varname_mem" are optional.
    * The subset_var variable describes the decomposition, size and offsets of a dataset to be created on disk, possible containg only a subset
    * of values from the original variable.  The variable is processed using the HDF5Store::ComputeSelectSubsetData() method.
    * Data can be selected for writing to HDF5 datasets this way and written to a multi-dimensional dataset using the decomposition sizes and localDim
    * block sizes stored in the subset_var variable.
    * The number of points selected from memory and selected for the file dataset must contain the same number of elements.
    */
    void OutputCollectiveSelectSubset(int32_t iteration, std::shared_ptr<Variable> v, std::shared_ptr<Variable> v_select_file, 
                                                                                 std::shared_ptr<Variable> v_subset, 
                                                                                 std::shared_ptr<Variable> v_select_memo, 
                                                                                 hid_t& fileId, hid_t& lcplId) ;

    /**
    * This function is enabled when the Damaris variable XML includes a select="varname" together with a select_subset="subset_var" attribute. 
    * This function places the block size information into the std::map selectSubsetBlockSize_ for use in creating the HDF5 memory space and 
    * the function also computes the global size of the selected data dataset.
    *      
    *   This input vraiable v_subset must contain information for each block of data (one or more block of data originates from a damaris client 
    *   when damaris_write() or damaris_write_block() is called )
    *    e.g. 1:
    *     ranks:              sizes:
    *         ------ ---   
    *        |   0  | 1 |            1,6   1,3
    *         ------ ---
    *        | 2 |   3  |            2,3   2,6
    *        |   |      |  
    *         --- ------ 
    *
    *     block data in v_subset will be:
    *     | rank |  data   |
    *     |------|---------|
    *     |  0   | 2,2,1,6 |
    *     |  1   | 2,2,1,3 |
    *     |  2   | 2,2,2,3 |
    *     |  3   | 2,2,2,6 |
    *
    *    e.g. 2:
    *     This example is set up to be symmetric:
    *      ---- ----   
    *     | 0  | 1  |            1,4   1,4
    *      ---- ---- 
    *     | 2  | 3  |            1,4   1,4
    *      ---- ----
    *
    *      block data in v_subset will be:
    *      | rank |  data   |
    *      |------|---------|
    *      |  0   | 2,2,1,4 |
    *      |  1   | 2,2,1,4 |
    *      |  2   | 2,2,1,4 |
    *      |  3   | 2,2,1,4 |
    *
    *
    */
    int ComputeSelectSubsetData(int32_t iteration, std::shared_ptr<Variable> v_subset) ;
    
   /**
    * This function writes the data of an iteration into a single HDF5 file using collective I/O (i.e. as a single dataset). 
    * Parallel HDF5 has been used as an I/O middleware here.
    * It is a to-be-tested version that can be used by specifying "CollectiveTest" in the Damaris
    * XML config file <store> section:
    *  <option key="FileMode">CollectiveTest</option>
    */
    //void OutputCollectiveTest(int32_t iteration);

    /**
    * This function writes the data of an iteration into multiple HDF5 datasets within a single HDF5 file. 
    * Each iteration writes to a new file.
    * No collective I/O is used in this case.
    * This function is enabled by specifying "FilePerCore" in the Damaris XML config file <store> section:
    *  <option key="FileMode">FilePerCore</option>
    *
    * N.B. This name is somewhat decieving, as it is actually a hdf5 dataset written per core, not a file per core.
    */
    void OutputPerCore(int32_t iteration);
    
    
    /**
    * Used to store 'select' block data. This data is obtained from a specified Damaris variable 
    * using the <variable select="" > attribute. The data is used to specify the coordinates in the HDF5 memory space of where to
    * select data from in the memory buffer. The <variable select-subset="" > data is used to chose the HDF5 dataset overall size and
    * shape and the coordinates in cellSelect_ only have to match the memory dataspeace, not what select-subset sets as the shape of
    * the output file. 
    * The data is cast to a hsize_t array of integers from a limited
    * set of Damaris variable types (as specified in the 'select' variable layout int, unsigned int, long int or unsigned long int)
    *
    */
    std::map< std::pair<int, int>,   std::vector<hsize_t> * > memSelect_ ; 
    
    /**
    * Used to store 'select-file' data. This data is obtained from a specified Damaris variable 
    * using the <variable select-file="" > attribute. The data is used to specify the coordinates in the HDF5 file space of where to
    * put the data selected from the HDF5 memory space. The <variable select-subset="" > data is used to chose the HDF5 dataset overall size and
    * shape and the coordinates in fileSelect_ have to match the dimensions and rank of this data.
    * The data is cast to a hsize_t array of integers from a limited
    * set of Damaris variable types (as specified in the 'select' variable layout int, unsigned int, long int or unsigned long int)
    *
    */
    std::map< std::pair<int, int>,   std::vector<hsize_t> * > fileSelect_ ; 
    
    /**
    * Used to store 'select_subset' block data sizes. This data is used to create the HDF5 memory space when the <variable select="" > 
    * attribute is present when using the HDF5 API call H5Sselect_elements() when the size ofthe data selected does not match the size
    * of the original input variable data being selected from.
    *
    */
    std::map< std::pair<int, int>,   std::vector<int> > selectSubsetBlockSize_ ;
    std::map< std::pair<int, int>,   std::vector<int> > selectSubsetOffset_ ;
    /**
    * Used to store 'select' block data offsets. This data is used to create the HDF5 memory space when the <variable select="" > 
    * attribute is present. Offsets are not used in H5Sselect_elements
    */
    // std::map< std::pair<int, int>,   std::vector<hsize_t> * > cellSelectOffsets_ ;
    
    /**
    * Used to store how many elements are in the cellSelect_ map after calling  this->SetSelectData( iteration, v_select) ;
    * Its value is used to check that the number of blocks we have in a variable is equal to the number of select map element blocks
    * when the select variable is not time varying, as the cellSelect_ map is only filled on iteration 0.
    */
    int number_local_blocks_ ; 
    
    
    /**
    * Used to store computed global size of as select_subset data set.
    */
    std::vector<hsize_t> globalSizeSelectSubset_ ;
    
    /**
    * Used to set metadata about a variable which is obtained from a Variables comment="..." 
    * attribute in the Damaris XML file.
    */
    void CreateDatasetStringAttributes(hid_t& dataset_id, int32_t iteration) ;
    
    /**
    * Pass in a string to be used for a datasets HDF attribute. sets attribute_string_
    * N.B. will be overwritten as SetAttributeString() is also called in all the Output...() 
    * methods
    */
    void SetAttributeString(std::string& inString) ;
    
    /**
    * String to be writted ans a HDF5 dataset attribute
    */
    std::string attribute_string_ ;

public:
    
    /**
     * This function is called after an iteration completed
     * and is meant to output all variables into files.
     */
    virtual void Output(const int32_t iteration);


    
    template<typename SUPER>
    static std::shared_ptr<SUPER> New(const model::Store& mdl, const std::string& name) {
        return std::shared_ptr<SUPER>(new HDF5Store(mdl), Deleter<HDF5Store>());
    }
};

}

#endif
