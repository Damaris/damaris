#! /bin/bash

# Author: Josh Bowden, Inria
# Date: 4/2/2022
# Usage:   ./build_damaris_docs.sh    DAMARIS_VER  
#               DAMARIS_VER  is the tag or a branch to build. Default is master.
#
#          Requires sudo if docker commands need sudo privlages
#          Script will try to build docker images with a build of Daamris with the -DENABLE_DOCS=ON
#          and then copy the built docs to the local machine at $PWD/public/$DAMARIS_VER
#
# Requirements: 
#         A file: Dockerfile.Damaris.for.DOCS that has template strings to replace using sed:
#           _BASEWITHPARAVIEW_  _DISTLIB_  _HDF_PATH_  _MPI_BIN_PATH_  _PV_SHORT_DOT_  _INSTALL_GFORT_
#
#          Need to be logged into the registry:
#            > docker login registry.gitlab.inria.fr
#
# Outputs: 
#        Docker image built with documentation built - the doc/html directory will be 
#        copied to pages directory as: pages/$DAMARIS_VER/html
#          
#         

# if you need to build docs from damaris-development then change this:
export DAMARIS_REPO=damaris

# Builds the master branch by default
if [[ "$1" != "" ]] ; then
  export DAMARIS_VER=$1
else
  export DAMARIS_VER=master
fi

echo ""
echo "DAMARIS DOCS BUILD JOB STARTING"
echo ""

PV_VER_ARRAY=(v5.10.0)
DOCKERFILE_ARRAY=(ubuntu22)
LIBNAME_ARRAY=(lib)
HDF_PATH_INDEX=(0)
HDF_PATH_ARRAY=("-DHDF5_ROOT=/usr/lib/x86_64-linux-gnu/hdf5/openmpi"  "")
MPI_BIN_PATH_ARRAY=("/usr/bin/")


#######################################################
##  Loop through docker images and build damaris
#######################################################

# docker login registry.gitlab.inria.fr
DOCKER_IMAGE_BASENAME=registry.gitlab.inria.fr/damaris/damaris-development
DOCKER_IMAGE_OUTPUTNAME=registry.gitlab.inria.fr/damaris/$DAMARIS_REPO

i=0

LIB64=${LIBNAME_ARRAY[$i]}
WHICH_HDFPATH=${HDF_PATH_INDEX[$i]}
HDF_PATH=${HDF_PATH_ARRAY[${WHICH_HDFPATH}]}
MPI_BIN_PATH=${MPI_BIN_PATH_ARRAY[$i]}
LEN=${LEN_ARRAY[$i]}
PV_VERSION=${PV_VER_ARRAY[$i]};

PVSHORT=${PV_VERSION//./}
PV_SHORT_DOT=${PV_VERSION:1:-2}
BASEIMAGETAG=$(echo $BASE_IMAGE_SHORT-p${PVSHORT})

 # Check if the image exists in the repository (returns 0 if image is present)
 BUILD_IMAGE=$(docker manifest inspect registry.gitlab.inria.fr/damaris/damaris-development:ubuntu-22.04-pv5100-docs  > /dev/null 2>&1 ; echo $?) 
  
if [[ "$BUILD_IMAGE" == "0" ]] ; then
      # The base container exists in the repository   
        cp Dockerfile.Damaris.DOCS Dockerfile.out            
        sed -i "s|_DISTLIB_|${LIB64}|g" Dockerfile.out
        sed -i "s|_HDF_PATH_|${HDF_PATH}|g" Dockerfile.out
        sed -i "s|_MPI_BIN_PATH_|${MPI_BIN_PATH}|g" Dockerfile.out
        sed -i "s|_PV_SHORT_DOT_|${PV_SHORT_DOT}|g" Dockerfile.out
        if [[ "$DAMARIS_REPO" == "damaris" ]] ; then
            DOCKER_BUILDKIT=1 docker build --no-cache -t \
            registry.gitlab.inria.fr/damaris/damaris:ubuntu-22.04-pv5100-docs-damaris-${DAMARIS_VER} \
               --build-arg INPUT_damaris_ver=${DAMARIS_VER} \
               --build-arg INPUT_repo=${DAMARIS_REPO} \
              -f ./Dockerfile.out . 
        fi
        if [[ $? -eq 0 ]] ; then
           echo "INFO: registry.gitlab.inria.fr/damaris/damaris:ubuntu-22.04-pv5100-docs-damaris-${DAMARIS_VER}  built"
           mkdir -p public/${DAMARIS_VER}
           echo "Copying created docs from container (docs) to directory: public/${DAMARIS_VER}"
           docker run -it -d --name docs registry.gitlab.inria.fr/damaris/damaris:ubuntu-22.04-pv5100-docs-damaris-${DAMARIS_VER}
           docker cp docs:/home/docker/build/${DAMARIS_REPO}/docs/doc/html/ public/${DAMARIS_VER}/
           docker stop docs
           docker rm docs
        else 
           echo "ERROR: registry.gitlab.inria.fr/damaris/damaris:ubuntu-22.04-pv5100-docs-damaris-${DAMARIS_VER} could not be built"
        fi
        # rm ./Dockerfile.out
 else
   echo "INFO: registry.gitlab.inria.fr/damaris/damaris-development:ubuntu-22.04-pv5100-docs could not be found. "
   echo "INFO: First make sure you are logged into the registry: "
   echo "     >docker login registry.gitlab.inria.fr "
   echo "INFO: or, if the image does not exist, then build it using: "
   echo "     >DOCKER_BUILDKIT=1 docker build --no-cache -t  registry.gitlab.inria.fr/damaris/damaris-development:ubuntu-22.04-pv5100-docs  -f Dockerfile.Damaris.Docs.base . "
   echo "INFO: And then push it to the registry "
   echo "     >docker login registry.gitlab.inria.fr "
   echo "     >docker push registry.gitlab.inria.fr/damaris/damaris-development:ubuntu-22.04-pv5100-docs"
fi


echo ""
echo "DAMARIS DOCS BUILD JOB FINISHED"
echo ""
